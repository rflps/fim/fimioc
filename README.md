# fimioc

RF Local Protection System (RFLPS) Fast Interlock Module (FIM) IOC is the EPICS module for an IOxOS IFC1410 equipped with the ADC3117 and DIO3118 FMCs used for the fast interlock system of the klystrons at the European Spallation Source.
