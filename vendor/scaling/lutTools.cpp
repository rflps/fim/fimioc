#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>

#include <iostream>
#include <stdexcept>      

#include "loadcsv.h"
#include "lutTools.h"


int lutByInterpolation_signed(int lutSize, int nPoints, double *rawArray, double *eguArray, double *lutArray)
{
    long iter, iter2;
    double a, b; // linear coeficients y = ax + b
    double x1, x2, y1, y2; // auxiliary variables

    if (nPoints < 2) {
        for (iter = 0; iter < (long) lutSize; iter++)
            lutArray[iter] = (double) iter - (lutSize/2);

        return 0;
    }

    /* First two pairs of points */
    x1 = rawArray[0];
    x2 = rawArray[1];
    y1 = eguArray[0];
    y2 = eguArray[1];

    a = ((y2 - y1) / (x2 - x1));
    b = y1 - (a * x1);

    /* Fill the lookup table starting from element 0 up two the end of first segment (rawArray[1]) */
    /* The linear function of the first segment will be extended to cross the y axis at zero */
    for (iter = (-lutSize/2); iter < (long) x2; iter++) {
        lutArray[iter+(lutSize/2)] = (a * (double) iter) + b;
    }

    /* Now iterates over the next segments */
    for (iter=2; iter < (long) nPoints; iter++) {

        /* Calculate coeficients */
        x1 = rawArray[iter-1];
        x2 = rawArray[iter];
        y1 = eguArray[iter-1];
        y2 = eguArray[iter];

        a = ((y2 - y1) / (x2 - x1));
        b = y1 - (a * x1);

        /* Fill the lookup table with this segment */
        for (iter2 = (long) x1; iter2 < (long) x2; iter2++)
            lutArray[iter2+(lutSize/2)] = (a * (double) iter2) + b;
    }

    /* Last segment is an extension of the last calculated line*/
    for (iter = (long) rawArray[nPoints-1]; iter < (long) (lutSize/2); iter++)
        lutArray[iter+(lutSize/2)] = (a * (double) iter) + b;
   
    // At this point, lutArray should be filled completely
    return 0;
}


int lutByInterpolation(int lutSize, int nPoints, double *rawArray, double *eguArray, double *lutArray)
{
    long iter, iter2;
    double a, b; // linear coeficients y = ax + b
    double x1, x2, y1, y2; // auxiliary variables

    if (nPoints < 2) {
        for (iter = 0; iter < (long) lutSize; iter++)
            lutArray[iter] = (double) iter;

        return 0;
    }

    /* First two pairs of points */
    x1 = rawArray[0];
    x2 = rawArray[1];
    y1 = eguArray[0];
    y2 = eguArray[1];

    a = ((y2 - y1) / (x2 - x1));
    b = y1 - (a * x1);

    /* Fill the lookup table starting from element 0 up two the end of first segment (rawArray[1]) */
    /* The linear function of the first segment will be extended to cross the y axis at zero */
    for (iter=0; iter < (long) x2; iter++)
        lutArray[iter] = (a * (double) iter) + b;

    /* Now iterates over the next segments */
    for (iter=2; iter < (long) nPoints; iter++) {

        /* Calculate coeficients */
        x1 = rawArray[iter-1];
        x2 = rawArray[iter];
        y1 = eguArray[iter-1];
        y2 = eguArray[iter];

        a = ((y2 - y1) / (x2 - x1));
        b = y1 - (a * x1);

        /* Fill the lookup table with this segment */
        for (iter2 = (long) x1; iter2 < (long) x2; iter2++)
            lutArray[iter2] = (a * (double) iter2) + b;
    }

    /* Last segment is an extension of the last calculated line*/
    for (iter = (long) rawArray[nPoints-1]; iter < (long) lutSize; iter++)
        lutArray[iter] = (a * (double) iter) + b;
   
    // At this point, lutArray should be filled completely
    return 0;
}


lutTools::lutTools(const std::string& filename, size_t lutsize, lutTools_types_t type, lutTools_default_t defaultvalues) :
    m_lutSize(lutsize)
{
    if (readCsv(filename.c_str())) {
        // Failed to read CSV, generate table according to default configuration
        if (defaultvalues == lutTools_voltage_table) {
            m_EguVector.push_back(-10.0);
            m_EguVector.push_back(0.0);
            m_EguVector.push_back(10.0 - (20/(double)m_lutSize));
        } else {
            m_EguVector.push_back((type == lutTools_signed ? (static_cast<double>(m_lutSize)/2.0*(-1.0)) : 0.0));
            m_EguVector.push_back((type == lutTools_signed ? 0.0 : (m_lutSize/2)));
            m_EguVector.push_back((type == lutTools_signed ? (m_lutSize/2 - 1) : (m_lutSize-1)));
        }

        if (type == lutTools_signed) {
            m_RawVector.push_back(static_cast<double>(m_lutSize)/2.0*(-1.0));
            m_RawVector.push_back(0.0);
            m_RawVector.push_back(static_cast<double>(m_lutSize/2 - 1));
        } else {
            m_RawVector.push_back(0.0);
            m_RawVector.push_back(static_cast<double>(m_lutSize/2));
            m_RawVector.push_back(static_cast<double>(m_lutSize - 1));
        }
        mNumPoints = 3;
    }

    // m_EguVector and m_RawVector contains the calibration points, now generates LUT
    createLUT(type);

    mLutArray = nullptr;
    mEguArray = nullptr;
    mRawArray = nullptr;
}

int lutTools::sortDataVectors() {

    if (m_EguVector.size() != m_RawVector.size())
    {
        return -1;
    }

    // Pair vectors of Raw and EGU values
    std::vector<std::pair<double,double>> pairedVector;
    for( size_t idx = 0; idx < m_EguVector.size(); idx++ )
    {
        pairedVector.push_back(std::make_pair(m_RawVector[idx], m_EguVector[idx]));
    }

    // Sort the vector of pairs on ascending order of raw values
    std::sort(
        std::begin(pairedVector), std::end(pairedVector), 
        [&](const std::pair<double,double>& a, const std::pair<double,double>& b)
        {
            return a.first < b.first;
        }
    );

    // Write sorted values back to the original vectors
    for( size_t idx = 0; idx < m_EguVector.size(); idx++ )
    {
        m_RawVector[idx] = pairedVector[idx].first;
        m_EguVector[idx] = pairedVector[idx].second;
    }

    return 0;

}

int lutTools::readCsv(const char* fileName) {
     // Parameters validation; TODO use exceptions?
    if (!fileName) {
        return -1;
    }

    // read the CSV file and obtain the raw and egu values
    loadCSV csvObj;
    int status = csvObj.load(fileName);

    // CSV file must have 2 columns and > 1 rows
    if (!status || csvObj.getCol() != 2 || csvObj.getRow() < 2) {
        return -1;
    }

    //allocate array in form _values[_col][_row] 
    double** _Arrays = csvObj.getValues();
    mNumPoints = csvObj.getRow();

    // Iterate over the _Arrays to obtain the vectors for raw and egu values
    for (int i = 0; i < mNumPoints; i++) {
        m_EguVector.push_back(_Arrays[0][i]);
        m_RawVector.push_back(_Arrays[1][i]);
    }

    /* After update values, free the memory allocated to _Arrays */
    delete[] _Arrays[0];
    delete[] _Arrays[1];
    delete[] _Arrays;
   
   return 0; //success
}


lutTools::lutTools()  : 
    m_lutSize(65536)
{
    /* init pointers to NULL */
    mLutArray = nullptr;
    mEguArray = nullptr;
    mRawArray = nullptr;
    mNumPoints = 0;
    mLutElements = 0;
}


lutTools::~lutTools(void) {
    /* Cleaning the memory */
    delete[] mLutArray;
    delete[] mEguArray;
    delete[] mRawArray;
}

int lutTools::readCsvFile(const char* fileName) {

     // Parameters validation; TODO use exceptions?
    if (!fileName) {
        throw std::invalid_argument("Invalid file handler");
    }

    // read the CSV file and obtain the raw and egu values
    loadCSV csvObj;
    int status = csvObj.load(fileName);

    // CSV file must have 2 columns and > 1 rows
    if (!status || csvObj.getCol() != 2 || csvObj.getRow() < 2) {
        throw std::invalid_argument("CSV file not valid");
    }

    //allocate array in form _values[_col][_row] 
    double** _Arrays = csvObj.getValues();
    mNumPoints = csvObj.getRow();

    // Allocate memory for the RAW and EGU arrays
    mEguArray = new double[mNumPoints];
    mRawArray = new double[mNumPoints];
    // m_EguVector.reserve(mNumPoints);
    // m_RawVector.reserve(mNumPoints);

    // Iterate over the _Arrays to obtain the vectors for raw and egu values
    for (int i = 0; i < mNumPoints; i++) {
        mEguArray[i] = _Arrays[0][i];
        mRawArray[i] = _Arrays[1][i];
        m_EguVector.push_back(_Arrays[0][i]);
        m_RawVector.push_back(_Arrays[1][i]);
    }

    /* After update values, free the memory allocated to _Arrays */
    delete[] _Arrays[0];
    delete[] _Arrays[1];
    delete[] _Arrays;
   
   return 0; //success
}


int lutTools::getNumPoints(void) {
    return mNumPoints;
}

int lutTools::generateLookupTable(int lutElements) {

    if ((lutElements < 2) || ((lutElements%2) != 0)) {
        throw std::invalid_argument("Invalide LUT size");
    }
    
    // Checking if file was already loaded, this guarantees that raw and egu arrays exists
    if (!mNumPoints) {
        throw std::logic_error("File not loaded yet");
    }

     /* Finally allocate memory for the lookup table */
    mLutElements = lutElements;
    mLutArray = new double[mLutElements];

    /* Call the routine that will perform the interpolation between the pairs */
    lutByInterpolation_signed(mLutElements, mNumPoints, mRawArray, mEguArray, mLutArray);

    return 0;
}


int lutTools::generateLookupTable(int lutElements, double* target) {

    if ((lutElements < 2) || ((lutElements%2) != 0)) {
        throw std::invalid_argument("Invalide LUT size");
    }
    
    // Checking if file was already loaded, this guarantees that raw and egu arrays exists
    if (!mNumPoints) {
        throw std::logic_error("File not loaded yet");
    }

    /* Check if the pointer exists */
    if (!target) {
        throw std::invalid_argument("Destination pointer is not valid");
    }

     /* Finally allocate memory for the lookup table */
    mLutElements = lutElements;

    /* Call the routine that will perform the interpolation between the pairs */
    lutByInterpolation_signed(mLutElements, mNumPoints, mRawArray, mEguArray, target);

    return 0;
}

int lutTools::getLut(double *target) {

    if ((!target) || (!mLutArray)) {
        printf("Error on lutFromFile::getLut -> pointer to NULL\n");
        return -1;
    }
    memcpy(target, mLutArray, mLutElements*(sizeof(double)));
    return 0;
} 

int lutTools::getRaw(double *target) {

    if ((!target) || (!mRawArray)) {
        printf("Error on lutFromFile::getRaw -> pointer to NULL\n");
        return -1;
    }
    memcpy(target, mRawArray, mNumPoints*(sizeof(double)));
    return 0;
} 

int lutTools::getEgu(double *target) {

    if ((!target) || (!mEguArray)) {
        printf("Error on lutFromFile::getEgu -> pointer to NULL\n");
        return -1;
    }
    memcpy(target, mEguArray, mNumPoints*(sizeof(double)));
    return 0;
}

int lutTools::createLUT(lutTools_types_t type) {
    // Error if vectors have different sizes
    if ((m_EguVector.size() != m_RawVector.size()) or (m_EguVector.size() != (size_t)mNumPoints)) {
        return -1;
    }

    // If just two points were given, fill standard 1-to-1 LUT    
    if (m_EguVector.size() < 2) {
        for (int iter = 0; iter < (int) m_lutSize; iter++) {
            if (type == lutTools_signed) m_LUT.push_back(static_cast<double>(iter-(m_lutSize/2)));
            else if (type == lutTools_unsigned) m_LUT.push_back(static_cast<double>(iter));
        }
        return 0;
    }

    sortDataVectors();
    
    int iter, iter2;
    double a, b; // linear coeficients y = ax + b
    double x1, x2, y1, y2; // auxiliary variables

    //First two pairs of points
    x1 = m_RawVector[0];
    x2 = m_RawVector[1];
    y1 = m_EguVector[0];
    y2 = m_EguVector[1];

    a = ((y2 - y1) / (x2 - x1));
    b = y1 - (a * x1);

    // Fill the lookup table starting from element 0 up two the end of first segment (rawArray[1]) 
    // The linear function of the first segment will be extended to cross the y axis at zero
    int lutinit = (type == lutTools_signed ? (-m_lutSize/2) : 0);    
    for (iter = lutinit; iter < (int) x2; iter++)
        m_LUT.push_back((a * (double) iter) + b);

    /* Now iterates over the next segments */
    for (iter=2; iter < (long) mNumPoints; iter++) {

        /* Calculate coeficients */
        x1 = m_RawVector[iter-1];
        x2 = m_RawVector[iter];
        y1 = m_EguVector[iter-1];
        y2 = m_EguVector[iter];

        a = ((y2 - y1) / (x2 - x1));
        b = y1 - (a * x1);

        /* Fill the lookup table with this segment */
        for (iter2 = (long) x1; iter2 < (long) x2; iter2++)
            m_LUT.push_back((a * (double) iter2) + b);
    }

    /* Last segment is an extension of the last calculated line*/
    int lutend = (type == lutTools_signed ? (m_lutSize/2) : m_lutSize);
    for (iter = (long) m_RawVector.back(); iter < (long) lutend; iter++)
        m_LUT.push_back((a * (double) iter) + b);
   
    // At this point, lutArray should be filled completely
    return 0;

}

// return by copy?
std::vector<double> lutTools::copyLUT() {
    return m_LUT;
}

std::vector<double> lutTools::copyRawPoints() {
    return m_RawVector;
}

std::vector<double> lutTools::copyEguPoints() {
    return m_EguVector;
}
