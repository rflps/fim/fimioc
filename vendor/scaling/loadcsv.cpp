#include "loadcsv.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



/*
 * Load a CSV file with just numbers (double) to an array
 * */
int loadCSV::load(const char* fileName) {
    FILE* stream;
    char line[BUFFERSIZE];
    char* tok;
    double num;

    stream = fopen(fileName, "r");
    if (stream == 0)
        return 0;

    _col = 0;
    _row = 0;

    fgets(line, BUFFERSIZE, stream);
    fgets(line, BUFFERSIZE, stream);

    
    // there is no data
    if (!strcmp(line,""))
        return 0;

    // get the number of columns
    for (tok = strtok(line, DELIMITER); tok && *tok; tok = strtok(NULL, ENDLINE), _col++)
        continue;

    //back file to begin
    rewind(stream);

    /* Will count the number of rows until EOF or until a new line starts with carriage return */
    while (fgets(line, BUFFERSIZE, stream)) {
        if (line[0] == '\n')
            break;

        // Ignore comments 
        if ( _row > 0 && line[0] == '#')
        {
            continue;
        }

        _row += 1;
    }
        
    //ignore first line
    _row -= 1;

    //allocate array in form _values[_col][_row]
    _values = new double*[_col];
    for (int i = 0; i < _col; i++)
        _values[i] = new double[_row];

    //back file to begin
    rewind(stream);

    int i, j ;
    i = j = 0;

    // read the first line and ignore it
    fgets(line, BUFFERSIZE, stream);

    while (fgets(line, BUFFERSIZE, stream) && (j<_row)){
        for (tok = strtok(line, DELIMITER); tok && *tok && i < _col; tok = strtok(NULL, ENDLINE), i++)
        {
            num = atof(tok);
            _values[i][j] = num;
        }
        j++;
        i = 0;
    }
    return 1;
}

double** loadCSV::getValues() {
    return _values;
}

int loadCSV::getCol(){
    return _col;
}

int loadCSV::getRow(){
    return _row;
}
