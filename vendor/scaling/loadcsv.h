#ifndef _loadcsv_h
#define _loadcsv_h

#define BUFFERSIZE 1024
#define DELIMITER ","
#define ENDLINE ",\n"


class loadCSV {
    public:
        int load(const char* fileName);
        double** getValues();
        int getCol();
        int getRow();
    protected:
        double** _values;
        int _col;
        int _row;
};

#endif /* _loadcsv_h */
