#!/usr/bin/python3

import os
import time
import epics

# configure runtime
os.environ["EPICS_CA_ADDR_LIST"] = "172.30.5.209"

# PVs
fim_commander = epics.PV("LAB-010:RFS-FIM-101:FSM")
fim_state = epics.PV("LAB-010:RFS-FIM-101:FSM-RB")

di_autoreset = epics.PV("LAB-010:RFS-FIM-101:DI1-FastRst-En")
di_mode = epics.PV("LAB-010:RFS-SIM-110:HvEnaCmd-DIlck-Sim")
di_reset = epics.PV("LAB-010:RFS-SIM-110:HvEnaCmd-IlckRst")
di_ilckcount = epics.PV("LAB-010:RFS-FIM-101:DI1-FastRst-IlckCnt")
di_ilckreset = epics.PV("LAB-010:RFS-FIM-101:DI1-FastRst-RstCnt")

fastrst_count = epics.PV("LAB-010:RFS-FIM-101:FastReset-IlckCnt")


def setRFON():
    fim_commander.put("RFON")
    while fim_state.get(as_string=True) != "RFON":
        print("Waiting FIM to go HV ON ...")
        fim_commander.put("RFON")
        time.sleep(0.5)

    print("FIM is in RF ON!")


# while loop
def main():

    print("Starting FIM tester ...")
    active = True

    # put DI0 into SIMULATION
    di_mode.put(0)

    # reset DI0
    di_reset.put(1)
    di_ilckreset.put(1)
    di_autoreset.put(1)

    # mode to RFON
    setRFON()

    while active:

        print("Starting interlock stream")

        # reset everything
        # put DI0 into SIMULATION
        di_mode.put(0)

        # reset DI0
        di_reset.put(1)
        di_ilckreset.put(1)

        # move to RF ON
        setRFON()
        # time.sleep(1.0)

        # put DI0 to interlock mode
        di_mode.put(1)

        # wait main ILCK count to reach 5 (max ilck)
        while fastrst_count.get() <= 14:
            time.sleep(0.1)
        print("Interlock detected!")

        time.sleep(5.0)

        # check if main ilck count and channel ilck counts are the same
        mainilck = fastrst_count.get()
        localilck = di_ilckcount.get()

        if mainilck != localilck:
            print("PROBLEM!!!!")
            print("GLOBAL ILCK COUNT = ", mainilck)
            print("LOCAL  ILCK COUNT = ", localilck)
            # di_autoreset.put(1)
            active = False
        else:
            print("Normal ilck behaviour")
            active = True

        # time.sleep(1.0)


if __name__ == "__main__":
    main()
