#!/usr/bin/python3

import time
import epics

# configure runtime
# os.environ['EPICS_CA_ADDR_LIST']='10.0.16.92 10.0.16.85'

# PVs
fim_commander = epics.PV("LAB-010:RFS-FIM-101:GPDO")


def main():
    print("Starting pulse generator loop...")
    while True:
        fim_commander.put(1)
        time.sleep(1.0)
        # print("setting output to high...")


if __name__ == "__main__":
    main()
