#!/usr/local/opt/python@3.8/bin/python3

import os
import time
import epics

# configure runtime
os.environ["EPICS_CA_ADDR_LIST"] = "10.0.16.92 10.0.16.85"

# PVs
fim_commander = epics.PV("LAB-010RFC:RFS-FIM-110:INTS")
fim_state = epics.PV("LAB-010RFC:RFS-FIM-110:INTS-RB")
pulse_length = epics.PV("LAB-010:RFS-EVR-001:DlyGen0-Width-SP")
reset_ai = epics.PV("LAB-010RFC:RFS-Mod-110:Cur:resetIlck")
reset_fim = epics.PV("LAB-010RFC:RFS-FIM-110:resetFIM")
interlock = epics.PV("LAB-010RFC:RFS-Mod-110:Cur:FastIntStat")


def setRFON():
    # Go back to HV ON...
    if fim_state.get(as_string=True) == "RF ON":
        fim_commander.put("HV ON")
        while fim_state.get(as_string=True) != "HV ON":
            print("Waiting FIM to go HV ON ...")
            time.sleep(0.5)

    # Go back to IDLE
    if fim_state.get(as_string=True) == "HV ON":
        fim_commander.put("IDLE")
        while fim_state.get(as_string=True) != "IDLE":
            print("Waiting FIM to go IDLE ...")
            time.sleep(0.5)

    # We are in IDLE, try to go up to RF ON
    print("Moving FIM to HV ON...")
    fim_commander.put("HV ON")
    while fim_state.get(as_string=True) != "HV ON":
        time.sleep(0.5)

    print("Moving FIM to RF ON...")
    fim_commander.put("RF ON")
    while fim_state.get(as_string=True) != "RF ON":
        time.sleep(0.5)

    print("FIM is in RF ON!")


def resetChannel():
    while interlock.get() == 0.0:
        print("Reset channel...")
        reset_ai.put(1)
        time.sleep(0.25)


# while loop
def main():

    print("Starting FIM tester ...")
    pulse_length.put(2998, wait=True)
    reset_fim.put(1, wait=True)
    setRFON()
    time.sleep(2.0)
    length = 3500

    while True:
        # set long pulse
        pulse_length.put(length, wait=True)
        while interlock.get() != 0.0:
            time.sleep(0.25)
        print("Interlock detected!")

        # reset
        pulse_length.put(2998, wait=True)
        # reset_ai.put(1,wait=True)
        resetChannel()

        # move to RF ON
        setRFON()

        # delay
        time.sleep(3.0)

        length = length + 1
        if length > 3600:
            length = 3500


if __name__ == "__main__":
    main()
