###############################################################################
# Generic startup template for RFLPS Fast Interlock Module IOC
###############################################################################

###############################################################################
# Required modules and versions
###############################################################################
require asyn
require autosave
require iocstats
require recsync

### Main module of the IOC
require nds3epics
require fimioc
#
###############################################################################
# Environmental variables settings
###############################################################################
#
### EPICS configurations
epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES, 10000000)
epicsEnvSet(NELM, 8000)
#
### External file with individual macros for all input channels
iocshLoad("fim_config.iocsh")
#
### SEC-SUB prefix
epicsEnvSet("PREFIX", "SYS-SUB")
#
### Calibration Files Folder
epicsEnvSet("CALIB_FOLDER", "$(E3_CMD_TOP)/calibration")
#
###############################################################################
# IOC Access Security File
###############################################################################
asSetFilename("$(E3_CMD_TOP)/fim-access.acf")
#
###############################################################################
# IFC1410 EPICS driver for RFLPS FIM firmware
###############################################################################
ndsCreateDevice(ifc1410_fim, $(PREFIX), card=0, fmc=1, files=$(CALIB_FOLDER=/tmp), winsize=$(NELM), dataformat=0)
#nds setLogLevelDebug $(PREFIX)-FSM
#nds setLogLevelDebug $(PREFIX)-FSM-DI-CH0-ILCK
#
###############################################################################
# Analog Inputs Records
###############################################################################
dbLoadRecords(fimioc.template, "P=$(PREFIX):, R=$(IOCDEVICE):, NDSROOT=$(PREFIX), NDS0=FSM, NELM=$(NELM)")
#
###############################################################################
# Analog Inputs Records
###############################################################################
iocshLoad("$(fimioc_DIR)/fimLoad_AI.iocsh")
#
###############################################################################
# Digital Inputs Records
###############################################################################
iocshLoad("$(fimioc_DIR)/fimLoad_DI.iocsh")
#
###############################################################################
# Reflected Power Special Blocks Records
###############################################################################
iocshLoad("$(fimioc_DIR)/fimLoad_special.iocsh")
#
###############################################################################
# Soft Interlock
###############################################################################
# epicsEnvSet("INPUT_CHANNEL", "LAB-010RFC:RFS-DirC-110:PwrFwdPA")
# dbLoadRecords(fimioc-softilck.template, "PREFIX=$(PREFIX), DEVICE=$(IOCDEVICE), P=$(PREFIX):, R=$(IOCDEVICE):, INPUT_CHANNEL=$(INPUT_CHANNEL)")

###############################################################################
# autosave Module setup
###############################################################################
epicsEnvSet("AS_REMOTE", "$(E3_CMD_TOP)/autosave")
epicsEnvSet("AS_FOLDER", "savfiles")
#
set_savefile_path    ("$(AS_REMOTE)/$(AS_FOLDER)", "/save")
set_requestfile_path ("$(AS_REMOTE)/$(AS_FOLDER)", "/req")
#
system("install -d $(AS_REMOTE)/$(AS_FOLDER)/save")
system("install -d $(AS_REMOTE)/$(AS_FOLDER)/req")
#
save_restoreSet_status_prefix("$(PREFIX):Ctrl-IOC-101:AS-")
save_restoreSet_Debug(0)
save_restoreSet_IncompleteSetsOk(1)
save_restoreSet_DatedBackupFiles(1)
save_restoreSet_NumSeqFiles(3)
save_restoreSet_SeqPeriodInSeconds(300)
save_restoreSet_CAReconnect(1)
save_restoreSet_CallbackTimeout(-1)
#
dbLoadRecords("save_restoreStatus.db", "P=$(PREFIX):Ctrl-IOC-101:AS-, DEAD_SECONDS=5")
#
afterInit("makeAutosaveFileFromDbInfo('$(AS_REMOTE)/$(AS_FOLDER)/req/values_pass1.req','autosaveFields_pass1')")
afterInit("create_monitor_set('values_pass1.req','5')")
afterInit("fdbrestore("$(AS_REMOTE)/$(AS_FOLDER)/save/values_pass1.sav")")
#

###############################################################################
# iocStats Module setup
###############################################################################
epicsEnvSet("IOCSTATSPREFIX", "$(PREFIX):Ctrl-IOC-101:Meta")
iocshLoad("$(iocstats_DIR)/iocStats.iocsh", "IOCNAME=$(IOCSTATSPREFIX)")
#
###############################################################################
# recSync Module setup
###############################################################################
epicsEnvSet("RECSYNCPREFIX", "$(PREFIX):Ctrl-IOC-101:Meta")
iocshLoad("$(recsync_DIR)/recsync.iocsh", "IOCNAME=$(RECSYNCPREFIX)")
#
###############################################################################
# Sequencer setup
###############################################################################
epicsEnvSet("SEQ_1", "$(PREFIX):$(DEV_DI0):$(CHN_DI0)-isFirstIlck")
epicsEnvSet("SEQ_2", "$(PREFIX):$(DEV_DI0):$(CHN_DI0)-IlckRst")
epicsEnvSet("SEQ_3", "$(PREFIX):$(DEV_DI1):$(CHN_DI1)-isFirstIlck")
epicsEnvSet("SEQ_4", "$(PREFIX):$(DEV_DI1):$(CHN_DI1)-IlckRst")
afterInit('seq fimioc_automation_seq "P=$(PREFIX):, R=$(IOCDEVICE):, HVON_FST=$(SEQ_1), HVON_RST=$(SEQ_2), RFON_FST=$(SEQ_3), RFON_RST=$(SEQ_4)"')
#
###############################################################################
# Launch IOC engine
###############################################################################
iocInit()
#
