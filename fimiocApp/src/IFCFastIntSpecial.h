#ifndef _IFCFastIntSpecial_H
#define _IFCFastIntSpecial_H

#include <vector>
#include <map>

#include <nds3/nds.h>
#include <ifcfastintdrv.h>

#include "interlock_info.h"

typedef std::map<int, std::vector<double> > calib_table_t;

class IFCFastIntSpecial {
public:
	IFCFastIntSpecial(const std::string &name, nds::Node &parentNode, struct ifcdaqdrv_usr &deviceUser, int block,
		calib_table_t &table, const fim_dataformat_t &dataformat, const fim_special_ilck_t &ilcktype);

	int32_t m_ilck;
	nds::Node m_node;
	nds::StateMachine m_stateMachine;
	const int m_block;

	std::shared_ptr<FIM_InterlockInfo> m_InterlockInfo;

	void historyAddValues(uint8_t ppq, uint8_t winmask, uint8_t winmeas, uint8_t trig, uint8_t trigoff);
	void historyReset(bool pmortem);
	void reserve(size_t nframes);

	void setState(nds::state_t newState);

	// void setPPOutput(int32_t qout);
	void getPPQOut(timespec *timespec, std::vector<std::uint8_t> *value);
	void getWinMask(timespec *timespec, std::vector<std::uint8_t> *value);
	void getWinMeas(timespec *timespec, std::vector<std::uint8_t> *value);
	void getTrig(timespec *timespec, std::vector<std::uint8_t> *value);
	void getTrigOff(timespec *timespec, std::vector<std::uint8_t> *value);

	void setPPMode(const timespec &timespec, const int32_t &value);
	void getPPMode(timespec *timespec, int32_t *value);

	void getState(timespec *timespec, int32_t *value);

	void setActive(const timespec &timespec, const int32_t &value);
	void getActive(timespec *timespec, int32_t *value);

	void resetInterlock(const timespec &timespec, const int32_t &value);
	void getresetInterlock(timespec *timespec, int32_t *value);

	void setAutoReset(const timespec &timespec, const int32_t &value);
	void getAutoReset(timespec *timespec, int32_t *value);

	void setTriggerChannel(const timespec &timespec, const int32_t &value);
	void getTriggerChannel(timespec *timespec, int32_t *value);

	void setMeasChannel(const timespec &timespec, const int32_t &value);
	void getMeasChannel(timespec *timespec, int32_t *value);

	void setMaskPeriod(const timespec &timespec, const int32_t &value);
	void getMaskPeriod(timespec *timespec, int32_t *value);

	void setMeasPeriod(const timespec &timespec, const int32_t &value);
	void getMeasPeriod(timespec *timespec, int32_t *value);

	void setThreTrigger(const timespec &timespec, const double &value);
	void getThreTrigger(timespec *timespec, double *value);

	void setThreInterlock(const timespec &timespec, const double &value);
	void getThreInterlock(timespec *timespec, double *value);

	void setInterlockStateTransitionIdlePre(const timespec &timespec, const int32_t &value);
	void getInterlockStateTransitionIdlePre(timespec *timespec, int32_t *value);

	void setInterlockStateTransitionPreRun(const timespec &timespec, const int32_t &value);
	void getInterlockStateTransitionPreRun(timespec *timespec, int32_t *value);

	void setBpOffTrigLine(const timespec &timespec, const int32_t &value);
	void getBpOffTrigLine(timespec *timespec, int32_t *value);

	void setBpOnTrigLine(const timespec &timespec, const int32_t &value);
	void getBpOnTrigLine(timespec *timespec, int32_t *value);

	void setBpOffTrigEna(const timespec &timespec, const int32_t &value);
	void getBpOffTrigEna(timespec *timespec, int32_t *value);

	void setBpOnTrigEna(const timespec &timespec, const int32_t &value);
	void getBpOnTrigEna(timespec *timespec, int32_t *value);

	void setCavDecEvalTime(const timespec &timespec, const int32_t &value);
	void getCavDecEvalTime(timespec *timespec, int32_t *value);
	void setCavDecDevRate(const timespec &timespec, const int32_t &value);
	void getCavDecDevRate(timespec *timespec, int32_t *value);

	void setCavDevDetecLimit(const timespec &timespec, const double &value);
	void getCavDevDetecLimit(timespec *timespec, double *value);

	void setCavDecOffset(const timespec &timespec, const double &value);
	void getCavDecOffset(timespec *timespec, double *value);

	uint8_t detectInterlock(ifcfastint_fsm_state state);
	void setFirstIlck(int32_t ilck);

	void fastResetCmd();

	bool getHVONprecond() {
		return m_HVONprecond;
	};
	bool getRFONprecond() {
		return m_RFONprecond;
	};

	void setHwTimestamp(const timespec& ts);

private:

	void switchOn();
	void switchOff();
	void start();
	void stop();
	void recover();
	bool allowChange(const nds::state_t, const nds::state_t, const nds::state_t);

	struct ifcdaqdrv_usr &m_deviceUser;
	const fim_dataformat_t m_DataFormat;
	const fim_special_ilck_t m_SpecialIlckType;

	std::vector<std::uint8_t> m_ppqhistory;
	std::vector<std::uint8_t>::iterator m_ppqhistory_ptr;

	std::vector<std::uint8_t> m_winmask;
	std::vector<std::uint8_t>::iterator m_winmask_ptr;

	std::vector<std::uint8_t> m_winmeas;
	std::vector<std::uint8_t>::iterator m_winmeas_ptr;

	std::vector<std::uint8_t> m_trig;
	std::vector<std::uint8_t>::iterator m_trig_ptr;

	std::vector<std::uint8_t> m_trigoff;
	std::vector<std::uint8_t>::iterator m_trigoff_ptr;


	/* Set of calibration table for all channels */
	calib_table_t &m_CalibTable;

	/* Variables for transition conditions */
	bool m_HVONenabled;
	bool m_RFONenabled;

	/* Variable to hold current interlock status */
	bool m_ilckStatus;
	bool m_HVONprecond;
	bool m_RFONprecond;

	// Variables to hold the trigger and measurement channels
	int m_TriggerChannel;
	int m_MeasChannel;
	int m_bpTrigger;

	/* Placeholder: external hardware timestamp */
	timespec m_HwTimestamp;

	/* PV for channel data */
	nds::PVDelegateIn<std::vector<std::uint8_t>> m_ppqhistoryPV;

	nds::PVDelegateIn<std::vector<std::uint8_t>> m_ppq_pmortemPV;
	nds::PVDelegateIn<std::vector<std::uint8_t>> m_winmaskpmortemPV;
	nds::PVDelegateIn<std::vector<std::uint8_t>> m_trigpmortemPV;

	nds::PVDelegateIn<std::vector<std::uint8_t>> m_winmaskPV;
	nds::PVDelegateIn<std::vector<std::uint8_t>> m_winmeasPV;
	nds::PVDelegateIn<std::vector<std::uint8_t>> m_trigPV;
	nds::PVDelegateIn<std::vector<std::uint8_t>> m_trig_offPV;


	/* PVs for readback values */
	nds::PVDelegateIn<std::int32_t> m_activePV;
	nds::PVDelegateIn<std::int32_t> m_autoresetPV;
	nds::PVDelegateIn<std::int32_t> m_modePV;
	nds::PVDelegateIn<std::int32_t> m_statePV;
	nds::PVDelegateIn<std::int32_t> m_trigSelPV;
	nds::PVDelegateIn<std::int32_t> m_measSelPV;

	nds::PVDelegateIn<std::int32_t> m_maskPeriodPV;
	nds::PVDelegateIn<std::int32_t> m_measPeriodPV;
	nds::PVDelegateIn<double> m_threTriggerPV;
	nds::PVDelegateIn<double> m_threMeasPV;

	nds::PVDelegateIn<std::int32_t> m_fsmTransIdlePrePV;
  	nds::PVDelegateIn<std::int32_t> m_fsmTransPreRunPV;

	nds::PVDelegateIn<std::int32_t> m_bpOffTriggerPV;
	nds::PVDelegateIn<std::int32_t> m_bpOnTriggerPV;

	nds::PVDelegateIn<std::int32_t> m_bpOffTriggerEnablePV;
	nds::PVDelegateIn<std::int32_t> m_bpOnTriggerEnablePV;

	nds::PVDelegateIn<std::int32_t> m_cavDecEvalTimePV;
	nds::PVDelegateIn<std::int32_t> m_cavDecDevRatePV;

	nds::PVDelegateIn<double> m_cavDecLimitPV;
	nds::PVDelegateIn<double> m_cavDecOffsetPV;

	// Precondition PVs
	nds::PVVariableIn<std::int32_t> m_ilckStatusPV;
	nds::PVVariableIn<std::int32_t> m_HVONPreCondPV;
	nds::PVVariableIn<std::int32_t> m_RFONPreCondPV;
	nds::PVVariableIn<std::int32_t> m_firstIlckPV;
};


class IFCFastIntSpecialGroup {
public:
	IFCFastIntSpecialGroup(const std::string &name, nds::Node &parentNode,ifcdaqdrv_usr &deviceUser,
		calib_table_t &table, const fim_dataformat_t &dataformat, const fim_special_ilck_t &ilcktype);

	nds::Port m_node;
	ifcdaqdrv_usr &m_deviceUser;
	std::vector<std::shared_ptr<IFCFastIntSpecial>> m_RefPwrChannels;

	std::vector<std::shared_ptr<IFCFastIntSpecial>> getChannels();
	//void getInfoMessage(timespec *timespec, std::string *value);
	void setState(nds::state_t newState);

	private:
};




#endif /* _IFCFastIntSpecial_H */
