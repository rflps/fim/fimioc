/**
 * @file IFCFastIntAIChannelGroup.h
 * @brief Header file defining the analog input channel group class.
 * @author nclaesson
 * @date 2016-03-29
 */

#ifndef IFCFastIntAICHANNELGROUP_H
#define IFCFastIntAICHANNELGROUP_H

#include <nds3/nds.h>

#include "IFCFastIntAIChannel.h"

/**
 * @brief IFCFastIntAIChannelGroup
 */
class IFCFastIntAIChannelGroup {
public:
  IFCFastIntAIChannelGroup(const std::string &name, nds::Node &parentNode,
                           ifcdaqdrv_usr &deviceUser, std::string &calibFolder,
                           size_t grpIndex, const fim_dataformat_t &dataformat);

  nds::Port m_node;
  nds::StateMachine m_stateMachine;
  ifcdaqdrv_usr &m_deviceUser;
  std::string &m_calibFolder;
  uint32_t m_numChannels;

  std::vector<std::shared_ptr<IFCFastIntAIChannel>> m_AIChannels;
  std::vector<std::shared_ptr<IFCFastIntAIChannel>> getChannels();

  void getInfoMessage(timespec *timespec, std::string *value);
  void getTriggerButton(timespec *timespec, std::string *value);

  void getSampleRate(timespec *timespec, double *value);
  void getClockSource(timespec *timespec, int32_t *value);
  void getClockFrequency(timespec *timespec, double *value);
  void getClockDivisor(timespec *timespec, int32_t *value);
  void getADCRange(timespec *timespec, std::vector<std::int32_t> *value);

  void setState(nds::state_t newState);
  void onSwitchOn();
  void onSwitchOff();
  void onStart();
  void onStop();
  void recover();
  bool allowChange(const nds::state_t currentLocal,
                   const nds::state_t currentGlobal,
                   const nds::state_t nextLocal);
  void commitParameters(bool calledFromAcquisitionThread = false);
  void setHwTimestamp(const timespec& ts);

private:
  double m_sampleRate;

  int32_t m_clockSource;
  double m_clockFrequency;
  int32_t m_clockDivisor;

  std::vector<std::int32_t> m_ADCRange;

  timespec m_HwTimestamp;

  nds::PVDelegateIn<std::string> m_infoPV;

  nds::PVDelegateIn<double> m_sampleRatePV;

  nds::PVDelegateIn<std::int32_t> m_clockSourcePV;
  nds::PVDelegateIn<double> m_clockFrequencyPV;
  nds::PVDelegateIn<std::int32_t> m_clockDivisorPV;

  nds::PVDelegateIn<std::vector<std::int32_t>>  m_ADCRangePV;
};

#endif /* IFCFastIntAICHANNELGROUP_H */
