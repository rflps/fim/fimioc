#ifndef INTERLOCK_INFO_H
#define INTERLOCK_INFO_H

#include <nds3/nds.h>
#include <ifcfastintdrv.h>
#include "common.h"

class FIM_InterlockInfo {
public:
	FIM_InterlockInfo(const std::string &name, nds::Node &parentNode,
						const int32_t channelNum, const fim_inputtype_t input_type);

	// Class public parameters
	nds::Node m_node;

	bool isAutoReset();
	void registerInterlock();

	void setResetCount(const timespec &timespec, const int32_t &value);
	void getResetCount(timespec *timespec, int32_t *value);

	fim_inputtype_t getInputType() {
		return m_inputType;
	};

private:

	// Class private parameters
	const std::int32_t m_channelNum;
	const fim_inputtype_t m_inputType;

	/* Variables for parameters */
	int32_t m_IlckCounter;

	/* PVs for readback values */
	nds::PVVariableOut<std::int32_t> m_AutoResetEnablePV;
	nds::PVVariableIn<std::int32_t> m_InterlockCountPV;

};


#endif // INTERLOCK_INFO_H
