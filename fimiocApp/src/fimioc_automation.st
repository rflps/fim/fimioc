program fimioc_automation_seq

%% #include <string.h>
%% #include <stdio.h>
%% #include <math.h>
%% #include <stdarg.h>

%{#include <stdlib.h>}%
%{#include <unistd.h>}%

option +d;

/* State machine info PV */
#define INITIAL         0
#define STATE_DETECTOR  1
#define IDLE_WAIT_COND  2
#define IDLE_WAIT_SIM   3
#define IDLE_WAIT_HVON  4
#define HVON_WAIT_COND  5
#define HVON_WAIT_SIM   6
#define HVON_WAIT_RFON  7
#define RFON_RUNNING    8
#define RF_INTERLOCK    9
#define HV_INTERLOCK    10
#define CONFIG_STATE    11

/* NDS3 state machine names */
#define NDS_UNKNOWN         0
#define NDS_OFF             1
#define NDS_SWITCHING_OFF   2
#define NDS_INITIALIZING    3
#define NDS_ON              4
#define NDS_STOPPING        5
#define NDS_STARTING        6
#define NDS_RUNNING         7
#define NDS_FAULT           8

/* FIM state machine names */
#define FIM_IDLE    0
#define FIM_ARM     1
#define FIM_HVON    2
#define FIM_RFON    3
#define FIM_ABO     4

/********************* EPICS PVs declaration *************************/

/* State machine status PV */
int state_info;
assign state_info to "{P}{R}#Dbg-stateS";

/* IOC initalization flag */
int iocinit_rb;
assign iocinit_rb to "{P}{R}InitDone";
monitor iocinit_rb;

/* NDS3 State machine*/
int nds_state;
assign nds_state to "{P}{R}NdsFSM";
int nds_state_rb;
assign nds_state_rb to "{P}{R}NdsFSM-RB";
monitor nds_state_rb;

/* FIM interlock logic state machine */
int fim_state_s;
assign fim_state_s to "{P}{R}FSM";
monitor fim_state_s;
int fim_state_rb;
assign fim_state_rb to "{P}{R}FSM-RB";
monitor fim_state_rb;

/* Overall pre-condition of IDLE to HVON */
int idle_hvon_precond;
assign idle_hvon_precond to "{P}{R}HVON-Precond";
monitor idle_hvon_precond;

/* Overall pre-condition of HVON to RFON */
int hvon_rfon_precond;
assign hvon_rfon_precond to "{P}{R}RFON-Precond";
monitor hvon_rfon_precond;


/* SIM HV ON is first */
int sim_hvon_first;
assign sim_hvon_first to "{HVON_FST}";
monitor sim_hvon_first;

int sim_hvon_reset;
assign sim_hvon_reset to "{HVON_RST}";

/* SIM RF ON is first */
int sim_rfon_first;
assign sim_rfon_first to "{RFON_FST}";
monitor sim_rfon_first;

int sim_rfon_reset;
assign sim_rfon_reset to "{RFON_RST}";


/********************* Sequencer State Machine *************************/

ss fimioc_automation_ss {

	state init_state {

		entry {
			state_info = INITIAL;
			pvPut(state_info);
            pvGet(nds_state_rb);
            //printf("[SEQUENCER] Entering init_state\n");
		}

    	when (nds_state_rb == NDS_ON) {
            // forcer NDS to go to RUNNING
            nds_state = NDS_RUNNING;
            pvPut(nds_state);
        }
		state wait_nds_running

    	when (nds_state_rb == NDS_RUNNING) {}
		state wait_init_done

		when(delay(10)) {}
		state init_state
	}

    state wait_nds_running {

        entry {
			state_info = INITIAL;
			pvPut(state_info);
            pvGet(nds_state_rb);
            //printf("[SEQUENCER] Entering wait_nds_running\n");
		}

        when (nds_state_rb == NDS_RUNNING) {}
        state wait_init_done

		when(delay(1)) {}
		state wait_nds_running
    }

    state wait_init_done {

		entry {
			state_info = INITIAL;
			pvPut(state_info);
            pvGet(iocinit_rb);
            pvGet(nds_state_rb);
            //printf("[SEQUENCER] Entering wait_init_done\n");
		}

    	when (nds_state_rb != NDS_RUNNING) {}
		state wait_init_done

        when (iocinit_rb == 1) {
            //printf("[SEQUENCER] Autosave loaded! starting operation\n");
        }
        state wait_safe_time

		when(delay(10)) {}
		state wait_init_done
    }

    state wait_safe_time {
		when(delay(5)) {}
		state fim_state_detector
    }


    state fim_state_detector {

		entry {
			state_info = STATE_DETECTOR;
			pvPut(state_info);
            pvGet(fim_state_rb);
            //printf("[SEQUENCER] Entering fim_state_detector\n");
		}

        when (fim_state_rb == FIM_IDLE) {
            //printf("[SEQUENCER] Detected IDLE state\n");
        }
        state s_idle_wait_cond

        when (fim_state_rb == FIM_HVON) {
            //printf("[SEQUENCER] Detected HVON state\n");
        }
        state s_hvon_wait_cond

        when (fim_state_rb == FIM_RFON) {
            //printf("[SEQUENCER] Detected RFON state\n");
        }
        state s_rfon_running

        when (fim_state_rb == FIM_ABO) {}
        state fim_state_detector
    }

    state s_idle_wait_cond {

		entry {
			state_info = IDLE_WAIT_COND;
			pvPut(state_info);
            pvGet(nds_state_rb);
            //printf("[SEQUENCER] Entering s_idle_wait_cond\n");
		}

        when (nds_state_rb == NDS_ON) {
            //printf("[SEQUENCER] Detected a transition to CONFIG state\n");
        }
        state s_config_state

        when (idle_hvon_precond == 1) {
            //printf("[SEQUENCER] Good to go for IDLE waiting command!\n");
        }
        state s_idle_wait_hvon

		when(delay(10)) {}
		state s_idle_wait_cond
    }

    state s_idle_wait_hvon {
		entry {
			state_info = IDLE_WAIT_HVON;
			pvPut(state_info);
            pvGet(idle_hvon_precond);
            pvGet(fim_state_rb);
		}

        when (nds_state_rb == NDS_ON) {
            //printf("[SEQUENCER] Detected a transition to CONFIG state\n");
        }
        state s_config_state

        when (idle_hvon_precond == 0) {
            //printf("[SEQUENCER] Falling back to wait condition...\n");
        }
        state s_idle_wait_cond

        when (fim_state_rb == FIM_HVON) {
            //printf("[SEQUENCER] HVON state detected!\n");
        }
        state s_hvon_wait_cond

		when(delay(2)) {}
		state s_idle_wait_hvon
    }

/******* HV ON state *************/
    state s_hvon_wait_cond {

		entry {
			state_info = HVON_WAIT_COND;
			pvPut(state_info);
            pvGet(nds_state_rb);
            pvGet(hvon_rfon_precond);
            //printf("[SEQUENCER] Entering s_hvon_wait_cond\n");
		}

        when (nds_state_rb == NDS_ON) {
            //printf("[SEQUENCER] Detected a transition to CONFIG state\n");
        }
        state s_config_state

        when (hvon_rfon_precond == 1) {
            //printf("[SEQUENCER] Good to go for RFON waiting command!\n");
        }
        state s_hvon_wait_rfon

        when (idle_hvon_precond == 0) {
            //printf("[SEQUENCER] Falling back to wait condition...\n");
        }
        state s_idle_wait_cond

        when (fim_state_s == FIM_IDLE) {
            //printf("[SEQUENCER] Not an interlock! A command to go back to IDLE!\n");
        }
        state s_idle_wait_cond

        when ((fim_state_rb == FIM_IDLE) && (fim_state_s != FIM_IDLE)) {
            //printf("[SEQUENCER] INTERLOCK IN HV ON!\n");
            state_info = HV_INTERLOCK;
        }
        state s_interlock

		when(delay(10)) {}
		state s_hvon_wait_cond
    }

    state s_hvon_wait_rfon {
		entry {
			state_info = HVON_WAIT_RFON;
			pvPut(state_info);
            pvGet(hvon_rfon_precond);
            pvGet(fim_state_rb);
		}

        when (nds_state_rb == NDS_ON) {
            //printf("[SEQUENCER] Detected a transition to CONFIG state\n");
        }
        state s_config_state

        when (hvon_rfon_precond == 0) {
            //printf("[SEQUENCER] Falling back to wait condition...\n");
        }
        state s_hvon_wait_cond

        when (fim_state_rb == FIM_RFON) {
            //printf("[SEQUENCER] RFON state detected!\n");
        }
        state s_rfon_running

        when (fim_state_s == FIM_IDLE) {
            //printf("[SEQUENCER] Not an interlock! A command to go back to IDLE!\n");
        }
        state s_idle_wait_cond

        when ((fim_state_rb == FIM_IDLE) && (fim_state_s != FIM_IDLE)) {
            //printf("[SEQUENCER] INTERLOCK IN HV ON!\n");
            state_info = HV_INTERLOCK;
        }
        state s_interlock


		when(delay(2)) {}
		state s_hvon_wait_rfon
    }


    /******************** RF ON *******************************/
    state s_rfon_running {

		entry {
			state_info = RFON_RUNNING;
			pvPut(state_info);
            pvGet(nds_state_rb);
            //printf("[SEQUENCER] Entering s_rfon_running\n");
		}

        when (nds_state_rb == NDS_ON) {
            //printf("[SEQUENCER] Detected a transition to CONFIG state\n");
        }
        state s_config_state

        when ((fim_state_s == FIM_HVON) || (fim_state_s == FIM_IDLE)) {
            //printf("[SEQUENCER] Not an interlock! A command to go back to IDLE or HVON!\n");
        }
        state s_idle_wait_cond

        when (((fim_state_rb == FIM_IDLE) && (fim_state_s != FIM_IDLE)) || ((fim_state_rb == FIM_HVON) && (fim_state_s != FIM_HVON))) {
            //printf("[SEQUENCER] INTERLOCK IN RF ON!\n");
            state_info = RF_INTERLOCK;
        }
        state s_interlock

		when(delay(10)) {}
		state s_rfon_running
    }

    state s_interlock {

        entry {
            pvPut(state_info);
            //printf("[SEQUENCER] Entering s_interlock\n");
        }

        when((sim_hvon_first > 0) || (sim_rfon_first > 0)) {

            /* If it's from FIM digital signals, auto reset it */
            pvGet(sim_hvon_first);
            if (sim_hvon_first > 0) {

                //printf("[SEQUENCER] sim_hvon_first == %d!\n", sim_hvon_first);

                sim_hvon_reset = 1;
                pvPut(sim_hvon_reset);
            }

            pvGet(sim_rfon_first);
            if (sim_rfon_first == 1) {
                //printf("[SEQUENCER] sim_rfon_first == %d!\n", sim_rfon_first);
                sim_rfon_reset = 1;
                pvPut(sim_rfon_reset);
            }
        }
        state fim_state_detector

		when(delay(0.5)) {}
		state fim_state_detector


    }




    /***************************************************************************************************************/
    state s_config_state {

		entry {
			state_info = CONFIG_STATE;
			pvPut(state_info);
            pvGet(nds_state_rb);
            //printf("[SEQUENCER] Entering s_config_state\n");
		}

        when (delay(10)) {
            nds_state = NDS_RUNNING;
            pvPut(nds_state);
        }
        state s_post_config_state
    }

    state s_post_config_state {

		entry {
			state_info = CONFIG_STATE;
			pvPut(state_info);
            pvGet(nds_state_rb);
            //printf("[SEQUENCER] Entering s_post_config_state\n");
		}

        when (nds_state_rb == NDS_RUNNING) {
            //printf("[SEQUENCER] Configuration State Timeout!\n");
        }
        state fim_state_detector

        when (delay(10)) {}
        state s_post_config_state
    }
    /***************************************************************************************************************/



} // end of state set (ss)
