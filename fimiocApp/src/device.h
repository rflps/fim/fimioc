#ifndef _DEVICE_H_
#define _DEVICE_H_

#include "nds3/nds.h"
#include "ifcdaqdrv.h"

#include "IFCFastIntInterlockFSM.h"

class FIM_Device {
public:
  FIM_Device(nds::Factory &factory, const std::string &deviceName,
                   const nds::namedParameters_t &parameters);
  ~FIM_Device();

private:
  std::shared_ptr<IFCFastIntInterlockFSM> m_FIM;
  struct ifcdaqdrv_usr m_deviceUser;
  std::string m_calibFolder;
  nds::Node m_node;
};

#endif /* _DEVICE_H_ */
