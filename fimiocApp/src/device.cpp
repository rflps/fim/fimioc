
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <csignal>
#include <unistd.h>

#include <initHooks.h>
#include <iocsh.h>
#include <epicsExport.h>

#include "device.h"
#include "common.h"
#include "IFCFastIntInterlockFSM.h"

void exitIOCHandler(int signum)
{
	std::cout << "\n[FIMIOC] Interrupt signal captured! " << std::endl;
	exit(signum);
}

FIM_Device::FIM_Device(nds::Factory &factory, const std::string &deviceName, const nds::namedParameters_t &parameters)
		: m_node(deviceName)
{
	ifcdaqdrv_status status;

	// Since this device support uses both fmc concurrently we simply always pass 1 as fmc.
	m_deviceUser = {static_cast<uint32_t>(std::stoul(parameters.at("card"))), 1, 0};

	// Get lenght of acquisition window (number of frames)
	size_t nframes;
	if (parameters.find("winsize") == parameters.end()) {
		nframes = HISTORY_FRAME_COUNT_MAX;
	} else {
		nframes = std::stoul(parameters.at("winsize"));
		if (nframes > HISTORY_FRAME_COUNT_MAX) nframes = HISTORY_FRAME_COUNT_MAX;
	}

	int dataformat;
	if (parameters.find("dataformat") == parameters.end()) {
		dataformat = FIM_DATAFORMAT_UNSIGNED;
	} else {
		dataformat = std::stoul(parameters.at("dataformat"));
		if ((dataformat != FIM_DATAFORMAT_UNSIGNED) and (dataformat != FIM_DATAFORMAT_SIGNED))
			dataformat = FIM_DATAFORMAT_UNSIGNED;
	}

	// Register EXIT signal handler
	std::signal(SIGTERM, exitIOCHandler);
	std::signal(SIGINT, exitIOCHandler);

	// Calibration/scaling initialization
	if (parameters.find("files") == parameters.end()) {
		// Calibration files are not present, indicate to the child nodes using pre-defined string
		m_calibFolder = "invalid";
	} else {
		// calibFolder holds the absolute address of the folder where the calibration files reside
		m_calibFolder = parameters.at("files");
	}

	status = ifcdaqdrv_open_device(&m_deviceUser);
	if (status != status_success) {
		throw nds::NdsError("Failed to open device");
	}
	status = ifcdaqdrv_init_adc(&m_deviceUser);
	if (status != status_success) {
		throw nds::NdsError("Failed to initialize ADC");
	}

	// Start the DIO3118 FMC card for the DIO signals
	status = ifcfastint_init_dio3118(&m_deviceUser);
	if (status != status_success) {
		throw nds::NdsError("Failed to initialize DIO3118 FMC");
	}

	status = ifcfastint_init_fsm(&m_deviceUser);
	if (status != status_success) {
		throw nds::NdsError("Failed to initialize FSM");
	}

	// Create the child node FSM
	m_FIM = std::make_shared<IFCFastIntInterlockFSM>("FSM", m_node, m_deviceUser, m_calibFolder, nframes, dataformat);

	// Initialize device
	m_node.initialize(this, factory);
	std::cout << "[FIMIOC] IFC1410 is initialized!" << std::endl;

	// Move main state machine to ON state
	m_FIM->m_stateMachine.setState(nds::state_t::on);
}

FIM_Device::~FIM_Device() {
	m_FIM->m_stateMachine.setState(nds::state_t::on);
	while (m_FIM->m_stateMachine.getLocalState() != nds::state_t::on) {
		usleep(100000);
	}

	m_FIM->m_stateMachine.setState(nds::state_t::off);
	while (m_FIM->m_stateMachine.getLocalState() != nds::state_t::off) {
		usleep(100000);
	}

	std::cout << "[FIMIOC] Gracefully exiting IOC... " << std::endl;
	ifcdaqdrv_close_device(&m_deviceUser);
}

//NDS_DEFINE_DRIVER(ifc1410_fim, FIM_Device);
extern "C" {

void* allocateDevice(nds::Factory& factory, const std::string& device, const nds::namedParameters_t& parameters) {
	return new FIM_Device(factory, device, parameters);
}

void deallocateDevice(void* device)
{
	delete (FIM_Device*)device;
}

const char* getDeviceName() {
	return "ifc1410_fim";
}

nds::RegisterDevice<FIM_Device> registerDeviceifc1410_fim("ifc1410_fim");


/**
 * @brief Callback function for EPICS IOC initialization steps.  Used to trigger
 *        normal processing by the driver.
 *
 * @param[in] state IOC current state
 */
void fimioc_initHookFunc(initHookState state)
{

}

/**
 * @brief The function that registers the EPIS IOC Shell functions
 */
void fimioc_Register(void)
{
  static bool firstTime = true;

  if (firstTime)
  {
    initHookRegister(fimioc_initHookFunc);
  }
}

epicsExportRegistrar(fimioc_Register);



}
