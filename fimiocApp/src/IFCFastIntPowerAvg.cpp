#include <cstdlib>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>
#include <ifcfastintdrv.h>

#include "device.h"
#include "IFCFastIntPowerAvg.h"

/* Convert from [0=-1, 32k=0, 64k=1] to [-32k=-1, 0=0, 32k-1=1] */
inline int32_t raw_to_signed(int16_t raw) { return (int16_t)(raw - 32768); }

/* Convert from [-32k=-1, 0=0, 32k-1=1] to [0=-1, 32k=0, 64k=1] */
inline int16_t signed_to_raw(int32_t signd) { return (int16_t)(signd + 32768); }


IFCFastIntPowerAvg::IFCFastIntPowerAvg(const std::string &name,
																				 nds::Node &parentNode,
																				 int32_t pwravgNum,
																				 ifcdaqdrv_usr &deviceUser)
		: m_pwravgNum(pwravgNum), m_deviceUser(deviceUser),
		m_node(nds::Port(name, nds::nodeType_t::generic)),
			m_dataPV(nds::PVDelegateIn<double>(
					"Data", std::bind(&IFCFastIntPowerAvg::getData, this,
														std::placeholders::_1, std::placeholders::_2))),
			m_historyPV(nds::PVDelegateIn<std::vector<double>>(
					"History", std::bind(&IFCFastIntPowerAvg::getHistory, this,
															 std::placeholders::_1, std::placeholders::_2))),
		 m_ppqhistoryPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
				"PPQHistory", std::bind(&IFCFastIntPowerAvg::getPPQHistory, this,
											 std::placeholders::_1, std::placeholders::_2))),
			m_pp_outPV(nds::PVDelegateIn<std::int32_t>(
					"PPQOUT", std::bind(&IFCFastIntPowerAvg::getPPQOut, this,
															std::placeholders::_1, std::placeholders::_2))),
	m_pp_emulatedPV(nds::PVDelegateIn<std::int32_t>(
					"PP-EMUL-RB",
					std::bind(&IFCFastIntPowerAvg::getPPEmulated, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_pp_modePV(nds::PVDelegateIn<std::int32_t>(
					"PP-MODE-RB",
					std::bind(&IFCFastIntPowerAvg::getPPMode, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_pp_cvalPV(nds::PVDelegateIn<double>(
					"PP-CVAL-RB",
					std::bind(&IFCFastIntPowerAvg::getPPCVal, this,
										std::placeholders::_1, std::placeholders::_2))),
		m_fsmTransIdlePrePV(nds::PVDelegateIn<std::int32_t>(
			"TRANS-IDLE2PRE-RB",
			std::bind(&IFCFastIntPowerAvg::getInterlockStateTransitionIdlePre,
					this, std::placeholders::_1, std::placeholders::_2))),
		m_fsmTransPreRunPV(nds::PVDelegateIn<std::int32_t>(
			"TRANS-PRE2RUN-RB",
			std::bind(&IFCFastIntPowerAvg::getInterlockStateTransitionPreRun,
					this, std::placeholders::_1, std::placeholders::_2))),
		m_vInputPV(nds::PVDelegateIn<std::int32_t>(
			"VINP-RB",
					std::bind(&IFCFastIntPowerAvg::getVinput, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_iInputPV(nds::PVDelegateIn<std::int32_t>(
					"IINP-RB",
					std::bind(&IFCFastIntPowerAvg::getIinput, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_winSizePV(nds::PVDelegateIn<std::int32_t>(
					"WINSIZE-RB",
					std::bind(&IFCFastIntPowerAvg::getWinSize, this,
										std::placeholders::_1, std::placeholders::_2))),
		m_maxinstpowerPV(nds::PVDelegateIn<double>(
					"MAX-INSTPWR-RB",
					std::bind(&IFCFastIntPowerAvg::getMaxInstPwr, this,
										std::placeholders::_1, std::placeholders::_2))),
		m_maxavgpowerPV(nds::PVDelegateIn<double>(
			"MAX-AVGPWR-RB",
			std::bind(&IFCFastIntPowerAvg::getMaxAvgPower, this,
					std::placeholders::_1, std::placeholders::_2)))
 {
		//m_node = parentNode.addChild(nds::Node(name));
		parentNode.addChild(m_node);

		m_stateMachine = nds::StateMachine(
		  true, std::bind(&IFCFastIntPowerAvg::switchOn, this),
		  std::bind(&IFCFastIntPowerAvg::switchOff, this),
		  std::bind(&IFCFastIntPowerAvg::start, this),
		  std::bind(&IFCFastIntPowerAvg::stop, this),
		  std::bind(&IFCFastIntPowerAvg::recover, this),
		  std::bind(&IFCFastIntPowerAvg::allowChange, this, std::placeholders::_1,
					std::placeholders::_2, std::placeholders::_3));
		m_node.addChild(m_stateMachine);
		m_stateMachine.setLogLevel(nds::logLevel_t::none);

		// m_dataPV.setMaxElements(CHANNEL_SAMPLES_MAX);
		m_dataPV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_dataPV);

		m_historyPV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_historyPV);

		m_ppqhistoryPV.setScanType(nds::scanType_t::interrupt);
			m_node.addChild(m_ppqhistoryPV);

		m_pp_outPV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_pp_outPV);

		/* Add PV for PP emulated ********************************************/
		m_node.addChild(nds::PVDelegateOut<std::int32_t>(
				"PP-EMUL", std::bind(&IFCFastIntPowerAvg::setPPEmulated, this,
														 std::placeholders::_1, std::placeholders::_2),
				std::bind(&IFCFastIntPowerAvg::getPPEmulated, this,
									std::placeholders::_1, std::placeholders::_2)));
		/* Readback PV */
		m_pp_emulatedPV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_pp_emulatedPV);

		/* Add PV for PP Mode **********************************************/
		m_node.addChild(nds::PVDelegateOut<std::int32_t>(
				"PP-MODE", std::bind(&IFCFastIntPowerAvg::setPPMode, this,
														 std::placeholders::_1, std::placeholders::_2),
				std::bind(&IFCFastIntPowerAvg::getPPMode, this,
									std::placeholders::_1, std::placeholders::_2)));
		/* Readback PV */
		m_pp_modePV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_pp_modePV);

		/* Add PV for CVAL **************************************************/
		m_node.addChild(nds::PVDelegateOut<double>(
				"PP-CVAL", std::bind(&IFCFastIntPowerAvg::setPPCVal, this,
														 std::placeholders::_1, std::placeholders::_2),
				std::bind(&IFCFastIntPowerAvg::getPPCVal, this,
									std::placeholders::_1, std::placeholders::_2)));
		/* Readback PV */
		m_pp_cvalPV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_pp_cvalPV);

		/* Add PV for Interlock Transisition qualifier **************************/
		m_node.addChild( nds::PVDelegateOut<std::int32_t>(
				"TRANS-IDLE2PRE",
				std::bind(&IFCFastIntPowerAvg::setInterlockStateTransitionIdlePre, this,
									std::placeholders::_1, std::placeholders::_2),
				std::bind(&IFCFastIntPowerAvg::getInterlockStateTransitionIdlePre, this,
									std::placeholders::_1, std::placeholders::_2)));
		/* Readback PV */
		m_fsmTransIdlePrePV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_fsmTransIdlePrePV);

		/* Add PV for Interlock Transisition qualifier *******************************/
		m_node.addChild( nds::PVDelegateOut<std::int32_t>(
				"TRANS-PRE2RUN",
				std::bind(&IFCFastIntPowerAvg::setInterlockStateTransitionPreRun, this,
									std::placeholders::_1, std::placeholders::_2),
				std::bind(&IFCFastIntPowerAvg::getInterlockStateTransitionPreRun, this,
									std::placeholders::_1, std::placeholders::_2)));
		/* Readback PV */
		m_fsmTransPreRunPV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_fsmTransPreRunPV);

		/* Add PV for Selecting Voltage Input of this PP  *************************/
		m_node.addChild(nds::PVDelegateOut<std::int32_t>(
				"VINP", std::bind(&IFCFastIntPowerAvg::setVinput, this,
														 std::placeholders::_1, std::placeholders::_2),
				std::bind(&IFCFastIntPowerAvg::getVinput, this,
									std::placeholders::_1, std::placeholders::_2)));
		/* Readback PV */
		m_vInputPV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_vInputPV);

		/* Add PV for Selecting Current Input of this PP  *************************/
		m_node.addChild(nds::PVDelegateOut<std::int32_t>(
				"IINP", std::bind(&IFCFastIntPowerAvg::setIinput, this,
														 std::placeholders::_1, std::placeholders::_2),
				std::bind(&IFCFastIntPowerAvg::getIinput, this,
									std::placeholders::_1, std::placeholders::_2)));
		/* Readback PV */
		m_iInputPV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_iInputPV);

		/* Add PV for measurement window size *************************/
		m_node.addChild(nds::PVDelegateOut<std::int32_t>(
				"WINSIZE", std::bind(&IFCFastIntPowerAvg::setWinSize, this,
														 std::placeholders::_1, std::placeholders::_2),
				std::bind(&IFCFastIntPowerAvg::getWinSize, this,
									std::placeholders::_1, std::placeholders::_2)));
		/* Readback PV */
		m_winSizePV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_winSizePV);

		/* Add PV for Maximum Instantaneous power *************************/
		m_node.addChild(nds::PVDelegateOut<double>(
				"MAX-INSTPWR", std::bind(&IFCFastIntPowerAvg::setMaxInstPwr, this,
														 std::placeholders::_1, std::placeholders::_2),
				std::bind(&IFCFastIntPowerAvg::getMaxInstPwr, this,
									std::placeholders::_1, std::placeholders::_2)));
		/* Readback PV */
		m_maxinstpowerPV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_maxinstpowerPV);

		/* Add PV for Maximum average power *************************/
		m_node.addChild(nds::PVDelegateOut<double>(
				"MAX-AVGPWR", std::bind(&IFCFastIntPowerAvg::setMaxAvgPower, this,
														 std::placeholders::_1, std::placeholders::_2),
				std::bind(&IFCFastIntPowerAvg::getMaxAvgPower, this,
									std::placeholders::_1, std::placeholders::_2)));
		/* Readback PV */
		m_maxavgpowerPV.setScanType(nds::scanType_t::interrupt);
		m_node.addChild(m_maxavgpowerPV);

		// TODO: set some default values from hardware ????????

 }


void IFCFastIntPowerAvg::addValue(int32_t data)
{

	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	//m_dataPV.push(now, data * m_linConvFactor + m_linConvOffset);
	m_dataPV.push(now, data);
}

void IFCFastIntPowerAvg::getData(timespec *timespec, double *value){}
void IFCFastIntPowerAvg::getHistory(timespec *timespec, std::vector<double> *value) {}
void IFCFastIntPowerAvg::getPPQHistory(timespec *timespec, std::vector<std::uint8_t> *value) {}

void IFCFastIntPowerAvg::historyAddValue(int32_t data) {}

void IFCFastIntPowerAvg::historyAddPPQValue(uint8_t data) {
	*m_ppqhistory_ptr = data;
	m_ppqhistory_ptr++;
}

void IFCFastIntPowerAvg::historyReset()
{
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	m_historyPV.push(now, m_history);
	m_history.clear();

	m_ppqhistoryPV.push(now, m_ppqhistory);
	m_ppqhistory.clear();

}

void IFCFastIntPowerAvg::reserve(size_t nframes)
{
	m_history.resize(nframes);
	m_history_ptr = m_history.begin();

	m_ppqhistory.resize(nframes);
	m_ppqhistory_ptr = m_ppqhistory.begin();

}

void IFCFastIntPowerAvg::setState(nds::state_t newState)
{
	m_stateMachine.setState(newState);
}

void IFCFastIntPowerAvg::setPPEmulated(const timespec &timespec, const int32_t &value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));
	option.emulation_en = value;

	/* Write PP configuration according to channel num and pp type */
	ifcfastint_set_conf_pwravg_pp(&m_deviceUser, m_pwravgNum,
								IFCFASTINT_ANALOG_EMULATION_EN_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_pp_emulatedPV.read(&now, &rb_val);
	m_pp_emulatedPV.push(now, rb_val);
}
void IFCFastIntPowerAvg::getPPEmulated(timespec *timespec, int32_t *value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));
	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option);

	clock_gettime(CLOCK_REALTIME, timespec);
	*value = option.emulation_en;
}

void IFCFastIntPowerAvg::setPPMode(const timespec &timespec, const int32_t &value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));

	/* Put MODE = ZERO if configuration is invalid */
	switch (value) {

		case 0:
		option.mode = ifcfastint_amode_zero;
		break;

		case 1:
		option.mode = ifcfastint_amode_one;
		break;

		case 2:
		option.mode = ifcfastint_amode_avg1;
		break;

		case 3:
		option.mode = ifcfastint_amode_avg2;
		break;

		default:
			option.mode = ifcfastint_amode_zero;
	}

	ifcfastint_set_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, IFCFASTINT_ANALOG_MODE_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_pp_modePV.read(&now, &rb_val);
	m_pp_modePV.push(now, rb_val);

}
void IFCFastIntPowerAvg::getPPMode(timespec *timespec, int32_t *value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));
	clock_gettime(CLOCK_REALTIME, timespec);
	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option);

	switch (option.mode)
	{
	case ifcfastint_amode_zero:
	*value = 0;
	break;
	case ifcfastint_amode_one:
	*value = 1;
	break;
	case ifcfastint_amode_avg1:
	*value = 2;
	break;
	case ifcfastint_amode_avg2:
	*value = 3;
	break;
	default:
	*value = 0;
	break;
	}
}

void IFCFastIntPowerAvg::setVinput(const timespec &timespec, const int32_t &value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));

	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option); // preserve option.i_in
	if ((value < 20) && (value >= 0)){
		option.u_in = (int16_t) value;
	}

	/* Write PP configuration according to channel num and pp type */
	ifcfastint_set_conf_pwravg_pp(&m_deviceUser, m_pwravgNum,
			IFCFASTINT_ANALOG_VAL2_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_vInputPV.read(&now, &rb_val);
	m_vInputPV.push(now, rb_val);
}
void IFCFastIntPowerAvg::getVinput(timespec *timespec, int32_t *value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));

	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option);

	clock_gettime(CLOCK_REALTIME, timespec);
	*value = option.u_in;
}

void IFCFastIntPowerAvg::setIinput(const timespec &timespec, const int32_t &value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));

	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option); // preserve option.u_in
	if ((value < 20) && (value >= 0)){
		option.i_in = value;
	}

	/* Write PP configuration according to channel num and pp type */
	ifcfastint_set_conf_pwravg_pp(&m_deviceUser, m_pwravgNum,
			IFCFASTINT_ANALOG_VAL2_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_iInputPV.read(&now, &rb_val);
	m_iInputPV.push(now, rb_val);
}
void IFCFastIntPowerAvg::getIinput(timespec *timespec, int32_t *value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));
	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option);

	clock_gettime(CLOCK_REALTIME, timespec);
	*value = option.i_in;
}

void IFCFastIntPowerAvg::setWinSize(const timespec &timespec, const int32_t &value)
{
		struct ifcfastint_pwravg_option option;
		memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));

		/* Maximum window size should be between 0 and 65536 */
		int32_t aux;

		aux = value;

		if (value < 0 ) {
			aux = 0;
		} else if (value >= 65536) {
			aux = 65535;
		}

		option.val1 = aux;

		/* Write PP configuration according to channel num and pp type */
		ifcfastint_set_conf_pwravg_pp(&m_deviceUser, m_pwravgNum,
				IFCFASTINT_ANALOG_VAL1_W, &option);

		// Trigger an update of the readback value from hardware
		int32_t rb_val;
		struct timespec now = {0, 0};
		m_winSizePV.read(&now, &rb_val);
		m_winSizePV.push(now, rb_val);

}
void IFCFastIntPowerAvg::getWinSize(timespec *timespec, int32_t *value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));
	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option);

	clock_gettime(CLOCK_REALTIME, timespec);
	*value = option.val1;
}

void IFCFastIntPowerAvg::setMaxInstPwr(const timespec &timespec, const double &value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));

	// TODO: for now, keep this value as 16 bit unsigned
	double aux;
	aux = value;

	if (value < 0.0) {
		aux = 0.0;
	} else if (value >= 65536.0) {
		aux = 65536.0;
	}

	option.val3 = signed_to_raw((int32_t) aux);

	ifcfastint_set_conf_pwravg_pp(&m_deviceUser, m_pwravgNum,
								IFCFASTINT_ANALOG_VAL3_W, &option);

	// Trigger an update of the readback value from hardware
	double rb_val;
	struct timespec now = {0, 0};
	m_maxinstpowerPV.read(&now, &rb_val);
	m_maxinstpowerPV.push(now, rb_val);
}
void IFCFastIntPowerAvg::getMaxInstPwr(timespec *timespec, double *value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));

	clock_gettime(CLOCK_REALTIME, timespec);
	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option);

	*value = (double) raw_to_signed(option.val3);
}

void IFCFastIntPowerAvg::setMaxAvgPower(const timespec &timespec, const double &value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));

	// TODO: for now, keep this value as 16 bit unsigned
	double aux;
	aux = value;

	if (value < 0.0) {
		aux = 0.0;
	} else if (value >= 65536.0) {
		aux = 65536.0;
	}

	option.val4 = signed_to_raw((int32_t) aux);

	ifcfastint_set_conf_pwravg_pp(&m_deviceUser, m_pwravgNum,
			IFCFASTINT_ANALOG_VAL4_W, &option);

	// Trigger an update of the readback value from hardware
	double rb_val;
	struct timespec now = {0, 0};
	m_maxavgpowerPV.read(&now, &rb_val);
	m_maxavgpowerPV.push(now, rb_val);
}
void IFCFastIntPowerAvg::getMaxAvgPower(timespec *timespec, double *value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));

	clock_gettime(CLOCK_REALTIME, timespec);
	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option);

	*value = (double) raw_to_signed(option.val4);
}

void IFCFastIntPowerAvg::setPPCVal(const timespec &timespec, const double &value) {}
void IFCFastIntPowerAvg::getPPCVal(timespec *timespec, double *value) {}

void IFCFastIntPowerAvg::setPPOutput(bool qout)
{
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	m_pp_outPV.push(now, (int32_t)qout);
}

void IFCFastIntPowerAvg::getPPQOut(timespec *timespec, int32_t *value) {}

void IFCFastIntPowerAvg::setInterlockStateTransitionIdlePre(const timespec &timespec, const int32_t &value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));
	option.idle2pre = value & 1;
	ifcfastint_set_conf_pwravg_pp(&m_deviceUser, m_pwravgNum,
								IFCFASTINT_ANALOG_IDLE2PRE_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_fsmTransIdlePrePV.read(&now, &rb_val);
	m_fsmTransIdlePrePV.push(now, rb_val);
}
void IFCFastIntPowerAvg::getInterlockStateTransitionIdlePre(timespec *timespec, int32_t *value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));
	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option);
	*value = option.idle2pre;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntPowerAvg::setInterlockStateTransitionPreRun(const timespec &timespec, const int32_t &value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));
	option.pre2run = value & 1;
	ifcfastint_set_conf_pwravg_pp(&m_deviceUser, m_pwravgNum,
			IFCFASTINT_ANALOG_PRE2RUN_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_fsmTransPreRunPV.read(&now, &rb_val);
	m_fsmTransPreRunPV.push(now, rb_val);
}

void IFCFastIntPowerAvg::getInterlockStateTransitionPreRun(timespec *timespec, int32_t *value)
{
	struct ifcfastint_pwravg_option option;
	memset(&option, 0, sizeof(struct ifcfastint_pwravg_option));
	ifcfastint_get_conf_pwravg_pp(&m_deviceUser, m_pwravgNum, &option);
	*value = option.pre2run;
	clock_gettime(CLOCK_REALTIME, timespec);
}

// void IFCFastIntPowerAvg::commitParameters(bool calledFromAcquisitionThread) {}

void IFCFastIntPowerAvg::switchOn()
{
	// // TODO: some parameter changed?
	// commitParameters();
}
void IFCFastIntPowerAvg::switchOff(){}
void IFCFastIntPowerAvg::start()
{
	// m_firstReadout = true;
}
void IFCFastIntPowerAvg::stop()
{
	// commitParameters();
}
void IFCFastIntPowerAvg::recover()
{
	// throw nds::StateMachineRollBack("Cannot recover");
}
bool IFCFastIntPowerAvg::allowChange(const nds::state_t, const nds::state_t, const nds::state_t)
{
	return true;
}


IFCFastIntPowerAvgGroup::IFCFastIntPowerAvgGroup(const std::string &name, nds::Node &parentNode, ifcdaqdrv_usr &deviceUser) :
	m_node(nds::Port(name,
	nds::nodeType_t::generic)),
	m_deviceUser(deviceUser)
{
	parentNode.addChild(m_node);

	m_PwrAvgChannels.push_back(std::make_shared<IFCFastIntPowerAvg>("PWRAVG0", m_node, 0, m_deviceUser));
	m_PwrAvgChannels.push_back(std::make_shared<IFCFastIntPowerAvg>("PWRAVG1", m_node, 1, m_deviceUser));
	m_PwrAvgChannels.push_back(std::make_shared<IFCFastIntPowerAvg>("PWRAVG2", m_node, 2, m_deviceUser));
	m_PwrAvgChannels.push_back(std::make_shared<IFCFastIntPowerAvg>("PWRAVG3", m_node, 3, m_deviceUser));

}

std::vector<std::shared_ptr<IFCFastIntPowerAvg>> IFCFastIntPowerAvgGroup::getChannels() {
	return m_PwrAvgChannels;
}

void IFCFastIntPowerAvgGroup::setState(nds::state_t newState) {
	for (auto channel : m_PwrAvgChannels) {
		channel->setState(newState);
	}
}
