TOP=../..

include $(TOP)/configure/CONFIG
#------------------------------------------------------------------------------
#  ADD MACRO DEFINITIONS AFTER THIS LINE
#==============================================================================

# Use typed rset structure (see 3.16.1 release notes)
USR_CPPFLAGS += -DUSE_TYPED_RSET

# C++11
USR_CXXFLAGS += -std=c++11

#------------------------------------------------------------------------------
# Build support library
#------------------------------------------------------------------------------
LIBRARY_IOC_linux-ppc64e6500 += fimiocSupport
LIBRARY_IOC += $(LIBRARY_IOC_$(T_A))

# Link locally-provided code into the support library,
# rather than directly into the IOC application, that
# causes problems on Windows DLL builds
fimiocSupport_SRCS += common.cpp
fimiocSupport_SRCS += device.cpp
fimiocSupport_SRCS += IFCFastIntInterlockFSM.cpp
fimiocSupport_SRCS += IFCFastIntDIChannel.cpp
fimiocSupport_SRCS += IFCFastIntAnalogPP.cpp
fimiocSupport_SRCS += IFCFastIntAIChannel.cpp
fimiocSupport_SRCS += IFCFastIntPowerAvg.cpp
fimiocSupport_SRCS += IFCFastIntSpecial.cpp
fimiocSupport_SRCS += IFCFastIntDIChannelGroup.cpp
fimiocSupport_SRCS += IFCFastIntAIChannelGroup.cpp
fimiocSupport_SRCS += interlock_info.cpp
fimiocSupport_SRCS += interlock_list.cpp

fimiocSupport_SRCS += IFCFastInt_aSub.c

fimiocSupport_LIBS += tsclib ifcdaqdrv
fimiocSupport_LIBS += scaling
fimiocSupport_LIBS += nds3 nds3epics
fimiocSupport_LIBS += $(EPICS_BASE_IOC_LIBS)

fimiocSupport_SRCS += fimioc_automation.st
fimiocSupport_LIBS += seq pv

DBD += fimiocSupport.dbd
fimiocSupport_DBD += fimioc_automation.dbd

#------------------------------------------------------------------------------
# Build IOC binary
#------------------------------------------------------------------------------
PROD_IOC_linux-ppc64e6500 += fimioc
PROD_IOC += $(PROD_IOC_$(T_A))

# fimioc.dbd will be created and installed
DBD += fimioc.dbd

# fimioc.dbd will be made up from these files:
fimioc_DBD += base.dbd

# Include dbd files from all support applications:
fimioc_DBD += asyn.dbd
fimioc_DBD += asSupport.dbd
fimioc_DBD += nds3epics.dbd
fimioc_DBD += IFCFastInt_aSub.dbd
fimioc_DBD += fimiocSupport.dbd

# Add dependency to support library
fimioc_LIBS += fimiocSupport

# Add all the support libraries needed by this IOC
fimioc_LIBS += scaling tsclib ifcdaqdrv
fimioc_LIBS += autosave seq pv
fimioc_LIBS += nds3 nds3epics asyn

# fimioc_registerRecordDeviceDriver.cpp derives from fimioc.dbd
fimioc_SRCS += fimioc_registerRecordDeviceDriver.cpp

# Build the main IOC entry point on workstation OSs.
fimioc_SRCS_DEFAULT += fimiocMain.cpp
fimioc_SRCS_vxWorks += -nil-

# Link QSRV (pvAccess Server) if available
ifdef EPICS_QSRV_MAJOR_VERSION
    fimioc_LIBS += qsrv
    fimioc_LIBS += $(EPICS_BASE_PVA_CORE_LIBS)
    fimioc_DBD += PVAServerRegister.dbd
    fimioc_DBD += qsrv.dbd
endif
fimioc_LIBS += $(EPICS_BASE_IOC_LIBS)

#==============================================================================

include $(TOP)/configure/RULES
#------------------------------------------------------------------------------
#  ADD RULES AFTER THIS LINE
