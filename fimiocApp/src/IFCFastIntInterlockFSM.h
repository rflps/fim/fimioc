/**
 * @file IFCFastIntInterlockFSM.h
 * @brief Header file defining the analog input channel group class.
 * @author nclaesson
 * @date 2016-03-29
 */

#ifndef IFCFASTINTINTERLOCKFSM_H
#define IFCFASTINTINTERLOCKFSM_H

#include <vector>
#include <map>
#include <mutex>


#include "epicsEvent.h"
#include "epicsMessageQueue.h"
#include "nds3/nds.h"
#include "eventTimer.h"
#include "common.h"


#include "IFCFastIntAIChannelGroup.h"
#include "IFCFastIntDIChannelGroup.h"
#include "IFCFastIntPowerAvg.h"
#include "IFCFastIntSpecial.h"

struct fimEventMsg {
	int evtType;
	int32_t readPointer;
};

struct interlockMsg {
	int evtType;
	void *pdata;
};

#define TOTAL_TIME
#define NEWMETHOD
#define USEASYNC

class IFCFastIntInterlockFSM {
public:
	IFCFastIntInterlockFSM(const std::string &name, nds::Node &parentNode,
							ifcdaqdrv_usr &deviceUser, std::string &calibFolder,
							size_t nframes, int dataformat);

	~IFCFastIntInterlockFSM();

	// Public parameters
	nds::Port m_node;
	nds::StateMachine m_stateMachine;
	ifcdaqdrv_usr &m_deviceUser;
	std::string &m_calibFolder;

	// Child nodes
	std::vector<std::shared_ptr<IFCFastIntAIChannelGroup> > m_AIGroup;
	std::shared_ptr<IFCFastIntDIChannelGroup> m_DIGroup;
	std::shared_ptr<IFCFastIntPowerAvgGroup> m_PwrAvgGroup;
	std::shared_ptr<IFCFastIntSpecialGroup> m_PwrRefGroup;
	std::shared_ptr<IFCFastIntSpecialGroup> m_CavDecGroup;

	// Delegate functions
	void sendCommand(const timespec &timespec, const int32_t &value);
	void recvCommand(timespec *timespec, int32_t *value);

	void getInfoMessage(timespec *timespec, std::string *value);

	void getPPLock(timespec *timespec, int32_t *value);
	// void setPPLock(const timespec &timespec, const int32_t &value);
	void updatePPLock();

	void getInterlockState(timespec *timespec, int32_t *value);

	void getHistMode(timespec *timespec, int32_t *value);
	void setHistMode(const timespec &timespec, const int32_t &value);

	void getHistEnabled(timespec *timespec, int32_t *value);
	void setHistEnabled(const timespec &timespec, const int32_t &value);

	void resetHistBuf(const timespec &timespec, const int32_t &value);
	void getresetHistBuf(timespec *timespec, int32_t *value);

	void setBkpTrigLine(const timespec &timespec, const int32_t &value);
	void getBkpTrigLine(timespec *timespec, int32_t *value);

	void resetAll(const timespec &timespec, const int32_t &value);
	void getResetAll(timespec *timespec, int32_t *value);

	void setAutoreset(const timespec &timespec, const int32_t &value);
	void getAutoreset(timespec *timespec, int32_t *value);

	void setAutoResetPeriod(const timespec &timespec, const double &value);
	void getAutoResetPeriod(timespec *timespec, double *value);

	void getFSMTrend(timespec *timespec, std::vector<std::int32_t> *value);

	void setFakeAnalogIn(const timespec &timespec, const int32_t &value);
	void getFakeAnalogIn(timespec *timespec, int32_t *value);

	void getGPDO(timespec *timespec, int32_t *value);
	void setGPDO(const timespec &timespec, const int32_t &value);

	void setHwTimestampStr(const timespec &timespec, const std::string& value);
	void getHwTimestampStr(timespec *timespec, std::string *value);

	// NDS State Machine Functions
	void onSwitchOn();
	void onSwitchOff();
	void onStart();
	void onStop();
	void recover();
	bool allowChange(const nds::state_t currentLocal,
									 const nds::state_t currentGlobal,
									 const nds::state_t nextLocal);



	void readFPGAinfo(void);
	double processScalarReads(void);
	double processFimStateMachine(void);
	void setInitOk() {
		m_InitComplete = true;
	}
	double resetInterlockCounter(void);


private:

	void initSystem();
	std::mutex updateMutex;

	const fim_dataformat_t m_DataFormat;

	bool m_InitComplete;

	epicsMessageQueue acqEvent;
	epicsMessageQueue ilckEvent;

	int8_t *m_rawData;
	int8_t m_rawMeasData[1024];

	calib_table_t m_calibTable;

	const size_t m_nFrames;

	int32_t m_fsmFrequency;
	uint32_t m_fsmOutChannelMask;
	bool m_ppconf_locked;

	int32_t m_HistMode;
	int32_t m_HistEnabled;
	int32_t m_HistState;
	bool m_HistModeChanged;
	bool m_HistEnabledChanged;

	int32_t m_ProcPeriod;
	int32_t m_IntrFreq;

	int m_BigEndian;
	int32_t m_bkpTriggerLine;
	bool m_bkpTriggerLineChanged;

	// Fast Recovery Window
	ifcfastint_fsm_state m_FIM_StateMachine;
	int32_t m_MaxNumInterlock;
	double m_AutoResetPeriod;
	double m_AutoResetPeriodQuantum;
	int32_t m_CurrentInterlockCount;
	bool m_skipNormalRead;

	// Timestamp from external device (EVR)
	struct timespec m_HwTimestamp;
	struct timespec m_HwTimestampUpdate;

	// --------------------------------------------------------
	static const unsigned int TimerThreadPriority = epicsThreadPriorityMedium;
	static const unsigned int autoResetThreadPriority = epicsThreadPriorityHigh;
	epicsTimerQueueActive  &timerQueue;  //<! queue used by timer thread to manage our timer events
	epicsTimerQueueActive  &autoResetQueue;  //<! queue used by timer thread to manage our timer events
	eventTimer  scalarReadsTimer;     //<! To periodically update scalar readings
	eventTimer  fimStateMachineTimer;     //<! To periodically update FIM state machine
	eventTimer	interlockResetTimer;

	// --------------------------------------------------------
	// PVs definitions
	// --------------------------------------------------------

	nds::PVDelegateIn<std::string> m_infoPV;

	nds::PVVariableIn<std::int32_t> m_nSamplesPV;
	nds::PVVariableIn<std::int32_t> m_fsmFrequencyPV;

	nds::PVDelegateIn<std::int32_t> m_intFSMPV;
	nds::PVDelegateIn<std::int32_t> m_pplockPV;
	nds::PVDelegateIn<std::int32_t> m_MainAutoResetPV;

	// History Buffer Management PVs
	nds::PVVariableIn<std::int32_t> m_HistModePV;
	nds::PVVariableIn<std::int32_t> m_HistEnabledPV;
	nds::PVVariableIn<std::int32_t> m_StatusRegPV;
	nds::PVVariableIn<std::int32_t> m_HistoryStatePV;
	nds::PVVariableIn<std::int32_t> m_HistOverFFPV;
	nds::PVVariableIn<std::int32_t> m_HistRingOverFFPV;

	// Process time tracking PVs
	nds::PVVariableOut<std::int32_t> m_LoopPeriod;
	nds::PVVariableIn<std::int32_t> m_LoopPeriodPV;
	nds::PVVariableIn<std::int32_t> m_ProcPeriodPV;

	nds::PVVariableIn<std::int32_t> m_bkpTriggerLinePV;
	nds::PVVariableIn<double> m_IntrFreqPV;
	nds::PVVariableIn<std::int32_t> m_QueueStatusPV;

	nds::PVVariableIn<std::int32_t> m_RawAI0PV;
	nds::PVVariableIn<std::int32_t> m_RawAI19PV;
	nds::PVVariableIn<std::int32_t> m_OutsMSBPV;
	nds::PVVariableIn<std::int32_t> m_OutsLSBPV;

	nds::PVVariableOut<std::int32_t> 	m_SelectRegisterPV;
	nds::PVVariableIn<std::int32_t> 	m_RawRegisterPV;

	nds::PVVariableOut<std::int32_t> 	m_SelectTMEMRegisterPV;
	nds::PVVariableIn<std::int32_t> 	m_RawTMEMRegisterLPV;
	nds::PVVariableIn<std::int32_t> 	m_RawTMEMRegisterHPV;

	// Auto-reset function
	nds::PVDelegateIn<double> 			m_AutoResetPeriodPV;
	nds::PVVariableOut<std::int32_t> 	m_AutoResetMaxIlckPV;
	nds::PVVariableIn<double> 			m_AutoResetDurationPV;
	nds::PVVariableIn<std::int32_t>		m_AutoResetIlckCountPV;

	nds::PVDelegateIn<std::vector<int32_t> > m_fsmHistoryBufferPV; // track the FSM history

	// special controllable digital output
	nds::PVVariableIn<std::int32_t> 			m_gpdoPV;

	/* Precondition PVs */
	nds::PVVariableIn<std::int32_t> m_HVONPreCondPV;
  	nds::PVVariableIn<std::int32_t> m_RFONPreCondPV;

	// External HW timestamp (EVR)
	nds::PVVariableIn<std::string> m_HwTimestampPV;
	nds::PVVariableIn<double> m_HwTimestampPeriodPV;
	nds::PVVariableOut<std::int32_t> m_HwTimestampOffsetNs;
	nds::PVVariableOut<std::int32_t> m_HwTimestampOK;

	nds::Thread m_ilckIntrThread;
	void ilckIntrThread();
	bool m_ilckIntrThreadRunning;

	nds::Thread m_interlockThread;
	void interlockLoop();
	bool m_interlockThreadRunning;

	nds::Thread m_acqIntrThread;
	void acqIntrThread();
	bool m_acqIntrThreadRunning;

	nds::Thread m_waveAcquisitionThread;
	void waveAcquisitionLoop(int32_t nsamples);
	bool m_waveAcquisitionThreadRunning;
};

#endif /* IFCFASTINTINTERLOCKFSM_H */
