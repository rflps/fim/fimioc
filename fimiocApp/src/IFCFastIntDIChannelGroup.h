/**
 * @file IFCFastIntDIChannelGroup.h
 * @brief Header file defining the analog input channel group class.
 * @author nclaesson
 * @date 2016-03-29
 */

#ifndef IFCFastIntDICHANNELGROUP_H
#define IFCFastIntDICHANNELGROUP_H

#include <nds3/nds.h>

#include "IFCFastIntDIChannel.h"

/**
 * @brief IFCFastIntDIChannelGroup
 */
class IFCFastIntDIChannelGroup {
public:
  IFCFastIntDIChannelGroup(const std::string &name, nds::Node &parentNode,
                           ifcdaqdrv_usr &deviceUser);

  nds::Port m_node;
  ifcdaqdrv_usr &m_deviceUser;
  uint32_t m_numChannels;
  std::vector<std::shared_ptr<IFCFastIntDIChannel>> m_DIChannels;
  std::vector<std::shared_ptr<IFCFastIntDIChannel>> getChannels();

  void getInfoMessage(timespec *timespec, std::string *value);
  void setState(nds::state_t newState);
  void setHwTimestamp(const timespec& ts);


private:
    /* Placeholder: external hardware timestamp */
    timespec m_HwTimestamp;
};

#endif /* IFCFastIntDICHANNELGROUP_H */
