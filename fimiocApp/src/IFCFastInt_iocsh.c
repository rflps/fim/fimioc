#include <stdio.h>

#include <epicsExport.h>
#include <iocsh.h>

#include "ifcdaqdrv.h"
#include "ifcfastintdrv.h"


void ifcfastintdrv_resetEEPROM(void)
{
	ifcdaqdrv_status status;
	struct ifcdaqdrv_usr pvt_deviceUser = {0, 1, 0};

	status = ifcdaqdrv_open_device(&pvt_deviceUser);

	if (status) {
		printf("Failed to access ifcdaqdrv library\n");
		return;
	}

	printf(" ---------- Reseting all analog inputs parameters on ADC3117 EEPROM ---------- \n");

	int channelNum;

	for (channelNum = 0; channelNum < 20; channelNum++) {

		printf("   [Channel %02d] Error Gain = 1.0 | Error Offset = 0.0 | EGU High = +10.0 | EGU Min = -10.0 \n", channelNum);
		ifcfastint_set_eeprom_param(&pvt_deviceUser, channelNum, ifcfastint_aichannel_ergain, 1.0);
		ifcfastint_set_eeprom_param(&pvt_deviceUser, channelNum, ifcfastint_aichannel_eroffset, 0.0);
		ifcfastint_set_eeprom_param(&pvt_deviceUser, channelNum, ifcfastint_aichannel_egumax, 10.0);
		ifcfastint_set_eeprom_param(&pvt_deviceUser, channelNum, ifcfastint_aichannel_egumin, -10.0);
	}

	printf(" ------------------------ ADC3117 EEPROM reset DONE --------------------------- \n");

	ifcdaqdrv_close_device(&pvt_deviceUser);
}

/* Information needed by iocsh */
static const iocshFuncDef helloFuncDef = {"ifcfastintdrv_resetEEPROM", 0, NULL};

/* Wrapper called by iocsh, selects the argument types that hello needs */
static void resetEEPROM_Func(const iocshArgBuf *args) {
    ifcfastintdrv_resetEEPROM();
}

/* Registration routine, runs at startup */
static void ifcfastintdrv_resetEEPROM_register(void) {
    iocshRegister(&helloFuncDef, resetEEPROM_Func);
}
epicsExportRegistrar(ifcfastintdrv_resetEEPROM_register);
