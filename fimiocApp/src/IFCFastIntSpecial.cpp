
#include <cstdlib>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <climits>
#include <algorithm>
#include <stdexcept>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>
#include <ifcfastintdrv.h>

#include "device.h"
#include "IFCFastIntSpecial.h"


inline int findADCCode(const double value, const std::vector<double>& scalingLut) {
    for (int i = scalingLut.size() - 1; i >= 0; --i) {
        if (scalingLut[i] <= value) // Stop at the largest ADC code <= value
            return i;
    }
    return 0; // Default to the lowest ADC code if nothing is found
}

IFCFastIntSpecial::IFCFastIntSpecial(const std::string &name, nds::Node &parentNode, struct ifcdaqdrv_usr &deviceUser,
									int block, calib_table_t &table, const fim_dataformat_t &dataformat,
									const fim_special_ilck_t &ilcktype) :
	m_block(block),
	m_deviceUser(deviceUser),
	m_DataFormat(dataformat),
	m_SpecialIlckType(ilcktype),
	m_CalibTable(table),
	m_ilckStatus(false), m_HVONprecond(false), m_RFONprecond(false),
	m_TriggerChannel(0), m_MeasChannel(0), m_bpTrigger(0),
	m_HwTimestamp({0,0}),
	m_ppqhistoryPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
		"PPQHistory", std::bind(&IFCFastIntSpecial::getPPQOut, this, std::placeholders::_1, std::placeholders::_2))),
	m_ppq_pmortemPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
		"PPQHistPMortem", std::bind(&IFCFastIntSpecial::getPPQOut, this, std::placeholders::_1, std::placeholders::_2))),
	m_winmaskpmortemPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
		"PPQWinMaskPMortem", std::bind(&IFCFastIntSpecial::getPPQOut, this, std::placeholders::_1, std::placeholders::_2))),
	m_trigpmortemPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
		"PPQTrigPMortem", std::bind(&IFCFastIntSpecial::getPPQOut, this, std::placeholders::_1, std::placeholders::_2))),
	m_winmaskPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
		"WinMask-RB", std::bind(&IFCFastIntSpecial::getWinMask, this, std::placeholders::_1, std::placeholders::_2))),
	m_winmeasPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
		"WinMeas-RB", std::bind(&IFCFastIntSpecial::getWinMeas, this, std::placeholders::_1, std::placeholders::_2))),
	m_trigPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
		"Trigger-RB", std::bind(&IFCFastIntSpecial::getTrig, this, std::placeholders::_1, std::placeholders::_2))),
	m_trig_offPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
		"TriggerOff-RB", std::bind(&IFCFastIntSpecial::getTrigOff, this, std::placeholders::_1, std::placeholders::_2))),

	m_activePV(nds::PVDelegateIn<std::int32_t>(
		"ACTIVE-RB", std::bind(&IFCFastIntSpecial::getActive, this, std::placeholders::_1, std::placeholders::_2))),
	m_autoresetPV(nds::PVDelegateIn<std::int32_t>(
		"AUTORESET-RB", std::bind(&IFCFastIntSpecial::getAutoReset, this, std::placeholders::_1, std::placeholders::_2))),
	m_modePV(nds::PVDelegateIn<std::int32_t>(
		"MODE-RB", std::bind(&IFCFastIntSpecial::getPPMode, this, std::placeholders::_1, std::placeholders::_2))),
	m_statePV(nds::PVDelegateIn<std::int32_t>(
		"STATE-RB", std::bind(&IFCFastIntSpecial::getState, this, std::placeholders::_1, std::placeholders::_2))),
	m_trigSelPV(nds::PVDelegateIn<std::int32_t>(
		"TRIGSEL-RB", std::bind(&IFCFastIntSpecial::getTriggerChannel, this, std::placeholders::_1, std::placeholders::_2))),
	m_measSelPV(nds::PVDelegateIn<std::int32_t>(
		"MEASSEL-RB", std::bind(&IFCFastIntSpecial::getMeasChannel, this, std::placeholders::_1, std::placeholders::_2))),

	m_maskPeriodPV(nds::PVDelegateIn<std::int32_t>(
		"MASKPER-RB", std::bind(&IFCFastIntSpecial::getMaskPeriod, this, std::placeholders::_1, std::placeholders::_2))),
	m_measPeriodPV(nds::PVDelegateIn<std::int32_t>(
		"MEASPER-RB", std::bind(&IFCFastIntSpecial::getMeasPeriod, this, std::placeholders::_1, std::placeholders::_2))),

	m_threTriggerPV(nds::PVDelegateIn<double>(
		"THRETRIG-RB", std::bind(&IFCFastIntSpecial::getThreTrigger, this, std::placeholders::_1, std::placeholders::_2))),
	m_threMeasPV(nds::PVDelegateIn<double>(
		"THREMEAS-RB", std::bind(&IFCFastIntSpecial::getThreInterlock, this, std::placeholders::_1, std::placeholders::_2))),

	m_fsmTransIdlePrePV(nds::PVDelegateIn<std::int32_t>(
		  "TRANS-IDLE2PRE-RB",
		  std::bind(&IFCFastIntSpecial::getInterlockStateTransitionIdlePre,
					this, std::placeholders::_1, std::placeholders::_2))),

	m_fsmTransPreRunPV(nds::PVDelegateIn<std::int32_t>(
		  "TRANS-PRE2RUN-RB",
		  std::bind(&IFCFastIntSpecial::getInterlockStateTransitionPreRun,
					this, std::placeholders::_1, std::placeholders::_2))),
	m_bpOffTriggerPV(nds::PVDelegateIn<std::int32_t>(
		"BPOFFTRIG-RB", std::bind(&IFCFastIntSpecial::getBpOffTrigLine, this, std::placeholders::_1, std::placeholders::_2))),
	m_bpOnTriggerPV(nds::PVDelegateIn<std::int32_t>(
		"BPONTRIG-RB", std::bind(&IFCFastIntSpecial::getBpOnTrigLine, this, std::placeholders::_1, std::placeholders::_2))),

	m_bpOffTriggerEnablePV(nds::PVDelegateIn<std::int32_t>(
		"BPOFFTRIGENA-RB", std::bind(&IFCFastIntSpecial::getBpOffTrigEna, this, std::placeholders::_1, std::placeholders::_2))),

	m_bpOnTriggerEnablePV(nds::PVDelegateIn<std::int32_t>(
		"BPONTRIGENA-RB", std::bind(&IFCFastIntSpecial::getBpOnTrigEna, this, std::placeholders::_1, std::placeholders::_2))),

	m_cavDecEvalTimePV(nds::PVDelegateIn<std::int32_t>(
		"CAVDECTIME-RB", std::bind(&IFCFastIntSpecial::getCavDecEvalTime, this, std::placeholders::_1, std::placeholders::_2))),

	m_cavDecDevRatePV(nds::PVDelegateIn<std::int32_t>(
		"CAVDECRATE-RB", std::bind(&IFCFastIntSpecial::getCavDecDevRate, this, std::placeholders::_1, std::placeholders::_2))),

	m_cavDecLimitPV(nds::PVDelegateIn<double>(
		"CAVDECLIMIT-RB", std::bind(&IFCFastIntSpecial::getCavDevDetecLimit, this, std::placeholders::_1, std::placeholders::_2))),

	m_cavDecOffsetPV(nds::PVDelegateIn<double>(
		"CAVDECOFFSET-RB", std::bind(&IFCFastIntSpecial::getCavDecOffset, this, std::placeholders::_1, std::placeholders::_2))),

	m_ilckStatusPV(nds::PVVariableIn<std::int32_t>("ilckStatus")),
	m_HVONPreCondPV(nds::PVVariableIn<std::int32_t>("HVONPreCond")),
	m_RFONPreCondPV(nds::PVVariableIn<std::int32_t>("RFONPreCond")),
	m_firstIlckPV(nds::PVVariableIn<std::int32_t>("PPQOUT"))

{
	m_node = parentNode.addChild(nds::Node(name));

  	m_stateMachine = nds::StateMachine(
	  true, std::bind(&IFCFastIntSpecial::switchOn, this),
	  std::bind(&IFCFastIntSpecial::switchOff, this),
	  std::bind(&IFCFastIntSpecial::start, this),
	  std::bind(&IFCFastIntSpecial::stop, this),
	  std::bind(&IFCFastIntSpecial::recover, this),
	  std::bind(&IFCFastIntSpecial::allowChange, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

	m_node.addChild(m_stateMachine);
	m_stateMachine.setLogLevel(nds::logLevel_t::none);

	// Add the input-only PVs to the NDS tree
	m_ppqhistoryPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_ppqhistoryPV);
	m_ppq_pmortemPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_ppq_pmortemPV);

	m_winmaskpmortemPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_winmaskpmortemPV);
	m_trigpmortemPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_trigpmortemPV);

	m_winmaskPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_winmaskPV);
	m_winmeasPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_winmeasPV);
	m_trigPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_trigPV);
	m_trig_offPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_trig_offPV);

	// Add the output and readback PVs to the NDS tree

	nds::PVDelegateOut<std::int32_t> node = nds::PVDelegateOut<std::int32_t>("ACTIVE",
		std::bind(&IFCFastIntSpecial::setActive, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getActive, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_activePV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_activePV);

	node = nds::PVDelegateOut<std::int32_t>("RESET",
		std::bind(&IFCFastIntSpecial::resetInterlock, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getresetInterlock, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);

	node = nds::PVDelegateOut<std::int32_t>("AUTORESET",
		std::bind(&IFCFastIntSpecial::setAutoReset, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getAutoReset, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_autoresetPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_autoresetPV);

	node = nds::PVDelegateOut<std::int32_t>("MODE",
		std::bind(&IFCFastIntSpecial::setPPMode, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getPPMode, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_modePV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_modePV);

  	m_statePV.setScanType(nds::scanType_t::periodic);
  	m_node.addChild(m_statePV);

	node = nds::PVDelegateOut<std::int32_t>("TRIGSEL",
		std::bind(&IFCFastIntSpecial::setTriggerChannel, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getTriggerChannel, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_trigSelPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_trigSelPV);

	node = nds::PVDelegateOut<std::int32_t>("MEASSEL",
		std::bind(&IFCFastIntSpecial::setMeasChannel, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getMeasChannel, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_measSelPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_measSelPV);

	node = nds::PVDelegateOut<std::int32_t>("MASKPER",
		std::bind(&IFCFastIntSpecial::setMaskPeriod, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getMaskPeriod, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_maskPeriodPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_maskPeriodPV);

	node = nds::PVDelegateOut<std::int32_t>("MEASPER",
		std::bind(&IFCFastIntSpecial::setMeasPeriod, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getMeasPeriod, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_measPeriodPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_measPeriodPV);

	nds::PVDelegateOut<double> node_d = nds::PVDelegateOut<double>("THRETRIG",
		std::bind(&IFCFastIntSpecial::setThreTrigger, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getThreTrigger, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node_d);
  	//Readback PV
  	m_threTriggerPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_threTriggerPV);

	node_d = nds::PVDelegateOut<double>("THREMEAS",
		std::bind(&IFCFastIntSpecial::setThreInterlock, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getThreInterlock, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node_d);
  	//Readback PV
  	m_threMeasPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_threMeasPV);

	node = nds::PVDelegateOut<std::int32_t>("TRANS-IDLE2PRE",
		std::bind(&IFCFastIntSpecial::setInterlockStateTransitionIdlePre, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getInterlockStateTransitionIdlePre, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_fsmTransIdlePrePV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_fsmTransIdlePrePV);

	node = nds::PVDelegateOut<std::int32_t>("TRANS-PRE2RUN",
		std::bind(&IFCFastIntSpecial::setInterlockStateTransitionPreRun, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getInterlockStateTransitionPreRun, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_fsmTransPreRunPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_fsmTransPreRunPV);

	node = nds::PVDelegateOut<std::int32_t>("BPOFFTRIG",
		std::bind(&IFCFastIntSpecial::setBpOffTrigLine, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getBpOffTrigLine, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_bpOffTriggerPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_bpOffTriggerPV);

	node = nds::PVDelegateOut<std::int32_t>("BPONTRIG",
		std::bind(&IFCFastIntSpecial::setBpOnTrigLine, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getBpOnTrigLine, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_bpOnTriggerPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_bpOnTriggerPV);

	node = nds::PVDelegateOut<std::int32_t>("BPONTRIGENA",
		std::bind(&IFCFastIntSpecial::setBpOnTrigEna, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getBpOnTrigEna, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_bpOnTriggerEnablePV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_bpOnTriggerEnablePV);

	node = nds::PVDelegateOut<std::int32_t>("BPOFFTRIGENA",
		std::bind(&IFCFastIntSpecial::setBpOffTrigEna, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getBpOffTrigEna, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_bpOffTriggerEnablePV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_bpOffTriggerEnablePV);

	node = nds::PVDelegateOut<std::int32_t>("CAVDECTIME",
		std::bind(&IFCFastIntSpecial::setCavDecEvalTime, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getCavDecEvalTime, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_cavDecEvalTimePV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_cavDecEvalTimePV);

	node = nds::PVDelegateOut<std::int32_t>("CAVDECRATE",
		std::bind(&IFCFastIntSpecial::setCavDecDevRate, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getCavDecDevRate, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node);
  	//Readback PV
  	m_cavDecDevRatePV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_cavDecDevRatePV);

	node_d = nds::PVDelegateOut<double>("CAVDECLIMIT",
		std::bind(&IFCFastIntSpecial::setCavDevDetecLimit, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getCavDevDetecLimit, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node_d);
  	//Readback PV
  	m_cavDecLimitPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_cavDecLimitPV);

	node_d = nds::PVDelegateOut<double>("CAVDECOFFSET",
		std::bind(&IFCFastIntSpecial::setCavDecOffset, this, std::placeholders::_1, std::placeholders::_2),
	  	std::bind(&IFCFastIntSpecial::getCavDecOffset, this, std::placeholders::_1, std::placeholders::_2));
  	m_node.addChild(node_d);
  	//Readback PV
  	m_cavDecOffsetPV.setScanType(nds::scanType_t::interrupt);
  	m_node.addChild(m_cavDecOffsetPV);

	m_node.addChild(m_ilckStatusPV);
	m_node.addChild(m_HVONPreCondPV);
	m_node.addChild(m_RFONPreCondPV);
	m_node.addChild(m_firstIlckPV);

	// Initial reset and activation
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	// get current status registers
	if (m_SpecialIlckType == fim_reflected_power) {
		ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
		if (option.mask_time == 0)
			option.mask_time = 1;
	}

	option.reset = 1;
	option.autoreset = 0;
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, (IFCFASTINT_SPECIALPP_RESET_W | IFCFASTINT_SPECIALPP_AUTORESET_W), &option);

	option.reset = 0;
	option.active = 1;
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block,
		(IFCFASTINT_SPECIALPP_RESET_W | IFCFASTINT_SPECIALPP_ACTIVE_W | IFCFASTINT_SPECIALPP_MASKTIME_W), &option);

	// Interlock info object (used mostly for auto-reset feature)
	m_InterlockInfo = std::make_shared<FIM_InterlockInfo>("ILCK", m_node, m_block, (m_SpecialIlckType == fim_reflected_power ? fim_special_rp_in : fim_special_cd_in));

}

void IFCFastIntSpecial::switchOn() {}
void IFCFastIntSpecial::switchOff() {}
void IFCFastIntSpecial::start() {}
void IFCFastIntSpecial::stop() {}
void IFCFastIntSpecial::recover() {}
bool IFCFastIntSpecial::allowChange(const nds::state_t, const nds::state_t, const nds::state_t) {
	return true;
}
void IFCFastIntSpecial::setState(nds::state_t newState) {
	m_stateMachine.setState(newState);
}

void IFCFastIntSpecial::getPPQOut(timespec *timespec, std::vector<std::uint8_t> *value) {}
void IFCFastIntSpecial::getWinMask(timespec *timespec, std::vector<std::uint8_t> *value) {}
void IFCFastIntSpecial::getWinMeas(timespec *timespec, std::vector<std::uint8_t> *value) {}
void IFCFastIntSpecial::getTrig(timespec *timespec, std::vector<std::uint8_t> *value) {}
void IFCFastIntSpecial::getTrigOff(timespec *timespec, std::vector<std::uint8_t> *value) {}

void IFCFastIntSpecial::setPPMode(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
  	switch (value) {
  		case 0:
  		case 1:
    		option.mode = value;
    		break;
		case 2:
    		option.mode = 3;
    		break;
  		default:
    		option.mode = 0;
    		break;
	}

	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_MODE_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_modePV.read(&now, &rb_val);
	m_modePV.push(now, rb_val);
}

void IFCFastIntSpecial::getPPMode(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	clock_gettime(CLOCK_REALTIME, timespec);
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);

	if (option.mode == 3)
		*value = 2;
	else
		*value = (int32_t) option.mode;
}

void IFCFastIntSpecial::getState(timespec *timespec, int32_t *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	ifcfastint_get_refpwr_state(&m_deviceUser, m_block, (uint32_t*) value);
}

void IFCFastIntSpecial::setActive(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	option.active = (uint8_t) value;
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_ACTIVE_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_activePV.read(&now, &rb_val);
	m_activePV.push(now, rb_val);
}
void IFCFastIntSpecial::getActive(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.active;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntSpecial::resetInterlock(const timespec &timespec, const int32_t &value) {

	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	uint8_t mode_, autoreset_;
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	mode_ = option.mode;
	autoreset_ = option.autoreset;

	option.autoreset = 1;
	option.mode = 1;
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, (IFCFASTINT_SPECIALPP_AUTORESET_W | IFCFASTINT_SPECIALPP_MODE_W), &option);

	if (m_InterlockInfo->isAutoReset())
		option.autoreset = 1;
	else
		option.autoreset = autoreset_;
	option.mode = mode_;
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, (IFCFASTINT_SPECIALPP_AUTORESET_W | IFCFASTINT_SPECIALPP_MODE_W), &option);

	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	m_firstIlckPV.push(now, IFCFASTINT_CH_OK);

	int32_t rb_val;
	m_autoresetPV.read(&now, &rb_val);
	m_autoresetPV.push(now, rb_val);
}

void IFCFastIntSpecial::getresetInterlock(timespec *timespec, int32_t *value) {}

void IFCFastIntSpecial::setAutoReset(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	option.autoreset = (uint8_t) value;
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_AUTORESET_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_autoresetPV.read(&now, &rb_val);
	m_autoresetPV.push(now, rb_val);

	clock_gettime(CLOCK_REALTIME, &now);
	m_firstIlckPV.push(now, IFCFASTINT_CH_OK);
}

void IFCFastIntSpecial::getAutoReset(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.autoreset;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntSpecial::setTriggerChannel(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	option.trig_channel = (uint8_t) (value < 20 ? value : 0);
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_TRIGSEL_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_trigSelPV.read(&now, &rb_val);
	m_trigSelPV.push(now, rb_val);
}
void IFCFastIntSpecial::getTriggerChannel(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.trig_channel;
	m_TriggerChannel = (int) option.trig_channel;
	clock_gettime(CLOCK_REALTIME, timespec);

}

void IFCFastIntSpecial::setMeasChannel(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	option.proc_channel = (uint8_t) (value < 20 ? value : 0);
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_CHANSEL_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_measSelPV.read(&now, &rb_val);
	m_measSelPV.push(now, rb_val);
}
void IFCFastIntSpecial::getMeasChannel(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.proc_channel;
	m_MeasChannel = (int) option.proc_channel;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntSpecial::setMaskPeriod(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	if ((m_SpecialIlckType == fim_reflected_power) and (value <= 0)) {
		option.mask_time = 1;
	} else {
		option.mask_time = (value < 0) ? 0 : (uint16_t) value;
	}

	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_MASKTIME_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_maskPeriodPV.read(&now, &rb_val);
	m_maskPeriodPV.push(now, rb_val);
}
void IFCFastIntSpecial::getMaskPeriod(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.mask_time;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntSpecial::setMeasPeriod(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	option.meas_time = (uint16_t) value;
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_MEASTIME_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_measPeriodPV.read(&now, &rb_val);
	m_measPeriodPV.push(now, rb_val);
}
void IFCFastIntSpecial::getMeasPeriod(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.meas_time;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntSpecial::setThreTrigger(const timespec &timespec, const double &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcdaqdrv_status status;

	int adc_counts = findADCCode(value, m_CalibTable.at(m_TriggerChannel));
	if (m_DataFormat == fim_dataformat_signed) {
		option.threshold_trig = adc_counts - 32768;
		status = ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_THRETRIG_W, &option);
		if (status) {
			ndsErrorStream(m_node) << "Failed to set PP Val1" << std::endl;
		}
	} else {
		option.threshold_trig_ui16 = (adc_counts < 65536 ? adc_counts : 65535);
		status = ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_THRETRIG_UNSIGNED_W, &option);
		if (status) {
			ndsErrorStream(m_node) << "Failed to set PP Val1" << std::endl;
		}
	}

	double rb_val;
	struct timespec now = {0, 0};
	m_threTriggerPV.read(&now, &rb_val);
	m_threTriggerPV.push(now, rb_val);
}

void IFCFastIntSpecial::getThreTrigger(timespec *timespec, double *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	clock_gettime(CLOCK_REALTIME, timespec);

	size_t index;
	double aux;
	if (m_DataFormat == fim_dataformat_signed) {
		index = static_cast<size_t>((option.threshold_trig+32768) & 0xffff);
		aux = m_CalibTable.at(m_TriggerChannel).at(index);
	} else {
		index = static_cast<size_t>(option.threshold_trig_ui16);
		aux = m_CalibTable.at(m_TriggerChannel).at(index);
	}

	// Get EGU value from calibration table
	*value = aux;
}

void IFCFastIntSpecial::setThreInterlock(const timespec &timespec, const double &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcdaqdrv_status status;

	int adc_counts = findADCCode(value, m_CalibTable.at(m_MeasChannel));
	if (m_DataFormat == fim_dataformat_signed) {
		option.threshold_ilck = (int)adc_counts - 32768;
		status = ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_THREMEAS_W, &option);
		if (status) {
			ndsErrorStream(m_node) << "Failed to set PP Val1" << std::endl;
		}
	} else {
		option.threshold_ilck_ui16 = (adc_counts < 65536 ? adc_counts : 65535);
		status = ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_THREMEAS_UNSIGNED_W, &option);
		if (status) {
			ndsErrorStream(m_node) << "Failed to set PP Val1" << std::endl;
		}
	}

	double rb_val;
	struct timespec now = {0, 0};
	m_threMeasPV.read(&now, &rb_val);
	m_threMeasPV.push(now, rb_val);
}
void IFCFastIntSpecial::getThreInterlock(timespec *timespec, double *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	clock_gettime(CLOCK_REALTIME, timespec);

	size_t index;
	double aux;
	if (m_DataFormat == fim_dataformat_signed) {
		index = static_cast<size_t>((option.threshold_ilck+32768) & 0xffff);
		aux = m_CalibTable.at(m_MeasChannel).at(index);
	} else {
		index = static_cast<size_t>(option.threshold_ilck_ui16);
		aux = m_CalibTable.at(m_MeasChannel).at(index);
	}

	// Get EGU value from calibration table
	*value = aux;
}

// DEPRECATED
void IFCFastIntSpecial::setCavDevDetecLimit(const timespec &timespec, const double &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	double rb_val;
	struct timespec now = {0, 0};
	m_cavDecLimitPV.read(&now, &rb_val);
	m_cavDecLimitPV.push(now, rb_val);
}

// DEPRECATED
void IFCFastIntSpecial::getCavDevDetecLimit(timespec *timespec, double *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = 0.0;
}

void IFCFastIntSpecial::setCavDecOffset(const timespec &timespec, const double &value) {
	int16_t adc_counts = 0;
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcdaqdrv_status status;

	int16_t aux = static_cast<int16_t>(value);
	if (aux < -32768) {
		adc_counts = -32768;
	} else if (aux > 32767) {
		adc_counts = 32767;
	} else {
		adc_counts = aux;
	}

	if (m_DataFormat == fim_dataformat_signed) {
		option.cavdec_limit = 0;
		option.cavdec_offset = adc_counts;
		status = ifcfastint_set_conf_refpwr(&m_deviceUser, m_block,
			(IFCFASTINT_SPECIALPP_CAVDEC_OFFSET_W | IFCFASTINT_SPECIALPP_CAVDEC_LIMIT_W),
			&option);
		if (status) {
			ndsErrorStream(m_node) << "Failed to set PP Val1" << std::endl;
		}
	}

	double rb_val;
	struct timespec now = {0, 0};
	m_cavDecOffsetPV.read(&now, &rb_val);
	m_cavDecOffsetPV.push(now, rb_val);
}

void IFCFastIntSpecial::getCavDecOffset(timespec *timespec, double *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	clock_gettime(CLOCK_REALTIME, timespec);

	*value = static_cast<double>(option.cavdec_offset);
}



void IFCFastIntSpecial::setInterlockStateTransitionIdlePre(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
  	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
  	option.idle2pre = value;
  	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_IDLE2PRE_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_fsmTransIdlePrePV.read(&now, &rb_val);
	m_fsmTransIdlePrePV.push(now, rb_val);
}




void IFCFastIntSpecial::getInterlockStateTransitionIdlePre(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.idle2pre;
	clock_gettime(CLOCK_REALTIME, timespec);

	/* Update current status of the condition */
	if (*value) {
		m_HVONenabled = true;
	} else {
		m_HVONenabled = false;
	}
}

void IFCFastIntSpecial::setInterlockStateTransitionPreRun(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
  	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
  	option.pre2run = value;
  	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_PRE2RUN_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_fsmTransPreRunPV.read(&now, &rb_val);
	m_fsmTransPreRunPV.push(now, rb_val);
}
void IFCFastIntSpecial::getInterlockStateTransitionPreRun(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.pre2run;
	clock_gettime(CLOCK_REALTIME, timespec);

	/* Update current status of the condition */
	if (*value) {
		m_RFONenabled = true;
	} else {
		m_RFONenabled = false;
	}
}

void IFCFastIntSpecial::historyAddValues(uint8_t ppq, uint8_t winmask, uint8_t winmeas, uint8_t trig, uint8_t trigoff) {
	*m_ppqhistory_ptr = ppq;
	*m_winmask_ptr = winmask;
	*m_winmeas_ptr = winmeas;
	*m_trig_ptr = trig;
	*m_trigoff_ptr = trigoff;

	m_ppqhistory_ptr++;
	m_winmask_ptr++;
	m_winmeas_ptr++;
	m_trig_ptr++;
	m_trigoff_ptr++;
}

void IFCFastIntSpecial::historyReset(bool pmortem) {
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	if (pmortem == true) {
		m_ppq_pmortemPV.push(now, m_ppqhistory);
		m_winmaskpmortemPV.push(now, m_winmask);
		m_trigpmortemPV.push(now, m_trig);
	}

	m_ppqhistoryPV.push(now, m_ppqhistory);
	m_winmaskPV.push(now, m_winmask);
	m_winmeasPV.push(now, m_winmeas);
	m_trigPV.push(now, m_trig);

	if (m_SpecialIlckType == fim_cavity_decay) {
		m_trig_offPV.push(now, m_trigoff);
	}

	/* Before reset PPQ (interlock status) vector, generate slow update status */
	if (std::find(m_ppqhistory.begin(), m_ppqhistory.end(), 0) != m_ppqhistory.end()) {
		m_ilckStatus = false;
	} else {
		m_ilckStatus = true;
	}
	m_ilckStatusPV.push(now, static_cast<int32_t>(m_ilckStatus));

	// Update the precond PVs
	m_HVONprecond = (m_HVONenabled == true ? m_ilckStatus : true);
	m_HVONPreCondPV.push(now, static_cast<int32_t>(m_HVONprecond));

	m_RFONprecond = (m_RFONenabled == true ? m_ilckStatus : true);
	m_RFONPreCondPV.push(now, static_cast<int32_t>(m_RFONprecond));

	m_ppqhistory.clear();
	m_winmask.clear();
	m_winmeas.clear();
	m_trig.clear();
	m_trigoff.clear();
}


void IFCFastIntSpecial::reserve(size_t nframes) {
	m_ppqhistory.resize(nframes);
	m_ppqhistory_ptr = m_ppqhistory.begin();

	m_winmask.resize(nframes);
	m_winmask_ptr = m_winmask.begin();

	m_winmeas.resize(nframes);
	m_winmeas_ptr = m_winmeas.begin();

	m_trig.resize(nframes);
	m_trig_ptr = m_trig.begin();

	m_trigoff.resize(nframes);
	m_trigoff_ptr = m_trigoff.begin();
}

void IFCFastIntSpecial::setBpOffTrigLine(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	option.bptrigger_off = (uint8_t) (value & 0x07);
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_BPTRIGGEREND_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_bpOffTriggerPV.read(&now, &rb_val);
	m_bpOffTriggerPV.push(now, rb_val);
}
void IFCFastIntSpecial::getBpOffTrigLine(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.bptrigger_off;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntSpecial::setBpOnTrigLine(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	option.bptrigger_on = (uint8_t) (value & 0x07);
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_BPTRIGSTART_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_bpOnTriggerPV.read(&now, &rb_val);
	m_bpOnTriggerPV.push(now, rb_val);
}
void IFCFastIntSpecial::getBpOnTrigLine(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.bptrigger_on;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntSpecial::setBpOffTrigEna(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	option.bptrigger_off_en = (uint8_t) (value & 0x01);
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_BPTRIGEND_EN_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_bpOffTriggerEnablePV.read(&now, &rb_val);
	m_bpOffTriggerEnablePV.push(now, rb_val);
}
void IFCFastIntSpecial::getBpOffTrigEna(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.bptrigger_off_en;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntSpecial::setBpOnTrigEna(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	option.bptrigger_on_en = (uint8_t) (value & 0x01);
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_BPTRIGSTART_EN_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_bpOnTriggerEnablePV.read(&now, &rb_val);
	m_bpOnTriggerEnablePV.push(now, rb_val);
}
void IFCFastIntSpecial::getBpOnTrigEna(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.bptrigger_on_en;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntSpecial::setCavDecEvalTime(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	option.devtime = (uint8_t) (value & 0x0f);
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_CAVDEC_TIME_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_cavDecEvalTimePV.read(&now, &rb_val);
	m_cavDecEvalTimePV.push(now, rb_val);
}
void IFCFastIntSpecial::getCavDecEvalTime(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.devtime;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntSpecial::setCavDecDevRate(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	option.devrate = (uint8_t) (value & 0x03);
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_CAVDEC_RATE_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_cavDecDevRatePV.read(&now, &rb_val);
	m_cavDecDevRatePV.push(now, rb_val);
}
void IFCFastIntSpecial::getCavDecDevRate(timespec *timespec, int32_t *value) {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));
	ifcfastint_get_conf_refpwr(&m_deviceUser, m_block, &option);
	*value = option.devrate;
	clock_gettime(CLOCK_REALTIME, timespec);
}

IFCFastIntSpecialGroup::IFCFastIntSpecialGroup(const std::string &name, nds::Node &parentNode, ifcdaqdrv_usr &deviceUser,
	calib_table_t &table, const fim_dataformat_t &dataformat, const fim_special_ilck_t &ilcktype) :
	m_node(nds::Port(name,
	nds::nodeType_t::generic)),
	m_deviceUser(deviceUser)
{
	parentNode.addChild(m_node);

	// Reflected Power: blocks 0 and 1
	// Cavity Decay:    blocks 2 and 3
	int block_index = (ilcktype == fim_reflected_power) ? 0 : 2;

	{
		std::ostringstream channelName;
		channelName << "CH0";
		m_RefPwrChannels.push_back(std::make_shared<IFCFastIntSpecial>(
			channelName.str(), m_node, m_deviceUser, block_index, table, dataformat, ilcktype));
	}

	{
		std::ostringstream channelName;
		channelName << "CH1";
		m_RefPwrChannels.push_back(std::make_shared<IFCFastIntSpecial>(
			channelName.str(), m_node, m_deviceUser, (block_index+1), table, dataformat, ilcktype));
	}

}

std::vector<std::shared_ptr<IFCFastIntSpecial>> IFCFastIntSpecialGroup::getChannels() {
	return m_RefPwrChannels;
}

void IFCFastIntSpecialGroup::setState(nds::state_t newState) {
	for (auto channel : m_RefPwrChannels) {
		channel->setState(newState);
	}
}

uint8_t IFCFastIntSpecial::detectInterlock(ifcfastint_fsm_state state)
{
	std::vector<std::uint8_t>::iterator local_iter;
	uint8_t last_sample;
	uint8_t graphic_eval;

	/* Graphic evaluation: will iterate the vector searching for a HIGH to LOW transition */
	last_sample = m_ppqhistory[DETECT_START - 1];
	graphic_eval = IFCFASTINT_CH_OK;

	for (local_iter = (m_ppqhistory.begin() + DETECT_START); local_iter != (m_ppqhistory.end() - DETECT_PMORTEM_SIZE + DETECT_POST_WINDOW); local_iter++)
	{
		if ((*local_iter == 0) && (last_sample == 1)) {
			m_InterlockInfo->registerInterlock();
			graphic_eval = IFCFASTINT_CH_NOK;
			break;
		}
		last_sample = *local_iter;
	}
	return graphic_eval;
}

void IFCFastIntSpecial::setFirstIlck(int32_t ilck) {
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	if ((!m_HVONenabled) && (!m_RFONenabled))
	{
		ilck = IFCFASTINT_CH_OK;
	}
	m_firstIlckPV.push(now, ilck);
}

void IFCFastIntSpecial::fastResetCmd() {
	struct ifcfastint_specialpp_option option;
	memset(&option, 0, sizeof(struct ifcfastint_specialpp_option));

	option.autoreset = 1;
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_AUTORESET_W, &option);

	option.autoreset = 0;
	ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_AUTORESET_W, &option);

	// if auto-reset, must be back to NON-LATCHED mode
	if (m_InterlockInfo->isAutoReset()) {
		option.autoreset = 1;
		ifcfastint_set_conf_refpwr(&m_deviceUser, m_block, IFCFASTINT_SPECIALPP_AUTORESET_W, &option);
	}
}

// Placeholder function - NOT IN USE FOR NOW
// receive external hardware timestamp and keep it as class param
void IFCFastIntSpecial::setHwTimestamp(const timespec& ts) {
	m_HwTimestamp.tv_sec = ts.tv_sec;
	m_HwTimestamp.tv_nsec = ts.tv_nsec;
}
