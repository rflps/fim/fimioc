#ifndef _COMMON_H_
#define _COMMON_H_

#include <iostream>
#include <sstream>
#include <map>
#include <vector>

/*
 * Helper macro to push text to user through the INFO pv
 *
 * Not allowed to be called from constructor (before node tree is initialized)
 *
 * @param text Has to be something that can be passed to std::string()
 */

#define FIMIOC_MSGWRN(text)                                             \
	do {                                                                         \
		struct timespec now;                                                       \
		clock_gettime(CLOCK_REALTIME, &now);                                       \
		m_infoPV.push(now, std::string(text));                                     \
		ndsWarningStream(m_node) << std::string(text) << "\n";                     \
	} while (0)

/** Status check helper that prints error message from userspace library,
 * sends a message to the MSGR record and returns ndsError. */
#define FIMIOC_STATUS_MSGERR(func, status)                              \
	do {                                                                         \
		std::ostringstream tmp;                                                    \
		tmp << "[FIMIOC] ";                                                 \
		tmp << (func) << " " << ifcdaqdrv_strerror(status) << (status);            \
		FIMIOC_MSGWRN(tmp.str());                                           \
	} while (0)

#define FIMIOC_STATUS_CHECK(func, status)                               \
	do {                                                                         \
		if ((status) != status_success) {                                          \
			FIMIOC_STATUS_MSGERR(func, status);                               \
			/*throw nds::NdsError("ifcdaqdrv returned error");*/                     \
		}                                                                          \
	} while (0)

#define MAX_AI_CHANNELS 20
#define MAX_DI_CHANNELS 21
#define MAX_PWRAVG_CHANNELS 4
#define MAX_SPECIALPP_CHANNELS 4

// TODO: reallocate with every commitParameters instead. Issue: channels
// pointers has to be updated with every realloc.
#define AICHANNEL_SAMPLES_MAX (4 * 1024 * 1024)

#define DETECT_BUFSIZE    8000
#define DETECT_PMORTEM_SIZE 1000
#define DETECT_PRE_WINDOW 200
#define DETECT_POST_WINDOW  200
#define DETECT_START  (DETECT_BUFSIZE-DETECT_PMORTEM_SIZE-DETECT_PRE_WINDOW)

#define HISTORY_FRAME_SIZE 64
#define HISTORY_FRAME_COUNT_MAX (8 * 1024)

#define ILCK_MSGQUEUE_MAX_DATA (64 * 8 * 1024) // one entire 14 Hz cycle
#define ILCK_MSGQUEUE_MAX_MSG (ILCK_MSGQUEUE_MAX_DATA + sizeof(size_t))

#define IFCFASTINT_CH_NOTMONITORED -1
#define IFCFASTINT_CH_OK            0
#define IFCFASTINT_CH_NOK           1

#define MSGQUEUECAPACITY        100

// Message queue type identifiers
#define MSG_INTERLOCK   0x00
#define MSG_ACQUISITION 0x01
#define MSG_TIMEOUT     0x02
#define MSG_TERMINATE   0x03

enum fim_dataformat_t {
	fim_dataformat_signed = 0x00,
	fim_dataformat_unsigned = 0x01
};
#define FIM_DATAFORMAT_SIGNED   0x00
#define FIM_DATAFORMAT_UNSIGNED 0x01

enum fim_inputtype_t {
	fim_analog_in,
	fim_digital_in,
	fim_special_rp_in,
	fim_special_cd_in,
	fim_invalid
};

enum fim_special_ilck_t {
	fim_reflected_power,
	fim_cavity_decay
};


#define FASTRESET_GRANULARITY 0.5

/***************************************************************
* Workaround to avoid callback errors in EPICS (asyn)
****************************************************************/
/* If the worker thread takes more than FIMIOC_CRITICAL_PROC_TIME
*  it will sleep for FIMIOC_CRITICAL_WAIT_TIME to enable the IOC
*  layer clean up buffers. This might cause loss of interrupts
*/
#define FIMIOC_CRITICAL_PROC_TIME 70000
#define FIMIOC_CRITICAL_WAIT_TIME 35000

#define FIM_STATE_IDLE  0
#define FIM_STATE_ARM   1
#define FIM_STATE_HVON  2
#define FIM_STATE_RFON  3
#define FIM_STATE_ABO   4


typedef std::map<int, std::vector<double> > calib_table_t;

int CheckEndianess(void);
long elapsed_time(timespec start, timespec end);

/* This vector holds the offset for all the AI variables that will be extracted from the history buffer frame */
const int offsetTable[2][20] = {
	{10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29},
	{11, 10, 13, 12, 15, 14, 17, 16, 19, 18, 21, 20, 23, 22, 25, 24, 27, 26, 29, 28}
};


//-----------------------------------------------------------------------------
// Convert from [0=-1, 32k=0, 64k=1] to [-32k=-1, 0=0, 32k-1=1]
//-----------------------------------------------------------------------------
inline int32_t raw_to_signed(int16_t *raw) {
	return (int16_t)(*raw - 32768);
}

//-----------------------------------------------------------------------------
// Convert from [-32k=-1, 0=0, 32k-1=1] to [0=-1, 32k=0, 64k=1]
//-----------------------------------------------------------------------------
inline int16_t signed_to_raw(int32_t *signd) {
	return (int16_t)(*signd + 32768);
}

//-----------------------------------------------------------------------------
//  Round upwards to nearest power-of-2.
//-----------------------------------------------------------------------------
inline int ceil_pow2(unsigned number) {
	unsigned n = 1;
	unsigned i = number - 1;
	while (i) {
		n <<= 1;
		i >>= 1;
	}
	return n;
}

//-----------------------------------------------------------------------------
//  Round upwards to nearest mibibytes
//-----------------------------------------------------------------------------
inline int ceil_mibi(unsigned number) {
	return (1 + (number - 1) / (1024 * 1024)) * 1024 * 1024;
}


struct FIMHistoryBuffer
{
	uint32_t 	seqid;

	uint8_t 	reserved1;
	uint8_t   	sig_id;
	uint8_t   	match_op_OK;
	uint8_t		fsm_status;

	uint32_t	digital_qout;
	uint32_t	analog_qout;

	uint32_t	digital_input;
	int16_t		analog_input[20];

	uint16_t	magic_number;
	uint8_t   	reserved2;
	uint8_t   	cavDecStats;
};


class HistBufferParser
{
private:
	FIMHistoryBuffer *pdata;
	int m_Endian;
	const size_t aiPositionTable[2][20] = {
		{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19},
		{1,0,3,2,5,4,7,6,9,8,11,10,13,12,15,14,17,16,19,18}
	};
public:
	HistBufferParser(int8_t *pbuffer) :
		pdata(reinterpret_cast<FIMHistoryBuffer*>(pbuffer)),
		m_Endian(CheckEndianess())
	{}
	~HistBufferParser() {}

	void updateFrame(int8_t *pbuffer) {
		pdata = reinterpret_cast<FIMHistoryBuffer*>(pbuffer);
	}

	uint8_t getCurrentState() {
		return pdata->fsm_status;
	}

	uint8_t getAnalogInputQOUT(size_t channel) {
		return ((pdata->analog_qout >> channel) & 0x01);
	}

	uint8_t getDigitalInputQOUT(size_t channel) {
		return ((pdata->digital_qout >> channel) & 0x01);
	}

	uint8_t getDigitalInput(size_t channel) {
		return ((pdata->digital_input >> channel) & 0x01);
	}

	int16_t getAnalogInput(size_t channel) {
		return (pdata->analog_input[aiPositionTable[m_Endian][channel]]);
	}

	// Reflected Power diagnostics
	uint8_t getRefPwrOK(size_t channel) {
		return ((pdata->digital_input >> (24+channel)) & 0x01);
	}

	uint8_t getRefPwrWinMask(size_t channel) {
		return ((pdata->digital_input >> (26+channel)) & 0x01);
	}

	uint8_t getRefPwrWinMeas(size_t channel) {
		return ((pdata->digital_input >> (28+channel)) & 0x01);
	}

	uint8_t getRefPwrTrig(size_t channel) {
		return ((pdata->digital_input >> (30+channel)) & 0x01);
	}

	//Cavity Decay diagnostics
	uint8_t getCavDecOK(size_t channel) {
		// return ((pdata->cavDecStats >> (24+channel)) & 0x01);
		return ((pdata->cavDecStats >> (0+channel)) & 0x01);
	}

	uint8_t getCavDecWinMask(size_t channel) {
		// return ((pdata->cavDecStats >> (26+channel)) & 0x01);
		return ((pdata->cavDecStats >> (2+channel)) & 0x01);
	}

	uint8_t getCavDecWinMeas(size_t channel) {
		// return ((pdata->cavDecStats >> (28+channel)) & 0x01);
		return ((pdata->cavDecStats >> (4+channel)) & 0x01);
	}

	uint8_t getCavDecEndTrig(size_t channel) {
		// return ((pdata->cavDecStats >> (30+channel)) & 0x01);
		return ((pdata->cavDecStats >> (6+channel)) & 0x01);
	}

	uint8_t getCavDecStartTrig(size_t channel) {
		// return ((pdata->cavDecStats >> (30+channel)) & 0x01);
		return ((pdata->cavDecStats >> (6+channel)) & 0x01);
	}


};





#endif /* _COMMON_H_ */
