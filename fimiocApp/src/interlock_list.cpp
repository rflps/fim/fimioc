/**
 * @file interlock_list.cpp
 * @author Mateusz Nabywaniec (mateusz.nabywaniec@ess.eu)
 * @brief Create and update Intelock list
 * @date 2025-01-12
 *
 * @copyright Copyright (c) 2025 European Spallation Source ERIC
 *
 */

 #include "interlock_list.h"

 // Logger definition
 DEFINE_LOGGER(fimioclogger, "fimioc.interlock_list");

 InterlockList::InterlockList(const char *pvname, int initialSize)
     : maxSize(initialSize), elements(initialSize), pvName(pvname),
       outputPV(pvxs::server::SharedPV::buildReadonly()) {

   ctxt = pvxs::client::Context::fromEnv();

   pvxs::ioc::server().addPV(pvname, outputPV);

   // Create standard NTScalarArray
   auto def = pvxs::nt::NTScalar{pvxs::TypeCode::StringA, true}.build();

   // Additional metadata
   def += {pvxs::members::UInt32("size")};

   pvxs::Value initialval(def.create());
   outputPV.open(initialval);

   log_info_printf(fimioclogger, "Initialized first interlock list %s \n", pvname);
 }

 std::string InterlockList::createMessage(const std::string &chanName) {
   std::time_t now = std::time(nullptr);
   std::tm *localTime = std::localtime(&now);
   char timeBuffer[20];

   if (chanName.empty())
       return chanName;

   std::string lastTripPVName = chanName + "LevMonTrip-RB";
   std::string levelParAPVName = chanName + "LevMonParA-RB";
   std::string levelParBPVName = chanName + "LevMonParB-RB";

   std::strftime(timeBuffer, sizeof(timeBuffer), "%Y-%m-%d %H:%M:%S", localTime);
   std::string element = "[ " + std::string(timeBuffer) + "] " + chanName;

   struct dbAddr addrLastTrip;
   struct dbAddr addrLevelA;
   struct dbAddr addrLevelB;

   long resultTrip = dbNameToAddr(lastTripPVName.c_str(), &addrLastTrip);
   long resultLevA = dbNameToAddr(levelParAPVName.c_str(), &addrLevelA);
   long resultLevB = dbNameToAddr(levelParBPVName.c_str(), &addrLevelB);

   double tripValue, levelAValue, levelBValue;
   long status;
   long numElem = 1;

   if (resultTrip == 0){
       status = dbGetField(&addrLastTrip, addrLastTrip.dbr_field_type, &tripValue, NULL, &numElem, NULL);
       if(status == 0)
           element = element + " Trip value: " + std::to_string(tripValue);
   }

   if (resultLevA == 0){
       status = dbGetField(&addrLevelA, addrLevelA.dbr_field_type, &levelAValue, NULL, &numElem, NULL);
       if(status == 0)
           element = element + "[" + std::to_string(levelAValue) + ", ";
   }

   if (resultLevB == 0){
       status = dbGetField(&addrLevelB, addrLevelB.dbr_field_type, &levelBValue, NULL, &numElem, NULL);
       if(status == 0)
           element = element + std::to_string(levelBValue) + "]";
   }

   return element;
 }

 void InterlockList::addElement(const std::string &newElement) {
   if (elements.size() >= maxSize)
     elements.pop_back();

   elements.emplace_front(newElement);
   updateNTPV();
 }

 void InterlockList::setSize(int newSize) {
   if (newSize < 1) {
     log_err_printf(fimioclogger,
                    "Error at resize, size must be bigger than 0, was: %d\n",
                    newSize);
   }

   elements.resize(newSize);
   maxSize = newSize;
   updateNTPV();
 }

 void InterlockList::updateNTPV() {
   pvxs::Value update = outputPV.fetch().cloneEmpty();
   pvxs::shared_array<std::string> elements_arr =
       pvxs::shared_array<std::string>(elements.begin(), elements.end());

   try {
     update["size"] = static_cast<epicsUInt32>(elements.size());
     update["value"] = elements_arr.freeze();

     epicsTimeStamp now;
     if (!epicsTimeGetCurrent(&now)) {
       update["timeStamp.secondsPastEpoch"] =
           now.secPastEpoch + POSIX_TIME_AT_EPICS_EPOCH;
       update["timeStamp.nanoseconds"] = now.nsec;
     }
     outputPV.post(update);

   } catch (std::exception &err) {
     log_err_printf(fimioclogger, "process [%s] : %s\n", pvName.c_str(),
                    err.what());
   }
 }

 // EPICS aSub routines
 long interlock_list_init(aSubRecord *prec) {
   if (!prec->inpa.lset->isConnected) {
     errlogSevPrintf(errlogMajor, "%s: INPA must be connected\n", prec->name);
     return S_dev_badInpType;
   }

 #define CHECK_INP(FT, INP, TYP)                                                \
   do {                                                                         \
     if (prec->FT != menuFtype##TYP) {                                          \
       errlogSevPrintf(errlogMajor,                                             \
                       "%s: incorrect input type for " #INP "; expected " #TYP  \
                       "\n",                                                    \
                       prec->name);                                             \
       return S_dev_badInpType;                                                 \
     }                                                                          \
   } while (0)

   CHECK_INP(fta, INPA, STRING);
   CHECK_INP(ftb, INPB, LONG);
   CHECK_INP(ftc, INPC, CHAR);

 #undef CHECK_INP

   const char *outputPvName = static_cast<const char *>(prec->c);

   if (sizeof(outputPvName) == 0) {
     errlogSevPrintf(errlogMajor, "%s: Missing output PV name (INPB)\n",
                     prec->name);
     return S_dev_badInpType;
   }

   // changed from size_t to int, somehow size_t does not work
   int size = 0;
   if (prec->neb != 1) {
     errlogSevPrintf(errlogMajor, "%s: Missing size of ilckList\n", prec->name);
     return S_dev_badInpType;
   } else {
     size = *static_cast<int *>(prec->b);
   }

   InterlockList *ilckList = new InterlockList(outputPvName, size);
   prec->dpvt = static_cast<void *>(ilckList);

   pvxs::logger_config_env();
   log_debug_printf(fimioclogger, "interlock_list_init[%s]: initialized\n",
                    prec->name);

   return 0;
 }

 long interlock_list_proc(aSubRecord *prec) {
   short const interlock_list_upd_en   =  *((short*)  prec->d);
   if (interlock_list_upd_en == 0) {
     log_debug_printf(fimioclogger, "interlock_list_update[%s]: Updating disabled\n",
                    prec->name);
     return 0;
   }


   if (!prec->inpa.lset->isConnected) {
     errlogSevPrintf(errlogMajor, "%s: INPA must be connected\n", prec->name);
     return S_dev_badInpType;
   }

   int size = 0;
   if (prec->neb != 1) {
     errlogSevPrintf(errlogMajor, "%s: Missing size of ilckList\n", prec->name);
     return S_dev_badInpType;
   } else {
     size = *static_cast<int *>(prec->b);
   }

   char message[256];
   strcpy(message, static_cast<char *>(prec->a));

   InterlockList *ilckList = static_cast<InterlockList *>(prec->dpvt);

   if (!ilckList) {
     log_crit_printf(fimioclogger,
                     "interlock_list_proc[%s] record in bad state\n", prec->name);
     errlogSevPrintf(errlogMajor, "%s: record in bad state\n", prec->name);
     prec->pact = 1;
     return S_dev_badInitRet;
   }

   if (ilckList->elements.size() != static_cast<size_t>(size)) {
     ilckList->setSize(size);
   }

   std::string newMessage = ilckList->createMessage(message);
   ilckList->addElement(newMessage);

   // Write last message to output record
   char *outMsg = (char*)prec->vala;
   if (outMsg){
     std::strcpy(outMsg, newMessage.c_str());
   }

   return 0;
 }

 // Register the functions for access by EPICS

 epicsRegisterFunction(interlock_list_init);
 epicsRegisterFunction(interlock_list_proc);
