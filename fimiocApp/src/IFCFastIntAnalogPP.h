#ifndef IFCFastIntAnalogPP_H
#define IFCFastIntAnalogPP_H

#include <nds3/nds.h>
#include <ifcfastintdrv.h>

class IFCFastIntAnalogPP {
public:
	IFCFastIntAnalogPP(const std::string &name, nds::Node &parentNode,
                      int32_t channelNum, struct ifcdaqdrv_usr &deviceUser,
                      ifcfastint_analog_pp ppType, std::vector<double> *scalingLut,
                      const fim_dataformat_t &dataformat);

  // Class public parameters
  std::int32_t m_channelNum;
  ifcfastint_analog_pp m_ppType;


  // Class public methods

  void setState(nds::state_t newState);

  void setPPEmulated(const timespec &timespec, const int32_t &value);
  void getPPEmulated(timespec *timespec, int32_t *value);

  void setPPMode(const timespec &timespec, const int32_t &value);
  void getPPMode(timespec *timespec, int32_t *value);

  void setPPVal1(const timespec &timespec, const double &value);
  void getPPVal1(timespec *timespec, double *value);

  void setPPVal2(const timespec &timespec, const double &value);
  void getPPVal2(timespec *timespec, double *value);

  void setPPCVal(const timespec &timespec, const double &value);
  void getPPCVal(timespec *timespec, double *value);

  void getRTStatus(timespec *timespec, int32_t *value);
  void refreshRTStatus(uint8_t *pp_measure_block);

  void getProcOut(timespec *timespec, int32_t *value);
  void getPresVal(timespec *timespec, double *value);
  void getTrigVal(timespec *timespec, double *value);

  void updateProcOut(void);
  void updatePresVal(void);
  void updateTrigVal(void);

  void updateDevMonMax(void);
  void updateDevMonMin(void);

  void resetPP(const timespec &timespec, const int32_t &value);
  void getResetPP(timespec *timespec, int32_t *value); // IS THIS REALLY NECESSARY?

  void setEnDevReference(const timespec &timespec, const int32_t &value);
  void getEnDevReference(timespec *timespec, int32_t *value);

  void setAutoreset(const timespec &timespec, const int32_t &value);
  void getAutoreset(timespec *timespec, int32_t *value);

  void setPolarity(const timespec &timespec, const int32_t &value);
  void getPolarity(timespec *timespec, int32_t *value);

  void getDevMonMin(timespec *timespec, double *value);
  void getDevMonMax(timespec *timespec, double *value);


private:

  // Class private parameters
  struct ifcdaqdrv_usr &m_deviceUser;
  std::vector<double> *m_scalingLut;
  nds::Node m_node;
  nds::StateMachine m_stateMachine;
  const fim_dataformat_t m_DataFormat;

  /* Are these necessary ?? */
  bool m_pp_emulated;
  ifcfastint_amode m_pp_mode;
  double m_pp_val1;
  double m_pp_val2;
  int16_t m_pp_cval;
  bool m_en_devreference; // Enable deviation monitor reference value
  int32_t m_rt_status;

  int32_t m_proc_out;
  double m_pres_val;
  double m_trig_val;

  int16_t  m_i16pres_val;
  uint32_t m_ui32pres_val;
  int16_t  m_i16trig_val;
  uint32_t m_ui32trig_val;

  //devmode parameters
  int16_t  m_i16devmon_min;
  uint32_t m_ui32devmon_min;
  int16_t  m_i16devmon_max;
  uint32_t m_ui32devmon_max;


  /* Variables for parameters */

  /* PVs for readback values */
  nds::PVDelegateIn<std::int32_t> m_pp_emulatedPV;
  nds::PVDelegateIn<std::int32_t> m_pp_modePV;
  nds::PVDelegateIn<double> m_pp_val1PV;
  nds::PVDelegateIn<double> m_pp_val2PV;
  nds::PVDelegateIn<double> m_pp_cvalPV;
  nds::PVDelegateIn<std::int32_t> m_rt_statusPV;
  nds::PVDelegateIn<std::int32_t> m_en_devreferencePV; // Enable deviation monitor reference value

  nds::PVDelegateIn<std::int32_t> m_proc_outPV;
  nds::PVDelegateIn<double> m_pres_valPV;
  nds::PVDelegateIn<double> m_trig_valPV;

  nds::PVDelegateIn<std::int32_t> m_autoresetPV;
  nds::PVDelegateIn<std::int32_t> m_polarityPV;

  nds::PVDelegateIn<double> m_devmon_maxPV;
  nds::PVDelegateIn<double> m_devmon_minPV;
};


#endif /* IFCFASTINTANALOG_PP */
