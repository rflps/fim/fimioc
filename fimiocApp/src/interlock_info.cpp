#include <cstdlib>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>

#include "device.h"
#include "interlock_info.h"

/* Class constructor */
FIM_InterlockInfo::FIM_InterlockInfo(const std::string &name, nds::Node &parentNode,
						const int32_t channelNum, const fim_inputtype_t input_type) :
	m_channelNum(channelNum),
	m_inputType(input_type),
	m_IlckCounter(0),
	m_AutoResetEnablePV(nds::PVVariableOut<std::int32_t>("AR-ENABLED")),
	m_InterlockCountPV(nds::PVVariableIn<std::int32_t>("ILCKCNT-RB"))

{
	m_node = parentNode.addChild(nds::Node(name));

	m_node.addChild(m_AutoResetEnablePV);
	m_InterlockCountPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_InterlockCountPV);

	/* Reset channel interlock counter */
	nds::PVDelegateOut<std::int32_t> node = nds::PVDelegateOut<std::int32_t>("ILCKCNT-RST",
		std::bind(&FIM_InterlockInfo::setResetCount, this, std::placeholders::_1, std::placeholders::_2),
		std::bind(&FIM_InterlockInfo::getResetCount, this, std::placeholders::_1, std::placeholders::_2));
	m_node.addChild(node);

	m_InterlockCountPV.setValue(m_IlckCounter);
}

bool FIM_InterlockInfo::isAutoReset() {
	return static_cast<bool>(m_AutoResetEnablePV.getValue());
}

void FIM_InterlockInfo::registerInterlock() {
	struct timespec now = {0,0};
	clock_gettime(CLOCK_REALTIME, &now);

	m_InterlockCountPV.setValue(++m_IlckCounter);
	m_InterlockCountPV.push(now, m_IlckCounter);

	ndsDebugStream(m_node) << "[AUTORESET] Increasing interlock counter [" << m_IlckCounter << "] on channel " << m_channelNum << std::endl;
}

void FIM_InterlockInfo::setResetCount(const timespec &timespec, const int32_t &value) {
	if (value) {
		m_IlckCounter = 0;
		m_InterlockCountPV.setValue(m_IlckCounter);
		m_InterlockCountPV.push(timespec, m_IlckCounter);
	}
}

void FIM_InterlockInfo::getResetCount(timespec *timespec, int32_t *value) {
	*value = 0;
}
