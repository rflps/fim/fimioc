#ifndef IFCFastIntDICHANNEL_H
#define IFCFastIntDICHANNEL_H

#include <nds3/nds.h>
#include <ifcfastintdrv.h>

#include "interlock_info.h"

class IFCFastIntDIChannel {
public:
	IFCFastIntDIChannel(const std::string &name, nds::Node &parentNode,
											int32_t channelNum, struct ifcdaqdrv_usr &deviceUser);

	std::int32_t m_channelNum;
	int32_t m_firstIlck;

	nds::Node m_node;
	nds::StateMachine m_stateMachine;

	std::shared_ptr<FIM_InterlockInfo> m_InterlockInfo;

	void addValue(int32_t data);
	void getData(timespec *timespec, int32_t *value);
	void getHistory(timespec *timespec, std::vector<std::int32_t> *value);
	void getPPQHistory(timespec *timespec, std::vector<std::uint8_t> *value);

	void getPPQPmortem(timespec *timespec, std::vector<std::uint8_t> *value);
	void getHistPMortem(timespec *timespec, std::vector<double> *value);

	void historyAddValue(int32_t data);
	void historyAddPPQValue(uint8_t data);
	void historyReset(bool pmortem);
	void reserve(size_t nframes);

	void setState(nds::state_t newState);

	void setPPOutput(int32_t qout);
	void getPPQOut(timespec *timespec, int32_t *value);

	void setPPEmulated(const timespec &timespec, const int32_t &value);
	void getPPEmulated(timespec *timespec, int32_t *value);

	void setPPMode(const timespec &timespec, const int32_t &value);
	void getPPMode(timespec *timespec, int32_t *value);

	void setPPCVal(const timespec &timespec, const int32_t &value);
	void getPPCVal(timespec *timespec, int32_t *value);

	void setPPVal1(const timespec &timespec, const int32_t &value);
	void getPPVal1(timespec *timespec, int32_t *value);

	void setPPVal2(const timespec &timespec, const int32_t &value);
	void getPPVal2(timespec *timespec, int32_t *value);

	void setChEnable(const timespec &timespec, const int32_t &value);
	void getChEnable(timespec *timespec, int32_t *value);

	void resetInterlock(const timespec &timespec, const int32_t &value);
	void getresetInterlock(timespec *timespec, int32_t *value);

	void setInterlockStateTransitionIdlePre(const timespec &timespec, const int32_t &value);
	void getInterlockStateTransitionIdlePre(timespec *timespec, int32_t *value);

	void setInterlockStateTransitionPreRun(const timespec &timespec, const int32_t &value);
	void getInterlockStateTransitionPreRun(timespec *timespec, int32_t *value);

	uint8_t detectInterlock(void);

	void setAutoreset(const timespec &timespec, const int32_t &value);
	void getAutoreset(timespec *timespec, int32_t *value);

	bool getHVONprecond() {
		return m_HVONprecond;
	};
	bool getRFONprecond() {
		return m_RFONprecond;
	};
	void setHwTimestamp(const timespec& ts);

private:

	void switchOn();
	void switchOff();
	void start();
	void stop();
	void recover();
	bool allowChange(const nds::state_t, const nds::state_t, const nds::state_t);

	struct ifcdaqdrv_usr &m_deviceUser;
	double m_data;

	std::vector<std::int32_t> m_history;
	std::vector<std::int32_t>::iterator m_history_ptr;

	std::vector<std::uint8_t> m_ppqhistory;
	std::vector<std::uint8_t>::iterator m_ppqhistory_ptr;

	/* Variables for parameters */
	bool m_pp_out;
	bool m_ch_enable;

	/* Variables for transition conditions */
	bool m_HVONenabled;
	bool m_RFONenabled;

	/* Variable to hold current interlock status */
	bool m_ilckStatus;
	bool m_HVONprecond;
	bool m_RFONprecond;

	/* Placeholder: external hardware timestamp */
	timespec m_HwTimestamp;

	/* PV for channel data */
	nds::PVDelegateIn<std::int32_t> m_dataPV;
	nds::PVDelegateIn<std::vector<std::int32_t>> m_historyPV;
	nds::PVDelegateIn<std::vector<std::uint8_t>> m_ppqhistoryPV;

	nds::PVDelegateIn<std::vector<double>>  m_pmortemPV;
	nds::PVDelegateIn<std::vector<std::uint8_t>> m_ppq_pmortemPV;


	/* PVs for readback values */
	nds::PVDelegateIn<std::int32_t> m_firstIlckPV;
	nds::PVDelegateIn<std::int32_t> m_pp_emulatedPV;
	nds::PVDelegateIn<std::int32_t> m_pp_modePV;
	nds::PVDelegateIn<std::int32_t> m_pp_val1PV;
	nds::PVDelegateIn<std::int32_t> m_pp_val2PV;
	nds::PVDelegateIn<std::int32_t> m_pp_cvalPV;
	nds::PVDelegateIn<std::int32_t> m_fsmTransIdlePrePV;
	nds::PVDelegateIn<std::int32_t> m_fsmTransPreRunPV;
	nds::PVDelegateIn<std::int32_t> m_ch_enablePV;
	nds::PVDelegateIn<std::int32_t> m_autoresetPV;

	// Precondition PVs
  	nds::PVVariableIn<std::int32_t> m_ilckStatusPV;
  	nds::PVVariableIn<std::int32_t> m_HVONPreCondPV;
 	nds::PVVariableIn<std::int32_t> m_RFONPreCondPV;

};

#endif /* IFCFastIntDICHANNEL_H */
