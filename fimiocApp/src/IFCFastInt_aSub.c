#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>

#include <string.h> // Provides memcpy prototype
#include <stdlib.h> // Provides calloc prototype
#include <stdint.h>
#include <stdio.h>


/* Simple function that interpolates two numbers and gives the equation y = ax + b */
int calc_linear_equation(double x1, double x2, double y1, double y2, double *a, double *b)
{
    if ((x2-x1) == 0) {
        printf("Invalid calibration points (equal raw values)\n");
        return -1;
    }

    /* First we calculate the angular coeficient */
    *a = (y2 - y1) / (x2 - x1);

    /* Then we use the first point to calculate the linear coeficient b = y - ax*/
    *b = y1 - (*a * x1);

    return 0;
}

/*
 *   Assuming that INPA = raw values / INPB = egu values
 *
*/
static int ifcfastint_linear(aSubRecord *precord) {
    unsigned long nelm_egu, nelm_raw;
    double *raw, *egu;
    double result[65536]; //array that holds the 2^16 values of the calculation
    double a, b; // line coeficients

    long iter; // iterator for the for loops
    long iter2; // iterator for the for loops

    // Find the number of elements of the calibration tables
    nelm_raw = precord->nea;
    nelm_egu = precord->neb;
    if (nelm_egu != nelm_raw) {
        printf("Calibration Tables with different sizes\n");
        return -1;
    }

    /* Point to the calibration values */
    raw = (double *) precord->a;
    egu = (double *) precord->b;

    /* First segment */
    if (calc_linear_equation(raw[0], raw[1], egu[0], egu[1], &a, &b))
    {
        return -1;
    }

    /* Fill the lookup table starting from zero */
    for (iter=0; iter < (long) raw[1]; iter++)
    {
        result[iter] = (a * (double) iter) + b;
    }

    /* Iterates over the following segments */
    for (iter=2; iter < nelm_egu; iter++) {
        // Calculates the interpolation of this segment
        if (calc_linear_equation(raw[iter-1], raw[iter], egu[iter-1], egu[iter], &a, &b))
        {
            return -1;
        }

        /* Fill the lookup table starting from zero */
        for (iter2= (long) raw[iter-1]; iter2 < (long) raw[iter]; iter2++)
        {
            result[iter2] = (a * (double) iter2) + b;
        }

    }

    /* Last segment, keep last segment coeficients*/
    for (iter=(long) raw[nelm_egu-1]; iter < 65536; iter++)
    {
        result[iter] = (a * (double) iter) + b;
    }

    /* Now the table is filled, copy it to the PV*/
    memcpy(precord->vala, result, 65536 * sizeof(double));

    /* Done! */
    return 0;
}

epicsRegisterFunction(ifcfastint_linear);

static int ifcfastint_defaultval(aSubRecord *precord) {
    unsigned long nelements;
    double *raw_table, *egu_table;
    //double *raw_table;

    nelements = precord->nova;
    if (nelements != precord->novb) {
	   printf("Invalid number of elements of calibration parameters\n");
    }

    raw_table = (double *) calloc(nelements, sizeof(double));
    egu_table = (double *) calloc(nelements, sizeof(double));

    /* Fills raw table considering 16-bit values */
    int iter;
    for (iter = 0; iter < nelements; iter++) {
	   raw_table[iter] = (65535 / nelements)*iter;
    }

    /* Fills EGU table considering -10 to +10 range */
    for (iter = 0; iter < nelements; iter++) {
	   egu_table[iter] = (20.0 / nelements)*iter - 10.0;
    }

    /* Now the table is filled, copy it to the PV*/
    memcpy(precord->vala, raw_table, nelements * sizeof(double));
    memcpy(precord->valb, egu_table, nelements * sizeof(double));

    free(raw_table);
    free(egu_table);

    /* Done! */
    return 0;
}

epicsRegisterFunction(ifcfastint_defaultval);

static int ifcfastint_defaultval_counts(aSubRecord *precord) {
    unsigned long nelements;
    double *raw_table, *egu_table;

    nelements = precord->nova;
    if (nelements != precord->novb) {
       printf("Invalid number of elements of calibration parameters\n");
    }

    raw_table = (double *) calloc(nelements, sizeof(double));
    egu_table = (double *) calloc(nelements, sizeof(double));

    /* Fills raw table considering 16-bit values */
    int iter;
    for (iter = 0; iter < nelements; iter++) {
       raw_table[iter] = (65535 / nelements)*iter;
    }

    /* Fills EGU table also considering 16-bit values */
    for (iter = 0; iter < nelements; iter++) {
       egu_table[iter] = (65535 / nelements)*iter;
    }

    /* Now the table is filled, copy it to the PV*/
    memcpy(precord->vala, raw_table, nelements * sizeof(double));
    memcpy(precord->valb, egu_table, nelements * sizeof(double));

    free(raw_table);
    free(egu_table);

    /* Done! */
    return 0;
}

epicsRegisterFunction(ifcfastint_defaultval_counts);


static int ifcfastint_transform_wave(aSubRecord *precord) {
    unsigned long nelements;
    double *eguWave, *lookuptable, *rawWave;

    // Caputure the number of elements of the waveform
    nelements = precord->nea;
    eguWave = (double *) calloc(nelements, sizeof(double));
    rawWave = (double *) precord->a;
    lookuptable = (double *) precord->b;

    long iter = 0;

    /* Iterate over raw waveform to find the EGU value*/
    for (iter = 0; iter < nelements; iter++) {
        eguWave[iter] = lookuptable[(long) rawWave[iter]];
    }

    /* Now the table is filled, copy it to the PV*/
    memcpy(precord->vala, eguWave, nelements * sizeof(double));
    free(eguWave);

    /* Done! */
    return 0;
}

epicsRegisterFunction(ifcfastint_transform_wave);

static int ifcfastint_rawtoegu(aSubRecord *precord) {
    double *lookuptable, *rawScalar;

    rawScalar = (double *) precord->a;
    lookuptable = (double *) precord->b;

    *(double *)precord->vala = lookuptable[(long) *rawScalar];

    /* Done! */
    return 0;
}

epicsRegisterFunction(ifcfastint_rawtoegu);

/* Very simple linear search algorithm */

static int ifcfastint_egutoraw(aSubRecord *precord) {
    double *lookuptable;
    double rawEGU;
    long iter;

    rawEGU = *(double *) precord->a;
    lookuptable = (double *) precord->b;

    for (iter = 0; iter < 65536; iter++)
    {
        if (rawEGU < lookuptable[iter])
            break;
    }

    if (iter == 65536) iter--;

    *(double *)precord->vala = (double) iter;

    /* Done! */
    return 0;
}

epicsRegisterFunction(ifcfastint_egutoraw);


/* Pre-condition checking */

/* @param comprec_wave
 *      is the compress record that contains, at its first element
 *      the current QOUT status of a given A/D input.
 * @param precond
 *      is the Idle->HV or HV->RF condition (transition qualifier)
 */

static int ifcfastint_checkprecond(aSubRecord *precord) {
    uint32_t *comprec_wave;
    uint32_t precond;

    comprec_wave = (uint32_t *) precord->a;
    precond = *(uint32_t *) precord->b;

    /* if the transition qualifier is disabled, precondition is OK
     * if not, then it must follow current QOUT value
     */

    *(uint32_t *)precord->vala = (~precond & 0x00000001) | (comprec_wave[0] & 0x00000001);

    /* Done! */
    return 0;
}

epicsRegisterFunction(ifcfastint_checkprecond);



/*
 * INPUT A = waveform to be reade
 * INPUT B = ROI start
 * INPUT C = ROI end
 *  */

static int ifcfastint_ROI_avg(aSubRecord *precord) {
    int nelements, cursor_a, cursor_b, max_elem;
    double *wave_input;
    int roi_size, i;
    double sum, avg;

    nelements = (int) precord->nea;
    if ((nelements < 2 ) || ((precord->neb + precord->nec) != 2)) {
	   printf("   error with the asub inputs\n");
       return -1;
    }

    wave_input = (double *) precord->a;
    cursor_a = *(int *) precord->b;
    roi_size = *(int *) precord->c;
    max_elem = *(int *) precord->d;

    // validate cursor A and size
    if (cursor_a < 0) {
        cursor_a = 0;
    }
    if (roi_size <= 0) {
        roi_size = 1;
    }
    if ((cursor_a + roi_size) > max_elem) {
        roi_size = max_elem - cursor_a;
    }
    cursor_b = cursor_a + roi_size;

    sum = 0.0;
    avg = 0.0;

    // cursor A and cursor B are valid
    for (i = cursor_a; i < cursor_b; i++) {
        sum += wave_input[i];
    }
    avg = (sum / (double) roi_size);

    *(double *)precord->vala = avg;

    /* Done! */
    return 0;
}

epicsRegisterFunction(ifcfastint_ROI_avg);



//----------------------------------------------------------------
// Determine if AI channel is active, simulated or bypassed
//----------------------------------------------------------------
#define FIM_ILCK_ACTIVE     3
#define FIM_ILCK_FORCED_NOK 2
#define FIM_ILCK_FORCED_OK  1
#define FIM_ILCK_BYPASSED   0

static int fimioc_ilck_status_ai(aSubRecord *precord) {
    int result = FIM_ILCK_ACTIVE;

    // reads all inputs
    unsigned int dev_sim = *(unsigned int *) precord->a;
    unsigned int dev_sim_mode = *(unsigned int *) precord->b;
    unsigned int lvl_sim = *(unsigned int *) precord->c;
    unsigned int lvl_sim_mode = *(unsigned int *) precord->d;
    unsigned int wid_sim = *(unsigned int *) precord->e;
    unsigned int wid_sim_mode = *(unsigned int *) precord->f;
    unsigned int rep_sim = *(unsigned int *) precord->g;
    unsigned int rep_sim_mode = *(unsigned int *) precord->h;
    unsigned int hvon_enable = *(unsigned int *) precord->i;
    unsigned int rfon_enable = *(unsigned int *) precord->j;

    // if channel is not used for HVON or RFON, it is bypassed
    if (!(hvon_enable || rfon_enable)) {
        result = FIM_ILCK_BYPASSED;
    }

    // if ALL channels are in simulation mode, it's either BYPASSED or SIMULATED (interlock)
    else if (!(dev_sim || lvl_sim || wid_sim || rep_sim)) {
        if (dev_sim_mode && lvl_sim_mode && wid_sim_mode && rep_sim_mode)
            result = FIM_ILCK_FORCED_OK;
        else
            result = FIM_ILCK_FORCED_NOK;
    }

    // channel is active
    else {
        result = FIM_ILCK_ACTIVE;
    }

    // write output
    memcpy(precord->vala, &result, sizeof(int));
    precord->neva = 1;

    return 0;
}

epicsRegisterFunction(fimioc_ilck_status_ai);

static int fimioc_ilck_status_di(aSubRecord *precord) {
    int result = FIM_ILCK_ACTIVE;

    // reads all inputs
    unsigned int di_mode = *(unsigned int *) precord->a;
    unsigned int hvon_enable = *(unsigned int *) precord->b;
    unsigned int rfon_enable = *(unsigned int *) precord->c;
    // if channel is not used for HVON or RFON, it is bypassed
    if (!(hvon_enable || rfon_enable)) {
        result = FIM_ILCK_BYPASSED;
    }

    // if ALL channels are in simulation mode, it's either BYPASSED or SIMULATED (interlock)
    else if (di_mode == 0) {
        result = FIM_ILCK_FORCED_NOK;
    }

    else if (di_mode == 1) {
        result = FIM_ILCK_FORCED_OK;
    }

    // channel is active
    else {
        result = FIM_ILCK_ACTIVE;
    }

    // write output
    memcpy(precord->vala, &result, sizeof(int));
    precord->neva = 1;

    return 0;
}

epicsRegisterFunction(fimioc_ilck_status_di);

static int fimioc_softilck_calc(aSubRecord *precord) {
    int result = 1;

    //Reads ilck modes
    unsigned int ilck_mode = *(unsigned int*) precord->a;
    unsigned int ilck_sim_ena = *(unsigned int*) precord->b;
    unsigned int ilck_sim_mode = *(unsigned int*) precord->c;

    //Reads input and thresholds
    double param_a = *(double*) precord->d;
    double param_b = *(double*) precord->e;
    double value = *(double*) precord->f;

    // Test if is simulation
    if (ilck_sim_ena) {
        result = ilck_sim_mode;
    } else {

        switch (ilck_mode)
        {
            //MODE 0: if >= A then OK
            case 0:
                if (value >= param_a) {
                    result = 1;
                } else {
                    result = 0;
                }
                break;

            //MODE 1: if <= B then OK
            case 1:
                if (value <= param_b) {
                    result = 1;
                } else {
                    result = 0;
                }
                break;

            //MODE 2: A <= val <= B then OK
            case 2:
                if ((value >= param_a) && (value <= param_b)) {
                    result = 1;
                } else {
                    result = 0;
                }
                break;

            default:
            result = 1;
                break;
        }
    }

    // write output
    memcpy(precord->vala, &result, sizeof(int));
    precord->neva = 1;

    return 0;
}

epicsRegisterFunction(fimioc_softilck_calc);


static int fimioc_ilck_status_soft(aSubRecord *precord) {
    int result = FIM_ILCK_ACTIVE;

    // reads all inputs
    unsigned int sim_mode = *(unsigned int *) precord->a;
    unsigned int sim_val = *(unsigned int *) precord->b;
    unsigned int hvon_enable = *(unsigned int *) precord->c;
    unsigned int rfon_enable = *(unsigned int *) precord->d;

    // if channel is not used for HVON or RFON, it is bypassed
    if (!(hvon_enable || rfon_enable)) {
        result = FIM_ILCK_BYPASSED;
    }

    // if ALL channels are in simulation mode, it's either BYPASSED or SIMULATED (interlock)
    else if (sim_mode == 1) {
        if (sim_val == 0) {
            result = FIM_ILCK_FORCED_NOK;
        } else {
            result = FIM_ILCK_FORCED_OK;
        }
    }

    // channel is active
    else {
        result = FIM_ILCK_ACTIVE;
    }

    // write output
    memcpy(precord->vala, &result, sizeof(int));
    precord->neva = 1;

    return 0;
}

epicsRegisterFunction(fimioc_ilck_status_soft);
