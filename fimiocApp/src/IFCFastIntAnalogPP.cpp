#include <cstdlib>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>
#include <ifcfastintdrv.h>

#include "device.h"
#include "IFCFastIntAnalogPP.h"

/* Convert from [0=-1, 32k=0, 64k=1] to [-32k=-1, 0=0, 32k-1=1] */
inline int32_t raw_to_signed(int16_t raw) { return (int16_t)(raw - 32768); }

/* Convert from [-32k=-1, 0=0, 32k-1=1] to [0=-1, 32k=0, 64k=1] */
inline int16_t signed_to_raw(int32_t signd) { return (int16_t)(signd + 32768); }

inline int findADCCode(const double value, const std::vector<double>& scalingLut) {
    for (int i = scalingLut.size() - 1; i >= 0; --i) {
        if (scalingLut[i] <= value) // Stop at the largest ADC code <= value
            return i;
    }
    return 0; // Default to the lowest ADC code if nothing is found
}

static inline void ifcfastint_swap64(uint64_t* destination)
{
    uint64_t source = *destination;
    uint8_t *byte_ppo;
    uint8_t *byte_aux;

    byte_ppo = (uint8_t*) destination;
    byte_aux = (uint8_t*) &source;

    byte_ppo[0] = byte_aux[7];
    byte_ppo[1] = byte_aux[6];
    byte_ppo[2] = byte_aux[5];
    byte_ppo[3] = byte_aux[4];
    byte_ppo[4] = byte_aux[3];
    byte_ppo[5] = byte_aux[2];
    byte_ppo[6] = byte_aux[1];
    byte_ppo[7] = byte_aux[0];
}


/* Class constructor */
IFCFastIntAnalogPP::IFCFastIntAnalogPP(const std::string &name, nds::Node &parentNode,
				int32_t channelNum, struct ifcdaqdrv_usr &deviceUser,
				ifcfastint_analog_pp ppType, std::vector<double> *scalingLut,
				const fim_dataformat_t &dataformat)
	:m_channelNum(channelNum),  m_ppType(ppType), m_deviceUser(deviceUser), m_scalingLut(scalingLut),
	m_DataFormat(dataformat),
	 m_proc_out(0), m_pres_val(0), m_trig_val(0),
	 m_i16pres_val(0), m_ui32pres_val(0), m_i16trig_val(0), m_ui32trig_val(0),
	 m_pp_emulatedPV (nds::PVDelegateIn<std::int32_t>(
			 "PP-EMUL-RB",
			 std::bind(&IFCFastIntAnalogPP::getPPEmulated, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_pp_modePV (nds::PVDelegateIn<std::int32_t>(
			 "PP-MODE-RB",
			 std::bind(&IFCFastIntAnalogPP::getPPMode, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_pp_val1PV (nds::PVDelegateIn<double>(
			 "PP-VAL1-RB",
			 std::bind(&IFCFastIntAnalogPP::getPPVal1, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_pp_val2PV (nds::PVDelegateIn<double>(
			 "PP-VAL2-RB",
			 std::bind(&IFCFastIntAnalogPP::getPPVal2, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_pp_cvalPV (nds::PVDelegateIn<double>(
			 "PP-CVAL-RB",
			 std::bind(&IFCFastIntAnalogPP::getPPCVal, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_rt_statusPV (nds::PVDelegateIn<std::int32_t>(
			 "PP-RTSTATS-RB",
			 std::bind(&IFCFastIntAnalogPP::getRTStatus, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_en_devreferencePV (nds::PVDelegateIn<std::int32_t>(
			 "PP-ENREF-RB",
			 std::bind(&IFCFastIntAnalogPP::getEnDevReference, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_proc_outPV (nds::PVDelegateIn<std::int32_t>(
			 "PP-PROCOUT-RB",
			 std::bind(&IFCFastIntAnalogPP::getProcOut, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_pres_valPV (nds::PVDelegateIn<double>(
			 "PP-PRESVAL-RB",
			 std::bind(&IFCFastIntAnalogPP::getPresVal, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_trig_valPV (nds::PVDelegateIn<double>(
			 "PP-TRIGVAL-RB",
			 std::bind(&IFCFastIntAnalogPP::getTrigVal, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_autoresetPV (nds::PVDelegateIn<std::int32_t>(
			 "PP-AUTORESET-RB",
			 std::bind(&IFCFastIntAnalogPP::getAutoreset, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_polarityPV (nds::PVDelegateIn<std::int32_t>(
			 "PP-POLARITY-RB",
			 std::bind(&IFCFastIntAnalogPP::getPolarity, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_devmon_maxPV (nds::PVDelegateIn<double>(
			 "PP-DEVMONMAX-RB",
			 std::bind(&IFCFastIntAnalogPP::getDevMonMax, this,
			 std::placeholders::_1, std::placeholders::_2))),
	 m_devmon_minPV (nds::PVDelegateIn<double>(
			 "PP-DEVMONMIN-RB",
			 std::bind(&IFCFastIntAnalogPP::getDevMonMin, this,
			 std::placeholders::_1, std::placeholders::_2)))
{

	m_node = parentNode.addChild(nds::Node(name));

	/* Emulation PV *********************************************************/
	m_node.addChild( nds::PVDelegateOut<std::int32_t>(
			"PP-EMUL", std::bind(&IFCFastIntAnalogPP::setPPEmulated, this,
			std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAnalogPP::getPPEmulated, this,
			std::placeholders::_1, std::placeholders::_2)) );

	/* Readback PV */
	m_pp_emulatedPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pp_emulatedPV);

	/* Add PV for the mode of this Analog PP ************************************/
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
		 "PP-MODE", std::bind(&IFCFastIntAnalogPP::setPPMode, this,
		 std::placeholders::_1, std::placeholders::_2),
		 std::bind(&IFCFastIntAnalogPP::getPPMode, this, std::placeholders::_1,
		 std::placeholders::_2)));

	/* Readback PV */
	m_pp_modePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pp_modePV);

	/* Add PV for VAL1 ********************************************************/
	m_node.addChild(nds::PVDelegateOut<double>(
			"PP-VAL1", std::bind(&IFCFastIntAnalogPP::setPPVal1, this,
			std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAnalogPP::getPPVal1, this, std::placeholders::_1,
			std::placeholders::_2)));

	/* Readback PV */
	m_pp_val1PV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pp_val1PV);

	/* Add PV for VAL2 ********************************************************/
	m_node.addChild(nds::PVDelegateOut<double>(
			"PP-VAL2", std::bind(&IFCFastIntAnalogPP::setPPVal2, this,
			std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAnalogPP::getPPVal2, this, std::placeholders::_1,
			std::placeholders::_2)));

	/* Readback PV */
	m_pp_val2PV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pp_val2PV);

	/* Add PV for emulated value ************************************************/
	m_node.addChild(nds::PVDelegateOut<double>(
			"PP-CVAL", std::bind(&IFCFastIntAnalogPP::setPPCVal, this,
			std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAnalogPP::getPPCVal, this, std::placeholders::_1,
			std::placeholders::_2)));

	/* Readback PV */
	m_pp_cvalPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pp_cvalPV);

	m_rt_statusPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_rt_statusPV);

	m_proc_outPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_proc_outPV);

	m_pres_valPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pres_valPV);

	m_trig_valPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_trig_valPV);

		/* Add PV for the mode of this Analog PP ************************************/
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
		 "PP-RESET", std::bind(&IFCFastIntAnalogPP::resetPP, this,
		 std::placeholders::_1, std::placeholders::_2),
		 std::bind(&IFCFastIntAnalogPP::getResetPP, this, std::placeholders::_1,
		 std::placeholders::_2)));


	/* Emulation PV *********************************************************/
	m_node.addChild( nds::PVDelegateOut<std::int32_t>(
			"PP-ENREF", std::bind(&IFCFastIntAnalogPP::setEnDevReference, this,
			std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAnalogPP::getEnDevReference, this,
			std::placeholders::_1, std::placeholders::_2)) );

	/* Readback PV */
	m_en_devreferencePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_en_devreferencePV);

	/* autoreset PV *********************************************************/
	m_node.addChild( nds::PVDelegateOut<std::int32_t>(
			"PP-AUTORESET", std::bind(&IFCFastIntAnalogPP::setAutoreset, this,
			std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAnalogPP::getAutoreset, this,
			std::placeholders::_1, std::placeholders::_2)) );

	/* Readback PV */
	m_autoresetPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_autoresetPV);

	/* polarity PV *********************************************************/
	m_node.addChild( nds::PVDelegateOut<std::int32_t>(
			"PP-POLARITY", std::bind(&IFCFastIntAnalogPP::setPolarity, this,
			std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAnalogPP::getPolarity, this,
			std::placeholders::_1, std::placeholders::_2)) );

	/* Readback PV */
	m_polarityPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_polarityPV);

	m_devmon_maxPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_devmon_maxPV);
	m_devmon_minPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_devmon_minPV);


	/* TODO: Additional initialization ?? */
	struct ifcfastint_analog_option current_option;
	ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_INITCONFIG_W, m_ppType, &current_option);
	//m_stateMachine.setLogLevel(nds::logLevel_t::none);
	// struct timespec now;
	// clock_gettime(CLOCK_REALTIME, &now);
	// resetPP(now, 1);
}

/*************************************** Class methods ***************************************************/

void IFCFastIntAnalogPP::resetPP(const timespec &timespec, const int32_t &value)
{
	struct ifcfastint_analog_option current_option;
	ifcfastint_amode current_mode;
	memset(&current_option, 0, sizeof(struct ifcfastint_analog_option));

	// Reads the current mode
	ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, m_ppType, &current_option);
	current_mode = current_option.mode;

	// if mode is ONE the block is bypassed, so ignore
	if ((value) and (current_mode != ifcfastint_amode_one) and (current_mode != ifcfastint_amode_zero))
	{
		// Forces PP MODE to ZERO (to reset the block)
		current_option.mode = ifcfastint_amode_one;
		ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_MODE_W, m_ppType, &current_option);

		// set back the original configuration
		current_option.mode = current_mode;
		ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_MODE_W, m_ppType, &current_option);
	}

}

void IFCFastIntAnalogPP::getResetPP(timespec *timespec, int32_t *value) // IS THIS REALLY NECESSARY?
{
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = 0;
}

/* Boilerplate code ? */
void IFCFastIntAnalogPP::setState(nds::state_t newState) {
	m_stateMachine.setState(newState);
}

void IFCFastIntAnalogPP::setPPEmulated(const timespec &timespec, const int32_t &value)
{
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	option.emulation_en = 0;

	/* Write PP configuration according to channel num and pp type */
	ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_EMULATION_EN_W, m_ppType, &option); // FORCED TO ZERO

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_pp_emulatedPV.read(&now, &rb_val);
	m_pp_emulatedPV.push(now, rb_val);
}

void IFCFastIntAnalogPP::getPPEmulated(timespec *timespec, int32_t *value) {
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, m_ppType, &option);

	clock_gettime(CLOCK_REALTIME, timespec);
	*value = option.emulation_en;
}

void IFCFastIntAnalogPP::setPPMode(const timespec &timespec, const int32_t &value)
{
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));

	/* Put MODE = ZERO if configuration is invalid */
	switch (value) {

	case 0:
		option.mode = ifcfastint_amode_zero;
		break;

	case 1:
		option.mode = ifcfastint_amode_one;
		break;

	case 2:
		if (m_ppType == ifcfastint_analog_pp_lvlmon)
			option.mode = ifcfastint_amode_level2;
		else
			option.mode = ifcfastint_amode_zero;
		break;

	case 3:
		if (m_ppType == ifcfastint_analog_pp_lvlmon)
			option.mode = ifcfastint_amode_level3;
		else
			option.mode = ifcfastint_amode_zero;
		break;

	case 4:
		if (m_ppType == ifcfastint_analog_pp_lvlmon)
			option.mode = ifcfastint_amode_level4;
		else
			option.mode = ifcfastint_amode_zero;
		break;

	case 5:
		if (m_ppType == ifcfastint_analog_pp_lvlmon)
			// OPTION 5 is not working on firmware, force to 4
			option.mode = ifcfastint_amode_level4;
		else
			option.mode = ifcfastint_amode_zero;
		break;

	case 6:
		if (m_ppType == ifcfastint_analog_pp_pulshp)
			option.mode = ifcfastint_amode_shape;
		else
			option.mode = ifcfastint_amode_zero;
		break;

	case 7:
		if (m_ppType == ifcfastint_analog_pp_pulrate)
			option.mode = ifcfastint_amode_rate;
		else
			option.mode = ifcfastint_amode_zero;
		break;

	case 8:
		if (m_ppType == ifcfastint_analog_pp_devmon)
				option.mode = ifcfastint_amode_devmon;
		else
				option.mode = ifcfastint_amode_zero;
		break;

	default:
		option.mode = ifcfastint_amode_zero;
	}
	ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_MODE_W, m_ppType, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_pp_modePV.read(&now, &rb_val);
	m_pp_modePV.push(now, rb_val);
}

void IFCFastIntAnalogPP::getPPMode(timespec *timespec, int32_t *value) {
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	clock_gettime(CLOCK_REALTIME, timespec);
	ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, m_ppType, &option);
	switch (option.mode) {
		case ifcfastint_amode_zero:
			*value = 0;
			break;
		case ifcfastint_amode_one:
			*value = 1;
			break;
		case ifcfastint_amode_level2:
			*value = 2;
			break;
		case ifcfastint_amode_level3:
			*value = 3;
			break;
		case ifcfastint_amode_level4:
			*value = 4;
			break;
		case ifcfastint_amode_level5:
			*value = 5;
			break;
		case ifcfastint_amode_shape:
			*value = 6;
			break;
		case ifcfastint_amode_rate:
			*value = 7;
			break;
		case ifcfastint_amode_devmon:
			*value = 8;
			break;
		default:
			*value = 0;
	}
}

void IFCFastIntAnalogPP::setAutoreset(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	option.autoreset = static_cast<bool>(value);

	/* Write PP configuration according to channel num and pp type */
	ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_AUTORESET_W, m_ppType, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_autoresetPV.read(&now, &rb_val);
	m_autoresetPV.push(now, rb_val);
}
void IFCFastIntAnalogPP::getAutoreset(timespec *timespec, int32_t *value) {
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, m_ppType, &option);

	clock_gettime(CLOCK_REALTIME, timespec);
	*value = static_cast<int32_t>(option.autoreset);
}

/* VAL1 and VAL2 conversion from bits (counts) to EGU is done here at the device support */

void IFCFastIntAnalogPP::setPPVal1(const timespec &timespec, const double &value)
{
	ifcdaqdrv_status status;
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));

	int i = 0;
	switch (m_ppType)	{
	/* If this parameter is an analog value, it needs conversion to ADC counts */
	case ifcfastint_analog_pp_lvlmon:
	case ifcfastint_analog_pp_pulshp:
	case ifcfastint_analog_pp_pulrate:

		i = findADCCode(value, *m_scalingLut);

		if (m_DataFormat == fim_dataformat_signed) {
			option.i16val1 = (int)i - 32768;
			status = ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_SIGNED_VAL1_W, m_ppType, &option);
			if (status) {
				ndsErrorStream(m_node) << "Failed to set PP Val1" << std::endl;
			}
		} else {
			option.val1 = (i < 65536 ? i : 65535);
			status = ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_VAL1_W, m_ppType, &option);
			if (status) {
				ndsErrorStream(m_node) << "Failed to set PP Val1" << std::endl;
			}
		}
		break;

	/* Deviation monitor is a special case of ENUM values */
	case ifcfastint_analog_pp_devmon:
		if (value <= 3.0)
			option.devfact = 0x00;
		else if ((value > 3.0) && (value <= 5.0))
			option.devfact = 0x01;
		else if ((value > 5.0) && (value <= 10.0))
			option.devfact = 0x02;
		else // value = 0.15
			option.devfact = 0x03;

		status = ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_DEVMON_FAC_W, m_ppType, &option);
		if (status) {
			ndsErrorStream(m_node) << "Failed to set DEVMON Factor" << std::endl;
		}

		break;

	default:
		option.val1 = 0;
		break;
	}


	// Trigger an update of the readback value from hardware
	double rb_val;
	struct timespec now = {0, 0};
	m_pp_val1PV.read(&now, &rb_val);
	m_pp_val1PV.push(now, rb_val);
}

void IFCFastIntAnalogPP::getPPVal1(timespec *timespec, double *value) {
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, m_ppType, &option);

	double aux;
	int16_t i16val = static_cast<int16_t>(option.i16val1);
	size_t index;

	/* We need to convert from ADC counts to EGU using the lookup table */
	switch (m_ppType)
	{
	/* If this parameter is an analog value, it needs conversion to ADC counts */
	case ifcfastint_analog_pp_lvlmon:
	case ifcfastint_analog_pp_pulshp:
	case ifcfastint_analog_pp_pulrate:

		if (m_DataFormat == fim_dataformat_signed) {
			index = static_cast<size_t>((i16val+32768) & 0xffff);
			aux = m_scalingLut->at(index);
			break;
		} else {
			index = static_cast<size_t>(option.val1);
			aux = m_scalingLut->at(index);
			break;
		}

	/* Deviation monitor is a special case of ENUM values */
	case ifcfastint_analog_pp_devmon:
		if (option.devfact == 0x00)
			aux = 3.0;
		else if (option.devfact == 0x01)
			aux = 5.0;
		else if (option.devfact == 0x02)
			aux = 10.0;
		else // value = 0.15
			aux = 15.0;
		break;

	default:
		aux = 0;
		break;
	}

	clock_gettime(CLOCK_REALTIME, timespec);
	*value = aux;
}


void IFCFastIntAnalogPP::setPPVal2(const timespec &timespec, const double &value)
{
	ifcdaqdrv_status status;
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));

	double aux = 0;
	int i = 0;
	if (m_ppType == ifcfastint_analog_pp_lvlmon)
	{
		try  {
			i = findADCCode(value, *m_scalingLut);

			if (m_DataFormat == fim_dataformat_signed) {
				option.i16val2 = (int)i - 32768;
				status = ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_SIGNED_VAL2_W, m_ppType, &option);
				if (status) {
					ndsErrorStream(m_node) << "Failed to set PP Val1" << std::endl;
				}
			} else {
				option.val2 = (i < 65536 ? i : 65535);
				status = ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_VAL2_W, m_ppType, &option);
				if (status) {
					ndsErrorStream(m_node) << "Failed to set PP Val1" << std::endl;
				}
			}

		} catch (std::exception& e) {
			ndsErrorStream(m_node) << " Exception caught at setPPVal2: " << e.what() << std::endl;
			option.val2 = 0;
		}
	}

	/* VAL2 is pulse width max time (us) */
	else if ((m_ppType == ifcfastint_analog_pp_pulshp)||(m_ppType == ifcfastint_analog_pp_pulrate))
	{

		aux = value;

		/* Lower value guard */
		if (value <= 0 ) {
			aux = 0;
		}

		/* Max value is 0xFFFFF (1048575)*/
		if (value > 1048575) {
			aux = 1048575;
		}

		option.val2 = (uint32_t) aux;
		ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_VAL2_W, m_ppType, &option);
	}

	else if (m_ppType == ifcfastint_analog_pp_devmon)
	{
		uint32_t evaltime = 0;
		if (value == 1) evaltime = 0;
		else if (value == 2) evaltime = 1;
		else if (value <= 4) evaltime = 2;
		else if (value <= 8) evaltime = 3;
		else if (value <= 16) evaltime = 4;
		else if (value <= 32) evaltime = 5;
		else if (value <= 64) evaltime = 6;
		else if (value <= 128) evaltime = 7;
		else if (value <= 256) evaltime = 8;
		else if (value <= 512) evaltime = 9;
		else if (value <= 1024) evaltime = 10;
		else if (value <= 2048) evaltime = 11;
		else if (value <= 4096) evaltime = 12;
		else if (value <= 8192) evaltime = 13;
		else if (value <= 16384) evaltime = 14;
		else evaltime = 15;

		option.devtime = evaltime;
		ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_DEVMON_TIME_W, m_ppType, &option);
	}

	// Trigger an update of the readback value from hardware
	double rb_val;
	struct timespec now = {0, 0};
	m_pp_val2PV.read(&now, &rb_val);
	m_pp_val2PV.push(now, rb_val);
}

void IFCFastIntAnalogPP::getPPVal2(timespec *timespec, double *value) {
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	clock_gettime(CLOCK_REALTIME, timespec);
	ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, m_ppType, &option);

	if (m_ppType == ifcfastint_analog_pp_devmon)
	{
		*value = (double) (1 << option.devtime);
	} else if (m_ppType == ifcfastint_analog_pp_lvlmon) {

		int16_t i16val = static_cast<int16_t>(option.val2);
		size_t index;
		if (m_DataFormat == fim_dataformat_signed) {
			index = static_cast<size_t>(i16val+32768);
			*value = m_scalingLut->at(index);
		} else {
			index = static_cast<size_t>(i16val & 0xffff);
			*value = m_scalingLut->at(index);
		}

	} else	{
		*value = (double) option.val2;
	}
}

void IFCFastIntAnalogPP::setPPCVal(const timespec &timespec, const double &value)
{
	if (m_ppType == ifcfastint_analog_pp_devmon)
	{
		ifcdaqdrv_status status;
		struct ifcfastint_analog_option option;
		memset(&option, 0, sizeof(struct ifcfastint_analog_option));

		// if mode is deviation monitor, this parameter controls the reference value
		int i = findADCCode(value, *m_scalingLut);

		if (m_DataFormat == fim_dataformat_signed) {
			option.i16cval = (int)i - 32768;
			m_pp_cval = (int)i - 32768;
			status = ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_DEVMON_REF_W, m_ppType, &option);
			if (status) {
				ndsErrorStream(m_node) << "Failed to set CVAL" << std::endl;
			}
		}

		// Trigger an update of the readback value from hardware
		//double rb_val;
		double rb_val;
		struct timespec now = {0, 0};
		m_pp_cvalPV.read(&now, &rb_val);
		m_pp_cvalPV.push(now, rb_val);
	}
}

void IFCFastIntAnalogPP::getPPCVal(timespec *timespec, double *value) {
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));

	ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, m_ppType, &option);
	int16_t i16val = option.i16cval;
	size_t index = static_cast<size_t>((i16val+32768) & 0xffff);
	*value = m_scalingLut->at(index);
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAnalogPP::getRTStatus(timespec *timespec, int32_t *value) {
	*value = 0;
	clock_gettime(CLOCK_REALTIME, timespec);
}

/* Read firmware parameters to update real-time status of the measurements */
void IFCFastIntAnalogPP::refreshRTStatus(uint8_t *pp_measure_block) {

	uint64_t *pp_measure_register;
	pp_measure_register = (uint64_t *) (pp_measure_block + ((int)m_ppType*0x100) + (m_channelNum*8));
	ifcfastint_swap64(pp_measure_register);

	m_proc_out = (bool) (*pp_measure_register >> 63) & 1;

	switch(m_ppType) {
		case ifcfastint_analog_pp_lvlmon:
			m_i16pres_val  = static_cast<int16_t>((*pp_measure_register >> 16) & 0xFFFF);
			m_ui32pres_val = static_cast<uint32_t>((*pp_measure_register >> 16) & 0xFFFF);
			m_i16trig_val  = static_cast<int16_t>(*pp_measure_register & 0xFFFF);
			m_ui32trig_val = static_cast<uint32_t>(*pp_measure_register & 0xFFFF);
			break;

		case ifcfastint_analog_pp_devmon:
			m_i16trig_val  = static_cast<int16_t>(*pp_measure_register & 0xFFFF);
			m_ui32trig_val = static_cast<uint32_t>(*pp_measure_register & 0xFFFF);
			m_i16devmon_min  = static_cast<int16_t>((*pp_measure_register >> 16) & 0xFFFF);
			m_ui32devmon_min = static_cast<uint32_t>((*pp_measure_register >> 16) & 0xFFFF);
			m_i16devmon_max  = static_cast<int16_t>((*pp_measure_register >> 32) & 0xFFFF);
			m_ui32devmon_max = static_cast<uint32_t>((*pp_measure_register >> 32) & 0xFFFF);
			break;

		case ifcfastint_analog_pp_pulshp:
		case ifcfastint_analog_pp_pulrate:
			m_ui32pres_val = (*pp_measure_register >> 32) & 0xFFFFFF;
			m_ui32trig_val = (*pp_measure_register & 0xFFFFFF);
			m_i16pres_val = 0;
			m_i16trig_val = 0;
			break;

		default:
			break;
	}
}

void IFCFastIntAnalogPP::getProcOut(timespec *timespec, int32_t *value)
{
	*value = m_proc_out;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAnalogPP::updateProcOut(void)
{
	std::int32_t rb_val;
	struct timespec now = {0, 0};
	m_proc_outPV.read(&now, &rb_val);
	m_proc_outPV.push(now, rb_val);

}

void IFCFastIntAnalogPP::getPresVal(timespec *timespec, double *value)
{
	if ((m_ppType == ifcfastint_analog_pp_lvlmon) or (m_ppType == ifcfastint_analog_pp_devmon)){
		try {
			if (m_DataFormat == fim_dataformat_signed)
				*value = m_scalingLut->at(m_i16pres_val + 32768);
			else
				*value = m_scalingLut->at(m_ui32pres_val);

		} catch (std::exception& e) {
			ndsErrorStream(m_node) << " Exception caught at getPresVal: " << e.what() << std::endl;
			*value = 11;
		}
	}
	else
	{
		*value = static_cast<double>(m_ui32pres_val);
	}

	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAnalogPP::updatePresVal(void)
{
	double rb_val;
	struct timespec now = {0, 0};
	m_pres_valPV.read(&now, &rb_val);
	m_pres_valPV.push(now, rb_val);
}


void IFCFastIntAnalogPP::getTrigVal(timespec *timespec, double *value)
{
	if ((m_ppType == ifcfastint_analog_pp_lvlmon) or (m_ppType == ifcfastint_analog_pp_devmon)){
		try {
			if (m_DataFormat == fim_dataformat_signed)
				*value = m_scalingLut->at(m_i16trig_val + 32768);
			else
				*value = m_scalingLut->at(m_ui32trig_val);
		} catch (std::exception& e) {
			ndsErrorStream(m_node) << " Exception caught at getTrigVal: " << e.what() << std::endl;
			*value = 11;
		}
	}
	else
	{
		*value = static_cast<double>(m_ui32trig_val);;
	}

	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAnalogPP::updateTrigVal(void)
{
	double rb_val;
	struct timespec now = {0, 0};
	m_trig_valPV.read(&now, &rb_val);
	m_trig_valPV.push(now, rb_val);
}


void IFCFastIntAnalogPP::setEnDevReference(const timespec &timespec, const int32_t &value)
{
	if (m_ppType == ifcfastint_analog_pp_devmon)
	{
		struct ifcfastint_analog_option option;
		memset(&option, 0, sizeof(struct ifcfastint_analog_option));

		// Force CVAL to be ZERO if DISABLING fixed reference on the DevMon PP block
		if (value == 0)
			option.i16cval = 0;
		else
			option.i16cval = m_pp_cval;

		ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_DEVMON_REF_W, m_ppType, &option);

		// Trigger an update of the readback value from hardware
		double rb_val;
		struct timespec now = {0, 0};
		m_pp_cvalPV.read(&now, &rb_val);
		m_pp_cvalPV.push(now, rb_val);
	}

	m_en_devreference = (bool) value;
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_en_devreferencePV.read(&now, &rb_val);
	m_en_devreferencePV.push(now, rb_val);
}

void IFCFastIntAnalogPP::getEnDevReference(timespec *timespec, int32_t *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = (int32_t) m_en_devreference;
}

void IFCFastIntAnalogPP::setPolarity(const timespec &timespec, const int32_t &value) {
	if ((m_ppType == ifcfastint_analog_pp_pulrate) || (m_ppType == ifcfastint_analog_pp_pulshp)) {

		struct ifcfastint_analog_option option;
		memset(&option, 0, sizeof(struct ifcfastint_analog_option));

		if (value) {
			option.polarity = 1;
		} else {
			option.polarity = 0;
		}

		ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_POLARITY_W, m_ppType, &option);
	}

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_polarityPV.read(&now, &rb_val);
	m_polarityPV.push(now, rb_val);
}


void IFCFastIntAnalogPP::getPolarity(timespec *timespec, int32_t *value) {
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, m_ppType, &option);
	*value = (int32_t) option.polarity;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAnalogPP::getDevMonMin(timespec *timespec, double *value) {
	if (m_ppType == ifcfastint_analog_pp_devmon){
		try {
			if (m_DataFormat == fim_dataformat_signed)
				*value = m_scalingLut->at(m_i16devmon_min + 32768);
			else
				*value = m_scalingLut->at(m_ui32devmon_min);
		} catch (std::exception& e) {
			ndsErrorStream(m_node) << " Exception caught at getDevMonMax: " << e.what() << std::endl;
			*value = 0;
		}
	}
	else
	{
		*value = 0;
	}
}

void IFCFastIntAnalogPP::getDevMonMax(timespec *timespec, double *value) {
	if (m_ppType == ifcfastint_analog_pp_devmon){
		try {
			if (m_DataFormat == fim_dataformat_signed)
				*value = m_scalingLut->at(m_i16devmon_max + 32768);
			else
				*value = m_scalingLut->at(m_ui32devmon_max);
		} catch (std::exception& e) {
			ndsErrorStream(m_node) << " Exception caught at getDevMonMax: " << e.what() << std::endl;
			*value = 0;
		}
	}
	else
	{
		*value = 0;
	}

	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAnalogPP::updateDevMonMax(void)
{
	double rb_val;
	struct timespec now = {0, 0};
	m_devmon_maxPV.read(&now, &rb_val);
	m_devmon_maxPV.push(now, rb_val);
}

void IFCFastIntAnalogPP::updateDevMonMin(void)
{
	double rb_val;
	struct timespec now = {0, 0};
	m_devmon_minPV.read(&now, &rb_val);
	m_devmon_minPV.push(now, rb_val);
}
