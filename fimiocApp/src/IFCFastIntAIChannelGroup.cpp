#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>

#include "device.h"
#include "IFCFastIntAIChannelGroup.h"
#include "IFCFastIntAIChannel.h"
#include "IFCFastIntAnalogPP.h"
#include "IFCFastIntPowerAvg.h"

#define FMC_INDEX_1 0x01

IFCFastIntAIChannelGroup::IFCFastIntAIChannelGroup(const std::string &name, nds::Node &parentNode,
													ifcdaqdrv_usr &deviceUser, std::string &calibFolder,
													size_t grpIndex, const fim_dataformat_t &dataformat)
		: m_node(nds::Port(name, nds::nodeType_t::generic)),
			m_deviceUser(deviceUser), m_calibFolder(calibFolder),
			m_HwTimestamp({0,0}),
			m_infoPV(nds::PVDelegateIn<std::string>(
					"InfoMessage",
					std::bind(&IFCFastIntAIChannelGroup::getInfoMessage, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_sampleRatePV(nds::PVDelegateIn<double>(
					"SampleRate-RB",
					std::bind(&IFCFastIntAIChannelGroup::getSampleRate, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_clockSourcePV(nds::PVDelegateIn<std::int32_t>(
					"ClockSource-RB",
					std::bind(&IFCFastIntAIChannelGroup::getClockSource, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_clockFrequencyPV(nds::PVDelegateIn<double>(
					"ClockFrequency-RB",
					std::bind(&IFCFastIntAIChannelGroup::getClockFrequency, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_clockDivisorPV(nds::PVDelegateIn<std::int32_t>(
					"ClockDivisor-RB",
					std::bind(&IFCFastIntAIChannelGroup::getClockDivisor, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_ADCRangePV(nds::PVDelegateIn<std::vector<std::int32_t>>(
					"ADCRANGE", std::bind(&IFCFastIntAIChannelGroup::getADCRange, this,
								std::placeholders::_1, std::placeholders::_2)))
{
	parentNode.addChild(m_node);

	/* TODO: Check ifcdaqdrv for ADC3117 number of channels */
	m_numChannels = 20;

	/* Object creation */
	for (size_t numChannel(10*grpIndex); numChannel < (10+(10*grpIndex)); ++numChannel)
	{
		std::ostringstream channelName;
		channelName << "CH" << numChannel;
		m_AIChannels.push_back(std::make_shared<IFCFastIntAIChannel>(
				channelName.str(), m_node, numChannel, m_deviceUser, 20.0/65536.0,
				m_calibFolder, dataformat));
	}

	m_sampleRatePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_sampleRatePV);

	// PVs for Clock Source
	nds::enumerationStrings_t clockSourceStrings;
	clockSourceStrings.push_back("Internal");
	clockSourceStrings.push_back("External");
	m_clockSourcePV.setScanType(nds::scanType_t::interrupt);
	m_clockSourcePV.setEnumeration(clockSourceStrings);
	m_node.addChild(m_clockSourcePV);

	// PVs for Clock Frequency
	m_clockFrequencyPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_clockFrequencyPV);

	// PVs for Clock Divisor
	m_clockDivisorPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_clockDivisorPV);

	// PV for debug/info messages.
	m_infoPV.setScanType(nds::scanType_t::interrupt);
	m_infoPV.setMaxElements(512);
	m_node.addChild(m_infoPV);


	m_ADCRangePV.setScanType(nds::scanType_t::passive);
	m_node.addChild(m_ADCRangePV);

	// Initialize some values from hardware
	ifcdaqdrv_get_clock_source(&deviceUser, (ifcdaqdrv_clock *)&m_clockSource);
	ifcdaqdrv_get_clock_frequency(&deviceUser, &m_clockFrequency);
	ifcdaqdrv_get_clock_divisor(&deviceUser, (uint32_t *)&m_clockDivisor);

	m_sampleRate = m_clockFrequency / m_clockDivisor;

	m_ADCRange.resize(65536);
	int32_t adcraw = -32768;
	for (auto& iter : m_ADCRange) {
		iter = adcraw;
		++adcraw;
	}

}

void IFCFastIntAIChannelGroup::setState(nds::state_t newState) {
	for (auto channel : m_AIChannels) {
		channel->setState(newState);
	}
}

std::vector<std::shared_ptr<IFCFastIntAIChannel>>
IFCFastIntAIChannelGroup::getChannels() {
	return m_AIChannels;
}

void IFCFastIntAIChannelGroup::getTriggerButton(timespec *timespec,
																								std::string *value) {}

void IFCFastIntAIChannelGroup::getInfoMessage(timespec *timespec,
																							std::string *value) {
	std::ostringstream tmp;
	char manufacturer[100];
	ifcdaqdrv_get_manufacturer(&m_deviceUser, manufacturer, 100);
	char product_name[100];
	ifcdaqdrv_get_product_name(&m_deviceUser, product_name, 100);
	tmp << manufacturer << " " << product_name;
	*value = tmp.str();
}

#if 0
#define MAX_SAMPLE_RATES 100

struct sample_rate {
	double frequency;
	uint32_t divisor;
	uint32_t decimation;
	uint32_t average;
	double sample_rate;
};

// First priority is sample_rate, second divisor
static int compare_sample_rates(const void *a, const void *b) {
	const struct sample_rate *da = (const struct sample_rate *)a;
	const struct sample_rate *db = (const struct sample_rate *)b;
	int32_t sample_diff =
			(da->sample_rate > db->sample_rate) - (da->sample_rate < db->sample_rate);
	if (!sample_diff) {
		return da->divisor - db->divisor;
	}
	return sample_diff;
}
#endif

void IFCFastIntAIChannelGroup::getSampleRate(timespec *timespec,
																						 double *value) {
	*value = m_sampleRate;
}

void IFCFastIntAIChannelGroup::getClockSource(timespec *timespec,
																							int32_t *value)
{
	//ifcdaqdrv_status status;
	ifcdaqdrv_clock clock;
	ifcdaqdrv_get_clock_source(&m_deviceUser, &clock);
	*value = (int32_t) clock;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAIChannelGroup::getClockFrequency(timespec *timespec, double *value)
{
	//ifcdaqdrv_status status;
	double freq;
	ifcdaqdrv_get_clock_frequency(&m_deviceUser, &freq);
	*value = freq;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntAIChannelGroup::getClockDivisor(timespec *timespec, int32_t *value)
{
	//ifcdaqdrv_status status;
	int32_t divisor;
	ifcdaqdrv_get_clock_divisor(&m_deviceUser, (uint32_t *)&divisor);
	*value = divisor;
	clock_gettime(CLOCK_REALTIME, timespec);
}

/* This application has fixed acquisition parameter values, therefore commitParameters
is only to read those values */
void IFCFastIntAIChannelGroup::commitParameters(bool calledFromAcquisitionThread)
{
	struct timespec now = {0, 0};
	//ifcdaqdrv_status status;

	clock_gettime(CLOCK_REALTIME, &now);

	/* Reads clock source */
	m_clockSourcePV.read(&now, &m_clockSource);
	m_clockSourcePV.push(now, m_clockSource);

	/* Reads clock frequency */
	m_clockFrequencyPV.read(&now, &m_clockFrequency);
	m_clockFrequencyPV.push(now, m_clockFrequency);

	/* Reads clock divisor */
	m_clockDivisorPV.read(&now, &m_clockDivisor);
	m_clockDivisorPV.push(now, m_clockDivisor);

	/* Calculates sampling rate */
	m_sampleRate = m_clockFrequency / m_clockDivisor;
	m_sampleRatePV.push(now, m_sampleRate);

#if 0
	/* If the call comes from the acquisition thread it is certain that the device
	 * is not armed. Otherwise, only allow changes if device is in on or going to
	 * on. */

	// if(!calledFromAcquisitionThread && (
	//        m_stateMachine.getLocalState() != nds::state_t::on &&
	//        m_stateMachine.getLocalState() != nds::state_t::stopping  &&
	//        m_stateMachine.getLocalState() != nds::state_t::initializing )) {
	//    return;
	//}

	if (m_clockSourceChanged) {
		m_clockSourceChanged = false;

		status = ifcfastint_set_clock_source(&m_deviceUser, (ifcdaqdrv_clock) m_clockSource, FMC_INDEX_1);

		FIMIOC_STATUS_CHECK("set_clock_source", status);
		if (!status)
		{
			struct timespec now = {0, 0};
			m_clockSourcePV.read(&now, &m_clockSource);
			m_clockSourcePV.push(now, m_clockSource);
		}
	}

	if (m_clockFrequencyChanged) {
		m_clockFrequencyChanged = false;
		status = ifcfastint_set_clock_frequency(&m_deviceUser, m_clockFrequency, FMC_INDEX_1);
		FIMIOC_STATUS_CHECK("set_clock_frequency", status);
		if (!status) {
			struct timespec now = {0, 0};
			m_clockFrequencyPV.read(&now, &m_clockFrequency);
			m_clockFrequencyPV.push(now, m_clockFrequency);
			m_sampleRate = m_clockFrequency / m_clockDivisor;
			m_sampleRatePV.push(now, m_sampleRate);
		}
	}

	if (m_clockDivisorChanged) {
		m_clockDivisorChanged = false;
		status = ifcfastint_set_clock_divisor(&m_deviceUser, m_clockDivisor, FMC_INDEX_1);
		FIMIOC_STATUS_CHECK("set_clock_divisor", status);
		if (!status) {
			struct timespec now = {0, 0};
			m_clockDivisorPV.read(&now, &m_clockDivisor);
			m_clockDivisorPV.push(now, m_clockDivisor);
			m_sampleRate = m_clockFrequency / m_clockDivisor;
			m_sampleRatePV.push(now, m_sampleRate);
		}
	}
#endif

}

void IFCFastIntAIChannelGroup::onSwitchOn() {
	// Enable all channels
	for (auto const &channel : m_AIChannels) {
		channel->setState(nds::state_t::on);
	}
	commitParameters();
}

void IFCFastIntAIChannelGroup::onSwitchOff() {
	// Disable all channels
	for (auto const &channel : m_AIChannels) {
		channel->setState(nds::state_t::off);
	}
}

void IFCFastIntAIChannelGroup::onStart() {
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	/* Start all Channels */
	for (auto const &channel : m_AIChannels) {
		channel->setState(nds::state_t::running);
	}
}

void IFCFastIntAIChannelGroup::onStop() {
	// ifcdaqdrv_disarm_device(&m_deviceUser);
	// Stop channels
	for (auto const &channel : m_AIChannels) {
		channel->setState(nds::state_t::on);
	}

	commitParameters();
}

void IFCFastIntAIChannelGroup::recover() {
	throw nds::StateMachineRollBack("Cannot recover");
}

bool IFCFastIntAIChannelGroup::allowChange(const nds::state_t currentLocal,
																					 const nds::state_t currentGlobal,
																					 const nds::state_t nextLocal) {
	return true;
}


void IFCFastIntAIChannelGroup::getADCRange(timespec *timespec, std::vector<std::int32_t> *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = m_ADCRange;
}


// Placeholder function - NOT IN USE FOR NOW
// receive external hardware timestamp and keep it as class param
void IFCFastIntAIChannelGroup::setHwTimestamp(const timespec& ts) {
	m_HwTimestamp.tv_sec = ts.tv_sec;
	m_HwTimestamp.tv_nsec = ts.tv_nsec;
	for (auto const &channel : m_AIChannels) {
		channel->setHwTimestamp(ts);
	}
}
