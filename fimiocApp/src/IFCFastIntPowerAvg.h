#ifndef IFCFastIntPowerAvg_H
#define IFCFastIntPowerAvg_H

#include <nds3/nds.h>
#include <ifcfastintdrv.h>

class IFCFastIntPowerAvg {
public:
	IFCFastIntPowerAvg(const std::string &name, nds::Node &parentNode,
                      int32_t pwravgNum, struct ifcdaqdrv_usr &deviceUser);

  // Class public parameters
  std::int32_t m_pwravgNum;

  void addValue(int32_t data);
  void getData(timespec *timespec, double *value);
  void getHistory(timespec *timespec, std::vector<double> *value);
  void getPPQHistory(timespec *timespec, std::vector<std::uint8_t> *value);

  void historyAddValue(int32_t data);
  void historyAddPPQValue(uint8_t data);
  void historyReset();
  void reserve(size_t nframes);

  void setState(nds::state_t newState);

  void setPPEmulated(const timespec &timespec, const int32_t &value);
  void getPPEmulated(timespec *timespec, int32_t *value);

  void setPPMode(const timespec &timespec, const int32_t &value);
  void getPPMode(timespec *timespec, int32_t *value);

  void setVinput(const timespec &timespec, const int32_t &value);
  void getVinput(timespec *timespec, int32_t *value);

  void setIinput(const timespec &timespec, const int32_t &value);
  void getIinput(timespec *timespec, int32_t *value);

  void setWinSize(const timespec &timespec, const int32_t &value);
  void getWinSize(timespec *timespec, int32_t *value);

  void setMaxInstPwr(const timespec &timespec, const double &value);
  void getMaxInstPwr(timespec *timespec, double *value);

  void setMaxAvgPower(const timespec &timespec, const double &value);
  void getMaxAvgPower(timespec *timespec, double *value);

  void setPPCVal(const timespec &timespec, const double &value);
  void getPPCVal(timespec *timespec, double *value);

  void setPPOutput(bool qout);
  void getPPQOut(timespec *timespec, int32_t *value);

  void setInterlockStateTransitionIdlePre(const timespec &timespec, const int32_t &value);
  void getInterlockStateTransitionIdlePre(timespec *timespec, int32_t *value);

  void setInterlockStateTransitionPreRun(const timespec &timespec, const int32_t &value);
  void getInterlockStateTransitionPreRun(timespec *timespec, int32_t *value);

  //void commitParameters(bool calledFromAcquisitionThread = false);

private:

  struct ifcdaqdrv_usr &m_deviceUser;

  double m_data;
  std::vector<double> m_history;
  std::vector<double>::iterator m_history_ptr;

  std::vector<uint8_t> m_ppqhistory;
  std::vector<uint8_t>::iterator m_ppqhistory_ptr;

  //nds::Node m_node;
  nds::Port m_node;
  nds::StateMachine m_stateMachine;

  bool m_firstReadout; /* Flag to indicate that this is the first time data is read out. */

  void switchOn();
  void switchOff();
  void start();
  void stop();
  void recover();
  bool allowChange(const nds::state_t, const nds::state_t, const nds::state_t);

  /* Preprocssing sub-block */
  bool m_pp_out;

  bool m_pp_emulated;
  ifcfastint_amode m_pp_mode;
  double m_pp_cval;
  double m_maxinstpower;
  double m_maxavgpower;
  int32_t m_winsize;
  int32_t m_vInput;
  int32_t m_iInput;

  /* PV for channel data */
  nds::PVDelegateIn<double> m_dataPV;
  nds::PVDelegateIn<std::vector<double>> m_historyPV;
  nds::PVDelegateIn<std::vector<std::uint8_t>> m_ppqhistoryPV;
  nds::PVDelegateIn<std::int32_t> m_pp_outPV;

  /* Preprocssing sub-block */
  nds::PVDelegateIn<std::int32_t> m_pp_emulatedPV;
  nds::PVDelegateIn<std::int32_t> m_pp_modePV;
  nds::PVDelegateIn<double> m_pp_cvalPV;
  nds::PVDelegateIn<std::int32_t> m_fsmTransIdlePrePV;
  nds::PVDelegateIn<std::int32_t> m_fsmTransPreRunPV;
  nds::PVDelegateIn<std::int32_t> m_vInputPV;
  nds::PVDelegateIn<std::int32_t> m_iInputPV;
  nds::PVDelegateIn<std::int32_t> m_winSizePV;
  nds::PVDelegateIn<double> m_maxinstpowerPV;
  nds::PVDelegateIn<double> m_maxavgpowerPV;

};

class IFCFastIntPowerAvgGroup {
public:
	IFCFastIntPowerAvgGroup(const std::string &name, nds::Node &parentNode,ifcdaqdrv_usr &deviceUser);

	nds::Port m_node;
	ifcdaqdrv_usr &m_deviceUser;
	std::vector<std::shared_ptr<IFCFastIntPowerAvg>> m_PwrAvgChannels;

	std::vector<std::shared_ptr<IFCFastIntPowerAvg>> getChannels();
	//void getInfoMessage(timespec *timespec, std::string *value);

  void setState(nds::state_t newState);

	private:
};



#endif //IFCFastIntPowerAvg_H
