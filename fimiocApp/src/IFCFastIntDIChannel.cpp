
#include <cstdlib>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <climits>
#include <algorithm>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>
#include <ifcfastintdrv.h>

#include "device.h"
#include "IFCFastIntDIChannel.h"
#include "interlock_info.h"

IFCFastIntDIChannel::IFCFastIntDIChannel(const std::string &name,
																				 nds::Node &parentNode,
																				 int32_t channelNum,
																				 ifcdaqdrv_usr &deviceUser)
		: m_channelNum(channelNum), m_firstIlck(IFCFASTINT_CH_OK),
			m_deviceUser(deviceUser), m_ch_enable(true),
			m_ilckStatus(false), m_HVONprecond(false), m_RFONprecond(false),
			m_HwTimestamp({0,0}),
			m_dataPV(nds::PVDelegateIn<std::int32_t>(
					"Data", std::bind(&IFCFastIntDIChannel::getData, this,
														std::placeholders::_1, std::placeholders::_2))),
			m_historyPV(nds::PVDelegateIn<std::vector<std::int32_t>>(
					"History", std::bind(&IFCFastIntDIChannel::getHistory, this,
															 std::placeholders::_1, std::placeholders::_2))),
			m_ppqhistoryPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
					"PPQHistory", std::bind(&IFCFastIntDIChannel::getPPQHistory, this,
															 std::placeholders::_1, std::placeholders::_2))),
			m_pmortemPV(nds::PVDelegateIn<std::vector<double>>(
					"PmortemHistory", std::bind(&IFCFastIntDIChannel::getHistPMortem, this,
															 std::placeholders::_1, std::placeholders::_2))),
			m_ppq_pmortemPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
					"PPQPmortem", std::bind(&IFCFastIntDIChannel::getPPQPmortem, this,
															 std::placeholders::_1, std::placeholders::_2))),
			m_firstIlckPV(nds::PVDelegateIn<std::int32_t>(
					"PPQOUT", std::bind(&IFCFastIntDIChannel::getPPQOut, this,
															std::placeholders::_1, std::placeholders::_2))),
			m_pp_emulatedPV(nds::PVDelegateIn<std::int32_t>(
					"PP-EMUL-RB",
					std::bind(&IFCFastIntDIChannel::getPPEmulated, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_pp_modePV(nds::PVDelegateIn<std::int32_t>(
					"PP-MODE-RB",
					std::bind(&IFCFastIntDIChannel::getPPMode, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_pp_val1PV(nds::PVDelegateIn<std::int32_t>(
					"PP-VAL1-RB",
					std::bind(&IFCFastIntDIChannel::getPPVal1, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_pp_val2PV(nds::PVDelegateIn<std::int32_t>(
					"PP-VAL2-RB",
					std::bind(&IFCFastIntDIChannel::getPPVal2, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_pp_cvalPV(nds::PVDelegateIn<std::int32_t>(
					"PP-CVAL-RB",
					std::bind(&IFCFastIntDIChannel::getPPCVal, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_fsmTransIdlePrePV(nds::PVDelegateIn<std::int32_t>(
					"TRANS-IDLE2PRE-RB",
					std::bind(&IFCFastIntDIChannel::getInterlockStateTransitionIdlePre,
										this, std::placeholders::_1, std::placeholders::_2))),
			m_fsmTransPreRunPV(nds::PVDelegateIn<std::int32_t>(
					"TRANS-PRE2RUN-RB",
					std::bind(&IFCFastIntDIChannel::getInterlockStateTransitionPreRun,
										this, std::placeholders::_1, std::placeholders::_2))),
			m_ch_enablePV(nds::PVDelegateIn<std::int32_t>(
					"ChEnable-RB", std::bind(&IFCFastIntDIChannel::getChEnable, this,
															std::placeholders::_1, std::placeholders::_2))),
			m_autoresetPV(nds::PVDelegateIn<std::int32_t>(
					"PP-AUTORESET-RB",
					std::bind(&IFCFastIntDIChannel::getAutoreset, this,
										std::placeholders::_1, std::placeholders::_2))),
			m_ilckStatusPV(nds::PVVariableIn<std::int32_t>("ilckStatus")),
			m_HVONPreCondPV(nds::PVVariableIn<std::int32_t>("HVONPreCond")),
			m_RFONPreCondPV(nds::PVVariableIn<std::int32_t>("RFONPreCond"))
{
	m_node = nds::Port(name, nds::nodeType_t::generic);
	parentNode.addChild(m_node);

	m_stateMachine = nds::StateMachine(
			true, std::bind(&IFCFastIntDIChannel::switchOn, this),
			std::bind(&IFCFastIntDIChannel::switchOff, this),
			std::bind(&IFCFastIntDIChannel::start, this),
			std::bind(&IFCFastIntDIChannel::stop, this),
			std::bind(&IFCFastIntDIChannel::recover, this),
			std::bind(&IFCFastIntDIChannel::allowChange, this, std::placeholders::_1,
								std::placeholders::_2, std::placeholders::_3));
	m_stateMachine.setLogLevel(nds::logLevel_t::none);

	m_node.addChild(m_stateMachine);

	/* Add data PV */
	m_dataPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_dataPV);

	/* Add history PV */
	m_historyPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_historyPV);

	m_ppqhistoryPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_ppqhistoryPV);


	m_pmortemPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pmortemPV);
	m_ppq_pmortemPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_ppq_pmortemPV);



	/* Add QOUT PV */
	m_firstIlckPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_firstIlckPV);

	/* Add PV for Interlock Transisition qualifier*/
	nds::PVDelegateOut<std::int32_t> node = nds::PVDelegateOut<std::int32_t>(
			"PP-EMUL", std::bind(&IFCFastIntDIChannel::setPPEmulated, this,
													 std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntDIChannel::getPPEmulated, this,
								std::placeholders::_1, std::placeholders::_2));
	m_node.addChild(node);
	/* Readback PV */
	m_pp_emulatedPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pp_emulatedPV);

	/* Add PV for Interlock Transisition qualifier*/
	node = nds::PVDelegateOut<std::int32_t>(
			"PP-MODE", std::bind(&IFCFastIntDIChannel::setPPMode, this,
													 std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntDIChannel::getPPMode, this, std::placeholders::_1,
								std::placeholders::_2));
	m_node.addChild(node);
	/* Readback PV */
	m_pp_modePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pp_modePV);

	/* Add PV for Interlock Transisition qualifier*/
	node = nds::PVDelegateOut<std::int32_t>(
			"PP-VAL1", std::bind(&IFCFastIntDIChannel::setPPVal1, this,
													 std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntDIChannel::getPPVal1, this, std::placeholders::_1,
								std::placeholders::_2));
	m_node.addChild(node);
	/* Readback PV */
	m_pp_val1PV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pp_val1PV);

	/* Add PV for Interlock Transisition qualifier*/
	node = nds::PVDelegateOut<std::int32_t>(
			"PP-VAL2", std::bind(&IFCFastIntDIChannel::setPPVal2, this,
													 std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntDIChannel::getPPVal2, this, std::placeholders::_1,
								std::placeholders::_2));
	m_node.addChild(node);
	/* Readback PV */
	m_pp_val2PV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pp_val2PV);

	/* Add PV for Interlock Transisition qualifier*/
	node = nds::PVDelegateOut<std::int32_t>(
			"PP-CVAL", std::bind(&IFCFastIntDIChannel::setPPCVal, this,
													 std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntDIChannel::getPPCVal, this, std::placeholders::_1,
								std::placeholders::_2));
	m_node.addChild(node);
	/* Readback PV */
	m_pp_cvalPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pp_cvalPV);

	/* Add PV for Interlock Transisition qualifier*/
	node = nds::PVDelegateOut<std::int32_t>(
			"TRANS-IDLE2PRE",
			std::bind(&IFCFastIntDIChannel::setInterlockStateTransitionIdlePre, this,
								std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntDIChannel::getInterlockStateTransitionIdlePre, this,
								std::placeholders::_1, std::placeholders::_2));
	m_node.addChild(node);
	/* Readback PV */
	m_fsmTransIdlePrePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_fsmTransIdlePrePV);

	/* Add PV for Interlock Transisition qualifier*/
	node = nds::PVDelegateOut<std::int32_t>(
			"TRANS-PRE2RUN",
			std::bind(&IFCFastIntDIChannel::setInterlockStateTransitionPreRun, this,
								std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntDIChannel::getInterlockStateTransitionPreRun, this,
								std::placeholders::_1, std::placeholders::_2));
	m_node.addChild(node);
	/* Readback PV */
	m_fsmTransPreRunPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_fsmTransPreRunPV);

	/* Add Channel Enable PV */
	node = nds::PVDelegateOut<std::int32_t>(
			"ChEnable",
			std::bind(&IFCFastIntDIChannel::setChEnable, this,
													 std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntDIChannel::getChEnable, this,
														std::placeholders::_1, std::placeholders::_2));
	m_node.addChild(node);
	m_ch_enablePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_ch_enablePV);

	node = nds::PVDelegateOut<std::int32_t>(
			"RESET-ILCK",
			std::bind(&IFCFastIntDIChannel::resetInterlock, this,
													 std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntDIChannel::getresetInterlock, this,
														std::placeholders::_1, std::placeholders::_2));
	m_node.addChild(node);


	/* Add autoreset Enable PV */
	node = nds::PVDelegateOut<std::int32_t>(
			"PP-AUTORESET",
			std::bind(&IFCFastIntDIChannel::setAutoreset, this,
													 std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntDIChannel::getAutoreset, this,
														std::placeholders::_1, std::placeholders::_2));
	m_node.addChild(node);
	m_autoresetPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_autoresetPV);

	m_node.addChild(m_ilckStatusPV);
	m_node.addChild(m_HVONPreCondPV);
	m_node.addChild(m_RFONPreCondPV);

	m_InterlockInfo = std::make_shared<FIM_InterlockInfo>("ILCK", m_node, channelNum, fim_digital_in);


	// Clear the PP configuration settings
	// TODO(nc): should this be controlled from PVs?
	// struct ifcfastint_digital_option option;
	// memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	// ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, 0xff, &option);
}

void IFCFastIntDIChannel::setChEnable(const timespec &timespec, const int32_t &value) {
	struct timespec tmp;
	int32_t rb_value;

	m_ch_enable = (bool) value;

	// Push a readback value update
	m_ch_enablePV.read(&tmp, &rb_value);
	m_ch_enablePV.push(tmp, rb_value);
}

void IFCFastIntDIChannel::getChEnable(timespec *timespec, int32_t *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = (int32_t) m_ch_enable;
}


void IFCFastIntDIChannel::reserve(size_t nframes) {
	m_history.resize(nframes);
	m_history_ptr = m_history.begin();

	m_ppqhistory.resize(nframes);
	m_ppqhistory_ptr = m_ppqhistory.begin();
}

void IFCFastIntDIChannel::historyReset(bool pmortem) {
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	/* If this is a post-mortem event, push data to EPICS */
	if (pmortem == true){
		m_pmortemPV.push(now, m_history);
		m_ppq_pmortemPV.push(now, m_ppqhistory);
	}

	if (m_ch_enable == true)
	{
		m_historyPV.push(now, m_history);
		m_ppqhistoryPV.push(now, m_ppqhistory);
	}
	m_history.clear();

	/* Before reset PPQ (interlock status) vector, generate slow update status */
	if (std::find(m_ppqhistory.begin(), m_ppqhistory.end(), 0) != m_ppqhistory.end()) {
		m_ilckStatus = false;
	} else {
		m_ilckStatus = true;
	}
	m_ilckStatusPV.push(now, static_cast<int32_t>(m_ilckStatus));

	// Update the precond PVs
	m_HVONprecond = (m_HVONenabled == true ? m_ilckStatus : true);
	m_HVONPreCondPV.push(now, static_cast<int32_t>(m_HVONprecond));
	m_RFONprecond = (m_RFONenabled == true ? m_ilckStatus : true);
	m_RFONPreCondPV.push(now, static_cast<int32_t>(m_RFONprecond));

	m_ppqhistory.clear();
}

void IFCFastIntDIChannel::historyAddValue(int32_t data) {
	*m_history_ptr = data;
	m_history_ptr++;
}

void IFCFastIntDIChannel::historyAddPPQValue(uint8_t data) {
	*m_ppqhistory_ptr = (std::uint8_t) data;
	m_ppqhistory_ptr++;
}

void IFCFastIntDIChannel::addValue(int32_t data) {
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	if (m_ch_enable == true)
	{
		m_dataPV.push(now, data);
	}
}

void IFCFastIntDIChannel::setPPOutput(int32_t ilck) {
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	if ((!m_HVONenabled) && (!m_RFONenabled))
	{
		m_firstIlck = IFCFASTINT_CH_OK;
	} else {
		m_firstIlck = ilck;
	}

	m_firstIlckPV.push(now, m_firstIlck);
}

void IFCFastIntDIChannel::getPPQOut(timespec *timespec, int32_t *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = m_firstIlck;
}
void IFCFastIntDIChannel::getData(timespec *timespec, int32_t *value) {}
void IFCFastIntDIChannel::getHistory(timespec *timespec, std::vector<std::int32_t> *value) {}
void IFCFastIntDIChannel::getPPQHistory(timespec *timespec, std::vector<std::uint8_t> *value) {}
void IFCFastIntDIChannel::getPPQPmortem(timespec *timespec, std::vector<std::uint8_t> *value) {}
void IFCFastIntDIChannel::getHistPMortem(timespec *timespec, std::vector<double> *value) {}

void IFCFastIntDIChannel::switchOn() {}
void IFCFastIntDIChannel::switchOff() {}
void IFCFastIntDIChannel::start() {}
void IFCFastIntDIChannel::stop() {}

/** @brief Don't support recover */
void IFCFastIntDIChannel::recover() {
	throw nds::StateMachineRollBack("Cannot recover");
}

/** @brief Allow all transitions */
bool IFCFastIntDIChannel::allowChange(const nds::state_t, const nds::state_t,const nds::state_t) {
	return true;
}

void IFCFastIntDIChannel::setState(nds::state_t newState) {
	m_stateMachine.setState(newState);
}

void IFCFastIntDIChannel::setPPEmulated(const timespec &timespec,const int32_t &value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	option.emulation_en = value;
	ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, IFCFASTINT_DIGITAL_EMULATION_EN_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_pp_emulatedPV.read(&now, &rb_val);
	m_pp_emulatedPV.push(now, rb_val);
}

void IFCFastIntDIChannel::getPPEmulated(timespec *timespec, int32_t *value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);

	clock_gettime(CLOCK_REALTIME, timespec);
	*value = option.emulation_en;
}

void IFCFastIntDIChannel::setPPMode(const timespec &timespec,const int32_t &value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	switch (value) {
	case 0:
		option.mode = ifcfastint_dmode_zero;
		break;
	case 1:
		option.mode = ifcfastint_dmode_one;
		break;
	case 2:
		option.mode = ifcfastint_dmode_pass_through;
		break;
	case 3:
		option.mode = ifcfastint_dmode_invert;
		break;
	}
	ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, IFCFASTINT_DIGITAL_MODE_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_pp_modePV.read(&now, &rb_val);
	m_pp_modePV.push(now, rb_val);
}

void IFCFastIntDIChannel::getPPMode(timespec *timespec, int32_t *value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	clock_gettime(CLOCK_REALTIME, timespec);
	ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
	switch (option.mode) {
	case ifcfastint_dmode_zero:
		*value = 0;
		break;
	case ifcfastint_dmode_one:
		*value = 1;
		break;
	case ifcfastint_dmode_pass_through:
		*value = 2;
		break;
	case ifcfastint_dmode_invert:
		*value = 3;
		break;
	}
}

void IFCFastIntDIChannel::setPPVal1(const timespec &timespec,const int32_t &value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	option.val1 = value;
	ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, IFCFASTINT_DIGITAL_VAL1_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_pp_val1PV.read(&now, &rb_val);
	m_pp_val1PV.push(now, rb_val);
}

void IFCFastIntDIChannel::getPPVal1(timespec *timespec, int32_t *value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
	*value = option.val1;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntDIChannel::setPPVal2(const timespec &timespec,const int32_t &value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	option.val2 = value;
	ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, IFCFASTINT_DIGITAL_VAL2_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_pp_val2PV.read(&now, &rb_val);
	m_pp_val2PV.push(now, rb_val);
}

void IFCFastIntDIChannel::getPPVal2(timespec *timespec, int32_t *value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
	*value = option.val2;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntDIChannel::setPPCVal(const timespec &timespec,const int32_t &value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	// option.cval = (int16_t)((value - m_linConvOffset) / m_linConvFactor);
	// option.cval = signed_to_raw(option.cval);
	option.cval = value;
	ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, IFCFASTINT_DIGITAL_CVAL_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_pp_cvalPV.read(&now, &rb_val);
	m_pp_cvalPV.push(now, rb_val);
}

void IFCFastIntDIChannel::getPPCVal(timespec *timespec, int32_t *value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	// int16_t cval;
	ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
	*value = option.cval;
	// cval = raw_to_signed(option.cval);
	//*value = cval * m_linConvFactor + m_linConvOffset;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntDIChannel::setInterlockStateTransitionIdlePre(
		const timespec &timespec, const int32_t &value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	option.idle2pre = value;
	ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, IFCFASTINT_DIGITAL_IDLE2PRE_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_fsmTransIdlePrePV.read(&now, &rb_val);
	m_fsmTransIdlePrePV.push(now, rb_val);
}

void IFCFastIntDIChannel::getInterlockStateTransitionIdlePre(timespec *timespec, int32_t *value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
	*value = option.idle2pre;
	clock_gettime(CLOCK_REALTIME, timespec);

	/* Update current status of the condition */
	if (*value) {
		m_HVONenabled = true;
	} else {
		m_HVONenabled = false;
	}
}

void IFCFastIntDIChannel::setInterlockStateTransitionPreRun(
		const timespec &timespec, const int32_t &value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	option.pre2run = value;
	ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, IFCFASTINT_DIGITAL_PRE2RUN_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_fsmTransPreRunPV.read(&now, &rb_val);
	m_fsmTransPreRunPV.push(now, rb_val);
}

void IFCFastIntDIChannel::getInterlockStateTransitionPreRun(timespec *timespec,	int32_t *value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);
	*value = option.pre2run;
	clock_gettime(CLOCK_REALTIME, timespec);

	/* Update current status of the condition */
	if (*value) {
		m_RFONenabled = true;
	} else {
		m_RFONenabled = false;
	}
}

void IFCFastIntDIChannel::resetInterlock(const timespec &timespec, const int32_t &value)
{
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);
	struct ifcfastint_digital_option option_digi;
	ifcfastint_dmode original_mode;

	// reset block
	ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option_digi);

	/* Ignore this procedure if the mode is already modified */
	if (option_digi.mode != ifcfastint_dmode_zero) {
		//Force mode to ONE
		original_mode = option_digi.mode;
		option_digi.mode = ifcfastint_dmode_one;
		ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, IFCFASTINT_DIGITAL_MODE_W, &option_digi);

		//Force mode back to original mode
		option_digi.mode = original_mode;
		ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, IFCFASTINT_DIGITAL_MODE_W, &option_digi);
	}

	/* Test if channel is being monitored or not */
	m_firstIlck = IFCFASTINT_CH_OK;
	m_firstIlckPV.push(now, m_firstIlck);
}

void IFCFastIntDIChannel::getresetInterlock(timespec *timespec, int32_t *value)
{
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = 0;
}


uint8_t IFCFastIntDIChannel::detectInterlock(void)
{
	std::vector<std::uint8_t>::iterator local_iter;
	uint8_t last_sample;
	uint8_t return_val;

	last_sample = m_ppqhistory[DETECT_START - 1];
	return_val = IFCFASTINT_CH_OK;

	// Loop the vector to search for a HIGH to LOW transition
	for (local_iter = (m_ppqhistory.begin() + DETECT_START); local_iter != (m_ppqhistory.end() - DETECT_PMORTEM_SIZE + DETECT_POST_WINDOW); local_iter++)
	{
		if ((*local_iter == 0) && (last_sample == 1)) {
			// interlock detected! increase counter!
			m_InterlockInfo->registerInterlock();
			return_val = 1;
			break;
		}
		last_sample = *local_iter;
	}
	return return_val;
}

void IFCFastIntDIChannel::setAutoreset(const timespec &timespec, const int32_t &value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	option.autoreset = static_cast<bool>(value);
	ifcfastint_set_conf_digital_pp(&m_deviceUser, m_channelNum, IFCFASTINT_DIGITAL_AUTORESET_W, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_autoresetPV.read(&now, &rb_val);
	m_autoresetPV.push(now, rb_val);
}

void IFCFastIntDIChannel::getAutoreset(timespec *timespec, int32_t *value) {
	struct ifcfastint_digital_option option;
	memset(&option, 0, sizeof(struct ifcfastint_digital_option));
	ifcfastint_get_conf_digital_pp(&m_deviceUser, m_channelNum, &option);

	clock_gettime(CLOCK_REALTIME, timespec);
	*value = static_cast<int32_t>(option.autoreset);
}

// Placeholder function - NOT IN USE FOR NOW
// receive external hardware timestamp and keep it as class param
void IFCFastIntDIChannel::setHwTimestamp(const timespec& ts) {
	m_HwTimestamp.tv_sec = ts.tv_sec;
	m_HwTimestamp.tv_nsec = ts.tv_nsec;
}
