#include <cstdlib>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <climits>
#include <unistd.h>
#include <inttypes.h>
#include <utility>
#include <cmath>

#include <thread>
#include <chrono>
#include <future>
#include <algorithm>

#include <iomanip>

#include "nds3/nds.h"
#include "ifcdaqdrv.h"
#include "ifcfastintdrv.h"

#include "device.h"
#include "IFCFastIntPowerAvg.h"
#include "IFCFastIntAIChannelGroup.h"
#include "IFCFastIntAIChannel.h"
#include "IFCFastIntAnalogPP.h"
#include "IFCFastIntDIChannelGroup.h"
#include "IFCFastIntDIChannel.h"
#include "IFCFastIntInterlockFSM.h"
#include "IFCFastIntSpecial.h"

IFCFastIntInterlockFSM::IFCFastIntInterlockFSM(const std::string &name,	nds::Node &parentNode,
						ifcdaqdrv_usr &deviceUser, std::string &calibFolder, size_t nframes, int dataformat) :

m_node(nds::Port(name, nds::nodeType_t::generic)),
m_deviceUser(deviceUser),
m_calibFolder(calibFolder),
m_DataFormat((fim_dataformat_t)dataformat),
m_InitComplete(false),
acqEvent(MSGQUEUECAPACITY, sizeof(fimEventMsg)),
ilckEvent(100, (64*8*1024*sizeof(int8_t))),
m_nFrames(nframes),
m_fsmFrequency(1000),
m_ppconf_locked(1),
m_ProcPeriod(200000),
m_IntrFreq(0.0),
m_bkpTriggerLine(0),
m_AutoResetPeriod(1.0),
m_AutoResetPeriodQuantum(1.0 / FASTRESET_GRANULARITY),
m_CurrentInterlockCount(0),
m_skipNormalRead(false),
m_HwTimestamp({0,0}),m_HwTimestampUpdate({0,0}),

// eventTimer member objects
timerQueue(epicsTimerQueueActive::allocate(false, TimerThreadPriority)),
autoResetQueue(epicsTimerQueueActive::allocate(false, autoResetThreadPriority)),
scalarReadsTimer(eventTimer(std::bind(&IFCFastIntInterlockFSM::processScalarReads, this), 0.5, timerQueue)),
fimStateMachineTimer(eventTimer(std::bind(&IFCFastIntInterlockFSM::processFimStateMachine, this), 0.071428, timerQueue)),
interlockResetTimer(eventTimer(std::bind(&IFCFastIntInterlockFSM::resetInterlockCounter, this), FASTRESET_GRANULARITY, autoResetQueue)),
// INFO PV
m_infoPV(nds::PVDelegateIn<std::string>(
	"InfoMessage",
	std::bind(&IFCFastIntInterlockFSM::getInfoMessage, this,
		std::placeholders::_1, std::placeholders::_2))),

// Number of Samples readback PV
m_nSamplesPV(nds::PVVariableIn<std::int32_t>("NSamples-RB")),

// Frequency readback PV
m_fsmFrequencyPV(nds::PVVariableIn<std::int32_t>("Frequency-RB")),

// FIM State Machine readback PV
m_intFSMPV(nds::PVDelegateIn<std::int32_t>(
	"InterlockState-RB",
	std::bind(&IFCFastIntInterlockFSM::getInterlockState, this,
		std::placeholders::_1, std::placeholders::_2))),

// Enable PP block modifications readback PV
m_pplockPV(nds::PVDelegateIn<std::int32_t>(
	"PP-Lock-RB",
	std::bind(&IFCFastIntInterlockFSM::getPPLock, this,
		std::placeholders::_1, std::placeholders::_2))),

// Main (global) autoreset PV
m_MainAutoResetPV(nds::PVDelegateIn<std::int32_t>(
	"GLBAUTORESET-RB",
	std::bind(&IFCFastIntInterlockFSM::getAutoreset, this,
		std::placeholders::_1, std::placeholders::_2))),

// General read-back PVs
m_HistModePV(nds::PVVariableIn<std::int32_t>("HistMod-RB")),
m_HistEnabledPV(nds::PVVariableIn<std::int32_t>("HistEn-RB")),
m_StatusRegPV(nds::PVVariableIn<std::int32_t>("StatusReg-RB")),
m_HistoryStatePV(nds::PVVariableIn<std::int32_t>("HistoryState-RB")),
m_HistOverFFPV(nds::PVVariableIn<std::int32_t>("HistOverFF-RB")),
m_HistRingOverFFPV(nds::PVVariableIn<std::int32_t>(	"HistRingOverFF-RB")),
m_LoopPeriod(nds::PVVariableOut<std::int32_t>("LoopPeriod")),
m_LoopPeriodPV(nds::PVVariableIn<std::int32_t>("LoopPeriod-RB")),
m_ProcPeriodPV(nds::PVVariableIn<std::int32_t>("ProcPeriod-RB")),
m_bkpTriggerLinePV(nds::PVVariableIn<std::int32_t>("TriggerLine-RB")),
m_IntrFreqPV(nds::PVVariableIn<double>("IntrFreq-RB")),
m_QueueStatusPV(nds::PVVariableIn<std::int32_t>("QueueStats-RB")),
m_RawAI0PV(nds::PVVariableIn<std::int32_t>("RawAI0-RB")),
m_RawAI19PV(nds::PVVariableIn<std::int32_t>("RawAI19-RB")),
m_OutsMSBPV(nds::PVVariableIn<std::int32_t>("OutsMSB-RB")),
m_OutsLSBPV(nds::PVVariableIn<std::int32_t>("OutsLSB-RB")),
m_SelectRegisterPV(nds::PVVariableOut<std::int32_t>("SelectReg")),
m_RawRegisterPV(nds::PVVariableIn<std::int32_t>("RawReg-RB")),
m_SelectTMEMRegisterPV(nds::PVVariableOut<std::int32_t>("SelectTmemReg")),
m_RawTMEMRegisterLPV(nds::PVVariableIn<std::int32_t>("RawTmemRegL-RB")),
m_RawTMEMRegisterHPV(nds::PVVariableIn<std::int32_t>("RawTmemRegH-RB")),

m_AutoResetPeriodPV(nds::PVDelegateIn<double>(
	"AUTORESETPERIOD-RB",
	std::bind(&IFCFastIntInterlockFSM::getAutoResetPeriod, this,
		std::placeholders::_1, std::placeholders::_2))),

m_AutoResetMaxIlckPV(nds::PVVariableOut<std::int32_t>("AUTORESET-MAXILCK")),
m_AutoResetDurationPV(nds::PVVariableIn<double>("AUTORESET-ELAPSED")),
m_AutoResetIlckCountPV(nds::PVVariableIn<std::int32_t>("AUTORESET-COUNT")),
m_fsmHistoryBufferPV(nds::PVDelegateIn<std::vector<int32_t> >("FSMTREND",
	std::bind(&IFCFastIntInterlockFSM::getFSMTrend, this,
		std::placeholders::_1, std::placeholders::_2))),

// Generic purpose digital output
m_gpdoPV(nds::PVVariableIn<std::int32_t>("GPDO-RB")),
m_HVONPreCondPV(nds::PVVariableIn<std::int32_t>("HVONPreCond")),
m_RFONPreCondPV(nds::PVVariableIn<std::int32_t>("RFONPreCond")),

// Hardware/EVR timestamp management PVs
m_HwTimestampPV(nds::PVVariableIn<std::string>("EVR-TIMESTAMP-RB")),
m_HwTimestampPeriodPV(nds::PVVariableIn<double>("EVR-TS-UPDATE-RB")),
m_HwTimestampOffsetNs(nds::PVVariableOut<std::int32_t>("EVR-TS-ADJUST-NS")),
m_HwTimestampOK(nds::PVVariableOut<std::int32_t>("EVR-TS-OK")),

// Flags for interlock threads status
m_ilckIntrThreadRunning(false),
m_interlockThreadRunning(false),
m_acqIntrThreadRunning(false),
m_waveAcquisitionThreadRunning(false)

{
	ndsDebugStream(m_node) << "[FIMIOC] IFCFastIntInterlockFSM constructor " << std::endl;

	struct timespec now = {0, 0};
	clock_gettime(CLOCK_REALTIME, &now);

	parentNode.addChild(m_node);

	// Allocate data for ring buffer frames
	m_rawData = (int8_t *)calloc(HISTORY_FRAME_SIZE * HISTORY_FRAME_COUNT_MAX,sizeof(int8_t));
	if (!m_rawData) {
		throw nds::NdsError("Out of memory");
	}

	// Clear vector for measurement data
	memset(m_rawMeasData, 0, 1024);

	//-----------------------------------------------------------------------------
	// Start of PV objects construction and addition to the tree
	//-----------------------------------------------------------------------------

  	// Setpoint PV for FIM state machine
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
		"Command", std::bind(&IFCFastIntInterlockFSM::sendCommand, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::recvCommand, this,
			std::placeholders::_1, std::placeholders::_2)));

	m_nSamplesPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_nSamplesPV);

  	// PVs for FSM Frequency
	m_fsmFrequencyPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_fsmFrequencyPV);

  	// PVs for FSM state
	nds::enumerationStrings_t interlockStateStrings;
	interlockStateStrings.push_back("IDLE");
	interlockStateStrings.push_back("ARM");
	interlockStateStrings.push_back("HV ON");
	interlockStateStrings.push_back("RF ON");
	interlockStateStrings.push_back("ABO");

	m_intFSMPV.setScanType(nds::scanType_t::interrupt);
	m_intFSMPV.setEnumeration(interlockStateStrings);
	m_node.addChild(m_intFSMPV);

  	// Setpoint PV to reset history buffer */
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
		"GLBAUTORESET", std::bind(&IFCFastIntInterlockFSM::setAutoreset, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::getAutoreset, this,
			std::placeholders::_1, std::placeholders::_2)));

  	// Main register (raw) PV
	m_MainAutoResetPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_MainAutoResetPV);

  	// PV for debug/info messages.
	m_infoPV.setScanType(nds::scanType_t::interrupt);
	m_infoPV.setMaxElements(512);
	m_node.addChild(m_infoPV);

  	// PVs for NDS state machine.
	m_stateMachine = m_node.addChild(nds::StateMachine(
		true, std::bind(&IFCFastIntInterlockFSM::onSwitchOn, this),
		std::bind(&IFCFastIntInterlockFSM::onSwitchOff, this),
		std::bind(&IFCFastIntInterlockFSM::onStart, this),
		std::bind(&IFCFastIntInterlockFSM::onStop, this),
		std::bind(&IFCFastIntInterlockFSM::recover, this),
		std::bind(&IFCFastIntInterlockFSM::allowChange, this,
			std::placeholders::_1, std::placeholders::_2,
			std::placeholders::_3)));
	m_stateMachine.setLogLevel(nds::logLevel_t::none);

	m_pplockPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pplockPV);

  	// Setpoint PV for the HISTORY MODE
	nds::enumerationStrings_t historyModeStrings;
	historyModeStrings.push_back("I_P_R_1K");
	historyModeStrings.push_back("P_R_1K");
	historyModeStrings.push_back("R_1K");
	historyModeStrings.push_back("R_1M");
	historyModeStrings.push_back("I_P_R_1K_OF");
	historyModeStrings.push_back("P_R_1K_OF");
	historyModeStrings.push_back("R_1K_OF");
	historyModeStrings.push_back("R_1M_OF");

	nds::PVDelegateOut<std::int32_t> aux_node(nds::PVDelegateOut<std::int32_t>(
		"HistMod", std::bind(&IFCFastIntInterlockFSM::setHistMode, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::getHistMode, this,
			std::placeholders::_1, std::placeholders::_2)));
	aux_node.setEnumeration(historyModeStrings);
	m_node.addChild(aux_node);

	m_HistModePV.setEnumeration(historyModeStrings);
	m_HistModePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_HistModePV);

  	// Setpoint PV for History Enabled
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
		"HistEn", std::bind(&IFCFastIntInterlockFSM::setHistEnabled, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::getHistEnabled, this,
			std::placeholders::_1, std::placeholders::_2)));

	m_HistEnabledPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_HistEnabledPV);

  	// Setpoint PV to reset history buffer */
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
		"ResetHistBuf", std::bind(&IFCFastIntInterlockFSM::resetHistBuf, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::getresetHistBuf, this,
			std::placeholders::_1, std::placeholders::_2)));

  	// Main register (raw) PV
	m_StatusRegPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_StatusRegPV);

	// General PVs for History Buffer Control
	nds::enumerationStrings_t historyStateStrings;
	historyModeStrings.push_back("IDLE");
	historyModeStrings.push_back("RUNNING");
	historyModeStrings.push_back("PMORTEM");
	historyModeStrings.push_back("ENDED");

	m_HistoryStatePV.setScanType(nds::scanType_t::interrupt);
	m_HistoryStatePV.setEnumeration(historyStateStrings);
	m_node.addChild(m_HistoryStatePV);

	m_HistOverFFPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_HistOverFFPV);

	m_HistRingOverFFPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_HistRingOverFFPV);

	// Setpoint PV for loop period control
	m_node.addChild(m_LoopPeriod);
	m_LoopPeriodPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_LoopPeriodPV);

	// Readback PV for real-time processing period
	m_ProcPeriodPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_ProcPeriodPV);

	// Setpoint PV for backplane trigger selection
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
		"TriggerLine", std::bind(&IFCFastIntInterlockFSM::setBkpTrigLine, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::getBkpTrigLine, this,
			std::placeholders::_1, std::placeholders::_2)));
	m_bkpTriggerLinePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_bkpTriggerLinePV);

	// Reset all interlocks
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
		"ResetAll", std::bind(&IFCFastIntInterlockFSM::resetAll, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::getResetAll, this,
			std::placeholders::_1, std::placeholders::_2)));

	// Readback PV for interrupt frequency
	m_IntrFreqPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_IntrFreqPV);

	// Readback PV for message queue status
	m_QueueStatusPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_QueueStatusPV);

	// Raw Analog input (diagnostics) PVs
	m_RawAI0PV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_RawAI0PV);
	m_RawAI19PV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_RawAI19PV);

	// Digital Output real-time status readback PVs
	m_OutsMSBPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_OutsMSBPV);
	m_OutsLSBPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_OutsLSBPV);

	m_RawRegisterPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_RawRegisterPV);
	m_node.addChild(m_SelectRegisterPV);

	m_RawTMEMRegisterHPV.setScanType(nds::scanType_t::interrupt);
	m_RawTMEMRegisterLPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_SelectTMEMRegisterPV);
	m_node.addChild(m_RawTMEMRegisterHPV);
	m_node.addChild(m_RawTMEMRegisterLPV);

	// PV for enable fake analog signals on AI16 to AI19
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
		"FakeAnalogIn", std::bind(&IFCFastIntInterlockFSM::setFakeAnalogIn, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::getFakeAnalogIn, this,
			std::placeholders::_1, std::placeholders::_2)));

	// Auto-reset PVs
	m_node.addChild(nds::PVDelegateOut<double>(
		"AUTORESETPERIOD", std::bind(&IFCFastIntInterlockFSM::setAutoResetPeriod, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::getAutoResetPeriod, this,
			std::placeholders::_1, std::placeholders::_2)));
	m_AutoResetPeriodPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_AutoResetPeriodPV);

	m_node.addChild(m_AutoResetMaxIlckPV);

	m_AutoResetDurationPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_AutoResetDurationPV);

	m_AutoResetIlckCountPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_AutoResetIlckCountPV);
	m_AutoResetIlckCountPV.setValue(m_CurrentInterlockCount);

	m_fsmHistoryBufferPV.setScanType(nds::scanType_t::interrupt);
	//m_fsmHistoryBufferPV.setMaxElements(m_nFrames);
	m_node.addChild(m_fsmHistoryBufferPV);

	// PV for generic purpose digital output
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
		"GPDO", std::bind(&IFCFastIntInterlockFSM::setGPDO, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::getGPDO, this,
			std::placeholders::_1, std::placeholders::_2)));
	m_gpdoPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_gpdoPV);

	m_node.addChild(m_HVONPreCondPV);
	m_node.addChild(m_RFONPreCondPV);

	m_node.addChild(nds::PVDelegateOut<std::string>(
		"EVR-TIMESTAMP", std::bind(&IFCFastIntInterlockFSM::setHwTimestampStr, this,
			std::placeholders::_1, std::placeholders::_2),
		std::bind(&IFCFastIntInterlockFSM::getHwTimestampStr, this,
			std::placeholders::_1, std::placeholders::_2)));

	m_HwTimestampPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_HwTimestampPV);

	m_HwTimestampPeriodPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_HwTimestampPeriodPV);

	m_node.addChild(m_HwTimestampOffsetNs);
	m_node.addChild(m_HwTimestampOK);

	//-----------------------------------------------------------------------------
	// End of PV objects construction
	//-----------------------------------------------------------------------------

  	// Check endianess of the processor
	m_BigEndian = CheckEndianess();

	// Perform firmware parameters initialization
	initSystem();

  	// Construction of child nodes of the main device
	m_AIGroup.push_back(std::make_shared<IFCFastIntAIChannelGroup>("AI0", m_node, m_deviceUser, m_calibFolder, 0, m_DataFormat));
	m_AIGroup.push_back(std::make_shared<IFCFastIntAIChannelGroup>("AI1", m_node, m_deviceUser, m_calibFolder, 1, m_DataFormat));
	m_DIGroup = std::make_shared<IFCFastIntDIChannelGroup>("DI", m_node, m_deviceUser);
	m_PwrAvgGroup= std::make_shared<IFCFastIntPowerAvgGroup>("AVG", m_node, m_deviceUser);

	// create map with all the calibration-scaling tables
	for (auto channel : m_AIGroup[0]->getChannels()) {
		m_calibTable.insert(std::make_pair(channel->m_channelNum, channel->copyCalibLut()));
	}
	for (auto channel : m_AIGroup[1]->getChannels()) {
		m_calibTable.insert(std::make_pair(channel->m_channelNum, channel->copyCalibLut()));
	}

	//create the reflected power blocks and forward the calibration table of all channels
	m_PwrRefGroup = std::make_shared<IFCFastIntSpecialGroup>("PR", m_node, m_deviceUser, m_calibTable, m_DataFormat, fim_reflected_power);
	m_CavDecGroup = std::make_shared<IFCFastIntSpecialGroup>("CD", m_node, m_deviceUser, m_calibTable, m_DataFormat, fim_cavity_decay);

	//Start timer thread to read firmware parameters periodically
    //scalarReadsTimer.start();
	//fimStateMachineTimer.start();
}

//-----------------------------------------------------------------------------
//  Destructor
//-----------------------------------------------------------------------------
IFCFastIntInterlockFSM::~IFCFastIntInterlockFSM() {
	ndsDebugStream(m_node) << "[FIMIOC] Destroying IFCFastIntInterlockFSM" << std::endl;
	scalarReadsTimer.destroy();
	fimStateMachineTimer.destroy();
	interlockResetTimer.destroy();
}

//-----------------------------------------------------------------------------
//  Timer that performs regular reads to hardware
//-----------------------------------------------------------------------------
double IFCFastIntInterlockFSM::processScalarReads(void) {

	// checkCallbackThread(__func__);

	// Get values from hardware and push to EPICS
	readFPGAinfo();

	return DefaultInterval;
}

//-----------------------------------------------------------------------------
//  Timer that regularly updates FIM state machine
//-----------------------------------------------------------------------------
double IFCFastIntInterlockFSM::processFimStateMachine(void) {

	// checkCallbackThread(__func__);
	struct timespec ts;
	int32_t rb_value;
	m_intFSMPV.read(&ts, &rb_value);
	m_intFSMPV.push(ts, rb_value);

	return DefaultInterval;
}

//-----------------------------------------------------------------------------
//  Delegate write function that sends new state machine command to hadware
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::sendCommand(const timespec &timespec, const int32_t &value) {

	// Checks if there is an active interlock
	// TODO: use getter function instead of member variable
	bool allowChange = true;
	for (auto channel : m_AIGroup[0]->getChannels()) {
		if (channel->m_firstIlck > 0) {
			allowChange = false;
			break;
		}
	}
	for (auto channel : m_AIGroup[1]->getChannels()) {
		if (channel->m_firstIlck > 0) {
			allowChange = false;
			break;
		}
	}
	for (auto channel : m_DIGroup->getChannels()) {
		if (channel->m_firstIlck > 0) {
			allowChange = false;
			break;
		}
	}

	if (value == FIM_STATE_ABO)
	{
		ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_abort);
	}
	else if (value == FIM_STATE_HVON)
	{
		// Prevents the state machine to go to HV ON if there is a non-ack interlock
		if (allowChange) ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_hvon);
	}
	else if (value == FIM_STATE_RFON)
	{
		// Prevents the state machine to go to RF ON if there is a non-ack interlock
		if (allowChange) ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_rfon);
	}
	else if (value == FIM_STATE_IDLE)
	{
		ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_idle);
	}
	else {
		ndsErrorStream(m_node) << "[FIMIOC] Unidentified state machine command: " << value << std::endl;
	}

	// Readback of interlock state machine
	//fimStateMachineTimer.wakeUp();
	struct timespec ts;
	int32_t rb_value;
	m_intFSMPV.read(&ts, &rb_value);
	m_intFSMPV.push(ts, rb_value);
}

//-----------------------------------------------------------------------------
//  Placeholder for PVDelegateOut init function
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::recvCommand(timespec *timespec, int32_t *value) {}

//-----------------------------------------------------------------------------
//  TODO: debug this
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::getInfoMessage(timespec *timespec,
	std::string *value) {
	std::ostringstream tmp;
	char manufacturer[100];
	ifcdaqdrv_get_manufacturer(&m_deviceUser, manufacturer, 100);
	char product_name[100];
	ifcdaqdrv_get_product_name(&m_deviceUser, product_name, 100);
	tmp << manufacturer << " " << product_name;
	*value = tmp.str();
}

//-----------------------------------------------------------------------------
//  Reads the firwmare the obtain the current state of the FIM state machine
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::getInterlockState(timespec *timespec, int32_t *value) {
	ifcdaqdrv_status status;

	clock_gettime(CLOCK_REALTIME, timespec);
	status = ifcfastint_get_fsm_state(&m_deviceUser, &m_FIM_StateMachine);
	if (status) {
		ndsErrorStream(m_node) << "[FIMIOC] ifcfastint_get_fsm_state failed" << std::endl;
	}
	*value = static_cast<int32_t>(m_FIM_StateMachine);
}

//-----------------------------------------------------------------------------
// TODO: Enable locking the pre-processing configuration
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::updatePPLock() {
	struct timespec timespec;
	int32_t rb_value;

	m_pplockPV.read(&timespec, &rb_value);
	m_pplockPV.push(timespec, rb_value);
}

void IFCFastIntInterlockFSM::getPPLock(timespec *timespec, int32_t *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = m_ppconf_locked;
}

// void IFCFastIntInterlockFSM::setPPLock(const timespec& timespec, const
// int32_t& value)
//{
//    struct timespec now;
//    int32_t rb_value;
//    if(value) {
//        m_ppconf_locked = 1;
//        ifcfastint_conf_lock(&m_deviceUser);
//    } else {
//        m_ppconf_locked = 0;
//        ifcfastint_conf_unlock(&m_deviceUser);
//    }
//
//    //m_pplockPV.read(&now, &rb_value);
//    //m_pplockPV.push(now, rb_value);
//}


//-----------------------------------------------------------------------------
// Enable/Disable global auto-reset mode
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::setAutoreset(const timespec &timespec, const int32_t &value) {
	ifcdaqdrv_status status = ifcfastint_set_global_autoreset(&m_deviceUser, value);
	FIMIOC_STATUS_CHECK("ifcfastint_set_global_autoreset", status);
	scalarReadsTimer.wakeUp();
}
void IFCFastIntInterlockFSM::getAutoreset(timespec *timespec, int32_t *value) {
	int32_t autoreset;
	ifcdaqdrv_status status = ifcfastint_get_global_autoreset(&m_deviceUser, &autoreset);
	FIMIOC_STATUS_CHECK("ifcfastint_get_global_autoreset", status);

	*value = autoreset;
	clock_gettime(CLOCK_REALTIME, timespec);
}

//-----------------------------------------------------------------------------
// Delegate functions for set history buffer enabled (PVDelegateOut)
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::getHistEnabled(timespec *timespec, int32_t *value)
{
	ifcfastint_histcontrol hist_status;
	ifcdaqdrv_status status = ifcfastint_get_history_status(&m_deviceUser, &hist_status);
	FIMIOC_STATUS_CHECK("ifcfastint_set_history_status", status);
	*value = (int32_t) hist_status;
	clock_gettime(CLOCK_REALTIME, timespec);
}

void IFCFastIntInterlockFSM::setHistEnabled(const timespec &timespec, const int32_t &value)
{
	ifcdaqdrv_status status;
	if (value)
		status = ifcfastint_set_history_status(&m_deviceUser, ifcfastint_history_enabled);
	else
		status = ifcfastint_set_history_status(&m_deviceUser, ifcfastint_history_disabled);

	FIMIOC_STATUS_CHECK("ifcfastint_set_history_status", status);
	scalarReadsTimer.wakeUp();
}

//-----------------------------------------------------------------------------
// Delegate functions for RESET HISTORY BUFFER (PVDelegateOut)
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::resetHistBuf(const timespec &timespec, const int32_t &value)
{
	ifcdaqdrv_status status;
	/* Simply enable/disable history buffer */
	if (value) {
		status = ifcfastint_history_reset(&m_deviceUser);
		if (status) {
			ndsErrorStream(m_node) << "[FIMIOC] ifcfastint_history_reset failed" << std::endl;
		}
	}
}
void IFCFastIntInterlockFSM::getresetHistBuf(timespec *timespec, int32_t *value) {
	*value = 0;
	clock_gettime(CLOCK_REALTIME, timespec);
}

//-----------------------------------------------------------------------------
// Delegate functions for SET BACKPLANE TRIGGER LINE (PVDelegateOut)
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::setBkpTrigLine(const timespec &timespec, const int32_t &value)
{
	ifcdaqdrv_status status = ifcfastint_set_timingmask(&m_deviceUser, value);
	FIMIOC_STATUS_CHECK("ifcfastint_set_timingmask", status);
	scalarReadsTimer.wakeUp();
}
void IFCFastIntInterlockFSM::getBkpTrigLine(timespec *timespec, int32_t *value)
{
	uint32_t bkpTrigLine;
	ifcdaqdrv_status status = ifcfastint_get_timingmask(&m_deviceUser, &bkpTrigLine);
	FIMIOC_STATUS_CHECK("ifcfastint_get_timingmask", status);

	*value = (int32_t) bkpTrigLine;
	clock_gettime(CLOCK_REALTIME, timespec);
}

//-----------------------------------------------------------------------------
// Write the generic purpose digital outpu
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::getGPDO(timespec *timespec, int32_t *value) {
	int32_t gpdo_rb;
	ifcdaqdrv_status status = ifcfastint_get_special_do(&m_deviceUser, &gpdo_rb);
	FIMIOC_STATUS_CHECK("ifcfastint_get_special_do", status);

	*value = (int32_t) gpdo_rb;
	clock_gettime(CLOCK_REALTIME, timespec);
}
void IFCFastIntInterlockFSM::setGPDO(const timespec &timespec, const int32_t &value) {
	ifcdaqdrv_status status = ifcfastint_set_special_do(&m_deviceUser, value);
	FIMIOC_STATUS_CHECK("ifcfastint_set_special_do", status);
	scalarReadsTimer.wakeUp();
}

//-----------------------------------------------------------------------------
// Delegate functions for RESET ALL NODES (PVDelegateOut)
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::resetAll(const timespec &timespec, const int32_t &value)
{
	ifcdaqdrv_status status = ifcfastint_general_reset(&m_deviceUser);
	FIMIOC_STATUS_CHECK("ifcfastint_general_reset", status);

	// When general reset, calls individual channels reset with value = 2
	// to differentiate from individual reset
	for (auto channel : m_AIGroup[0]->getChannels()) {
		channel->resetInterlock(timespec, 2);
	}
	for (auto channel : m_AIGroup[1]->getChannels()) {
		channel->resetInterlock(timespec, 2);
	}
	for (auto channel : m_DIGroup->getChannels()) {
		channel->resetInterlock(timespec, 2);
	}
	for (auto channel : m_PwrRefGroup->getChannels()) {
		channel->resetInterlock(timespec, 2);
	}
	for (auto channel : m_CavDecGroup->getChannels()) {
		channel->resetInterlock(timespec, 2);
	}
}

void IFCFastIntInterlockFSM::getResetAll(timespec *timespec, int32_t *value)
{
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = 0;
}

//-----------------------------------------------------------------------------
// Delegate functions for HISTORY BUFFER MODE (PVDelegateOut)
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::setHistMode(const timespec &timespec, const int32_t &value)
{
	if (value <= 7) {
		ifcfastint_histmode mode = static_cast<ifcfastint_histmode>(value);
		ifcdaqdrv_status status = ifcfastint_set_history_mode(&m_deviceUser, mode);
		FIMIOC_STATUS_CHECK("ifcfastint_set_history_mode", status);
	}
	scalarReadsTimer.wakeUp();
}
void IFCFastIntInterlockFSM::getHistMode(timespec *timespec, int32_t *value)
{
	ifcfastint_histmode hist_mode;
	ifcdaqdrv_status status = ifcfastint_get_history_mode(&m_deviceUser, &hist_mode);
	FIMIOC_STATUS_CHECK("ifcfastint_set_history_mode", status);
	*value = static_cast<int32_t>(hist_mode);
	clock_gettime(CLOCK_REALTIME, timespec);
}


//-----------------------------------------------------------------------------
// Delegate functions to enable FAKE ANALOG INPUT data on channels AI16 to AI19
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::setFakeAnalogIn(const timespec &timespec, const int32_t &value)
{
	int32_t bool_val = (value > 0) ? 1 : 0;
	ifcdaqdrv_status status = ifcfastint_set_fakeanalogin(&m_deviceUser, bool_val);
	FIMIOC_STATUS_CHECK("ifcfastint_set_fakeanalogin", status);
}

void IFCFastIntInterlockFSM::getFakeAnalogIn(timespec *timespec, int32_t *value) {
	*value = 0;
}

//-----------------------------------------------------------------------------
// NDs state machine: when switching to ON
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::onSwitchOn() {

	m_AIGroup[0]->setState(nds::state_t::on);
	m_AIGroup[1]->setState(nds::state_t::on);
	m_DIGroup->setState(nds::state_t::on);
	m_PwrAvgGroup->setState(nds::state_t::on);
	m_PwrRefGroup->setState(nds::state_t::on);
	m_CavDecGroup->setState(nds::state_t::on);

	// Only start timers after initialization is complete
	scalarReadsTimer.restart();
	//fimStateMachineTimer.restart();
	interlockResetTimer.restart();
}

//-----------------------------------------------------------------------------
// NDs state machine: when switching to OFF
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::onSwitchOff() {

	m_AIGroup[0]->setState(nds::state_t::off);
	m_AIGroup[1]->setState(nds::state_t::off);
	m_DIGroup->setState(nds::state_t::off);
	m_PwrAvgGroup->setState(nds::state_t::off);
	m_PwrRefGroup->setState(nds::state_t::off);
	m_CavDecGroup->setState(nds::state_t::off);
}

//-----------------------------------------------------------------------------
// NDs state machine: when switching to RUNNING
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::onStart() {

	m_AIGroup[0]->setState(nds::state_t::running);
	m_AIGroup[1]->setState(nds::state_t::running);
	m_DIGroup->setState(nds::state_t::running);
	m_PwrRefGroup->setState(nds::state_t::running);
	m_PwrAvgGroup->setState(nds::state_t::running);
	m_CavDecGroup->setState(nds::state_t::running);

	// TODO - decide when to lock the PP configuration
	// ifcfastint_conf_lock(&m_deviceUser);
	// m_ppconf_locked = 1;
	// updatePPLock();

	m_ilckIntrThreadRunning = true;
	m_ilckIntrThread = m_node.runInThread("interlockThread", std::bind(&IFCFastIntInterlockFSM::ilckIntrThread, this));

	// Start interlock loop
	m_interlockThreadRunning = true;
	m_interlockThread = m_node.runInThread("interlockLoop", std::bind(&IFCFastIntInterlockFSM::interlockLoop, this));

	//Start interrupt monitoring thread
	m_acqIntrThreadRunning = true;
	m_acqIntrThread = m_node.runInThread("interrupThread", std::bind(&IFCFastIntInterlockFSM::acqIntrThread, this));

	// Start data acquisition (worker) thread
	m_waveAcquisitionThreadRunning = true;
	m_waveAcquisitionThread = m_node.runInThread("waveAcquisitionLoop",std::bind(&IFCFastIntInterlockFSM::waveAcquisitionLoop, this, m_nFrames));

}

//-----------------------------------------------------------------------------
// NDs state machine: when switching from RUNNING to STOP
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::onStop() {

	//Stop the interruption thread
	m_ilckIntrThreadRunning = false;
	m_ilckIntrThread.join();

	// STOP the interlock thread */
	m_interlockThreadRunning = false;
	m_interlockThread.join();

	//Stop the interruption thread
	m_acqIntrThreadRunning = false;
	m_acqIntrThread.join();

	/* STOP the waveform acquisition thread */
	m_waveAcquisitionThreadRunning = false;
	m_waveAcquisitionThread.join();

	// Stop channels
	m_AIGroup[0]->setState(nds::state_t::on);
	m_AIGroup[1]->setState(nds::state_t::on);
	m_DIGroup->setState(nds::state_t::on);
	m_PwrAvgGroup->setState(nds::state_t::on);
	m_PwrRefGroup->setState(nds::state_t::on);
	m_CavDecGroup->setState(nds::state_t::on);
}

//-----------------------------------------------------------------------------
// NDs state machine: helper functions
// TODO: how to use this properly?
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::recover() {
	throw nds::StateMachineRollBack("Cannot recover");
}
bool IFCFastIntInterlockFSM::allowChange(const nds::state_t currentLocal, const nds::state_t currentGlobal, const nds::state_t nextLocal) {
	return true;
}

//-----------------------------------------------------------------------------
// Auto-Reset feature
//-----------------------------------------------------------------------------

void IFCFastIntInterlockFSM::setAutoResetPeriod(const timespec &timespec, const double &value) {

	// Value must be > 50ms and multiple of it
	if (value >= FASTRESET_GRANULARITY) {
		double mult = std::floor(value/FASTRESET_GRANULARITY);
		m_AutoResetPeriod = FASTRESET_GRANULARITY * mult;
	} else {
		m_AutoResetPeriod = FASTRESET_GRANULARITY;
	}

	// reset the "quantum" counter
	m_AutoResetPeriodQuantum = m_AutoResetPeriod / FASTRESET_GRANULARITY;

	// Restart the timer
	interlockResetTimer.restart();

	struct timespec now = {0,0};
	double tmp;
	m_AutoResetPeriodPV.read(&now, &tmp);
	m_AutoResetPeriodPV.push(now, tmp);
}
void IFCFastIntInterlockFSM::getAutoResetPeriod(timespec *timespec, double *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = m_AutoResetPeriod;
}

//-----------------------------------------------------------------------------
// Set timestamp from hardware (EVR) and measure latency between updates
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::setHwTimestampStr(const timespec &timespec, const std::string& value) {
	struct timespec ts_rb, now;
	std::string val_rb;
	double updatePeriodms = 0.0;
	long nanos_offset = m_HwTimestampOffsetNs.getValue(); // offset adjustment for HW timestamp
	clock_gettime(CLOCK_REALTIME, &now);

	// Find the position of the decimal point on the timestamp string
    size_t dot_pos = value.find('.');

	// try to parse the timestamp string to obtain epoch seconds (POSIX) and nanoseconds
	{
		std::lock_guard<std::mutex> lg(updateMutex);
		try {
			// Extract the seconds
			m_HwTimestamp.tv_sec = static_cast<time_t>(std::stoll(value.substr(0, dot_pos)));
			// Extract the nanos and apply offset (defined by external PV)
			m_HwTimestamp.tv_nsec = static_cast<long>(std::stoll(value.substr(dot_pos + 1))) + nanos_offset;
			getHwTimestampStr(&ts_rb, &val_rb);
		} catch(const std::exception& e) {
			// fallback to current time if the string is empty
			clock_gettime(CLOCK_REALTIME, &m_HwTimestamp);
			getHwTimestampStr(&ts_rb, &val_rb);
		}
	}
	m_HwTimestampPV.push(ts_rb, val_rb);

	// estimate latency (for diagnostics)
	updatePeriodms = static_cast<double>(elapsed_time(m_HwTimestampUpdate, now)) / 1000.0;
	m_HwTimestampUpdate.tv_sec = now.tv_sec;
	m_HwTimestampUpdate.tv_nsec = now.tv_nsec;
	m_HwTimestampPeriodPV.push(now, updatePeriodms);
}
void IFCFastIntInterlockFSM::getHwTimestampStr(timespec *timespec, std::string *value) {
	timespec->tv_sec = m_HwTimestamp.tv_sec;
	timespec->tv_nsec = m_HwTimestamp.tv_nsec;
	std::ostringstream oss;
    oss << m_HwTimestamp.tv_sec << "." << std::setw(9) << std::setfill('0') << m_HwTimestamp.tv_nsec;
	*value = oss.str();
}


//-----------------------------------------------------------------------------
//  Timer that regularly updates FIM state machine
//-----------------------------------------------------------------------------
double IFCFastIntInterlockFSM::resetInterlockCounter(void) {

	// Subtract one "quantum" and test if it is done
	if (--m_AutoResetPeriodQuantum <= 0.0) {
		// Reset the interlock counter
		m_CurrentInterlockCount = 0;
		m_AutoResetPeriodQuantum = (m_AutoResetPeriod/FASTRESET_GRANULARITY);

		m_AutoResetIlckCountPV.setValue(0);
		struct timespec tsUpdate = {0,0};
		clock_gettime(CLOCK_REALTIME, &tsUpdate);
		m_AutoResetIlckCountPV.push(tsUpdate, 0);

#if 0
		struct timespec now = {0,0};
		clock_gettime(CLOCK_REALTIME, &now);
		ndsDebugStream(m_node) << now.tv_sec << "." << now.tv_nsec << "[AUTORESET] RESETING THE ILCK COUNTER | m_CurrentInterlockCount = " << m_CurrentInterlockCount << std::endl;
#endif
	}

	return DefaultInterval;
}


//-----------------------------------------------------------------------------
// Initialization/configuration of the FIM
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::initSystem() {

	ndsDebugStream(m_node) << "[FIMIOC] IFCFastIntInterlockFSM::initSystem() " << std::endl;

	ifcfastint_conf_unlock(&m_deviceUser);
	m_ppconf_locked = 0;

  	// DISABLE PRE-CONDITION OF NON -USED DIGITAL CHANNELS
	struct ifcfastint_digital_option option_digi;
	memset(&option_digi, 0, sizeof(struct ifcfastint_digital_option));
	option_digi.idle2pre = 0;
	option_digi.pre2run = 0;
	ifcfastint_set_conf_digital_pp(&m_deviceUser, 16, IFCFASTINT_DIGITAL_IDLE2PRE_W | IFCFASTINT_DIGITAL_PRE2RUN_W, &option_digi);
	ifcfastint_set_conf_digital_pp(&m_deviceUser, 17, IFCFASTINT_DIGITAL_IDLE2PRE_W | IFCFASTINT_DIGITAL_PRE2RUN_W, &option_digi);
	ifcfastint_set_conf_digital_pp(&m_deviceUser, 18, IFCFASTINT_DIGITAL_IDLE2PRE_W | IFCFASTINT_DIGITAL_PRE2RUN_W, &option_digi);
	ifcfastint_set_conf_digital_pp(&m_deviceUser, 19, IFCFASTINT_DIGITAL_IDLE2PRE_W | IFCFASTINT_DIGITAL_PRE2RUN_W, &option_digi);
	// ifcfastint_set_conf_digital_pp(&m_deviceUser, 20, IFCFASTINT_DIGITAL_IDLE2PRE_W | IFCFASTINT_DIGITAL_PRE2RUN_W, &option_digi); // LLRF INPUT
	ifcfastint_set_conf_digital_pp(&m_deviceUser, 21, IFCFASTINT_DIGITAL_IDLE2PRE_W | IFCFASTINT_DIGITAL_PRE2RUN_W, &option_digi);
	ifcfastint_set_conf_digital_pp(&m_deviceUser, 22, IFCFASTINT_DIGITAL_IDLE2PRE_W | IFCFASTINT_DIGITAL_PRE2RUN_W, &option_digi);
	ifcfastint_set_conf_digital_pp(&m_deviceUser, 23, IFCFASTINT_DIGITAL_IDLE2PRE_W | IFCFASTINT_DIGITAL_PRE2RUN_W, &option_digi);

    // WORK AROUND - Digital Inputs does not sense the input changes if we do not exercise the OPTION_1 register
	int digi_iter;
	for (digi_iter = 0; digi_iter < MAX_DI_CHANNELS; digi_iter++)
	{
		// Read inital parameters
		ifcfastint_get_conf_digital_pp(&m_deviceUser, digi_iter, &option_digi);

		/* Ignore this procedure if the mode is already modified */
		if (option_digi.mode != ifcfastint_dmode_zero) {
			continue;
		}

		//Force mode to ONE
		option_digi.mode = ifcfastint_dmode_one;
		ifcfastint_set_conf_digital_pp(&m_deviceUser, digi_iter, IFCFASTINT_DIGITAL_MODE_W, &option_digi);
		//Force mode back to ZERO
		option_digi.mode = ifcfastint_dmode_zero;
		ifcfastint_set_conf_digital_pp(&m_deviceUser, digi_iter, IFCFASTINT_DIGITAL_MODE_W, &option_digi);
	}

    // Disabling power average option (not in use at the moment)
	struct ifcfastint_pwravg_option option_pwr;
	memset(&option_pwr, 0, sizeof(struct ifcfastint_pwravg_option));
	option_pwr.idle2pre = 0;
	option_pwr.pre2run = 0;
	int avg_iter;
	for (avg_iter = 0; avg_iter < MAX_PWRAVG_CHANNELS; avg_iter++) {
		ifcfastint_set_conf_pwravg_pp(&m_deviceUser, avg_iter, IFCFASTINT_ANALOG_IDLE2PRE_W | IFCFASTINT_ANALOG_PRE2RUN_W, &option_pwr);
	}

  	//Work around - Exercise all registers 0x70 0x72 0x74 0x76 and DISABLE EMULATION mode
	struct ifcfastint_analog_option option_ana;
	memset(&option_ana, 0, sizeof(struct ifcfastint_analog_option));

	int ana_iter;
	for (ana_iter = 0; ana_iter < MAX_AI_CHANNELS; ana_iter++)
	{
		option_ana.idle2pre = 0;
		option_ana.pre2run = 0;

		//Read current parameters
		ifcfastint_get_conf_analog_pp(&m_deviceUser, ana_iter, ifcfastint_analog_pp_channel, &option_ana);
		option_ana.emulation_en = 0;

		//Write the same parameteres just to exercise registers
		ifcfastint_set_conf_analog_pp(&m_deviceUser, ana_iter, (IFCFASTINT_ANALOG_IDLE2PRE_W|IFCFASTINT_ANALOG_PRE2RUN_W|IFCFASTINT_ANALOG_EMULATION_EN_W),
			ifcfastint_analog_pp_channel, &option_ana);
	}

	for (digi_iter = 0; digi_iter < MAX_DI_CHANNELS; digi_iter++)
	{
		//Read current parameters
		ifcfastint_get_conf_digital_pp(&m_deviceUser, digi_iter, &option_digi);
		option_digi.emulation_en = 0;

		//Write the same parameteres just to exercise registers
		ifcfastint_set_conf_digital_pp(&m_deviceUser, digi_iter, (IFCFASTINT_DIGITAL_IDLE2PRE_W | IFCFASTINT_DIGITAL_PRE2RUN_W | IFCFASTINT_DIGITAL_EMULATION_EN_W),
			&option_digi);
	}

	for (int specialpp_iter = 0; specialpp_iter < MAX_SPECIALPP_CHANNELS; specialpp_iter++) {
		struct ifcfastint_specialpp_option option_specialpp;

		//Read current parameters
		ifcfastint_get_conf_refpwr(&m_deviceUser, specialpp_iter, &option_specialpp);
		option_specialpp.active = 1;

		//Write the same parameteres just to exercise registers AND to activate blocks AND enable autoreset
		ifcfastint_set_conf_refpwr(&m_deviceUser, specialpp_iter, IFCFASTINT_SPECIALPP_ACTIVE_W, &option_specialpp);
	}

  	/* SET FREQUENCY OF THE STATE MACHINE*/
	ifcfastint_set_fsm_frequency(&m_deviceUser, 1000);

  	/* Force HISTORY BUFFER ACQ STATE TO state 4 (ring buffer on during states IDLE PRE RUN with 1k POST MORTEM ) */
	ifcfastint_set_history_mode(&m_deviceUser, ifcfastint_histmode_4);

	// Read current state machine
	ifcfastint_get_fsm_state(&m_deviceUser, &m_FIM_StateMachine);

}

//-----------------------------------------------------------------------------
//  Get various parameters of the firmware from the FPGA
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::readFPGAinfo() {

	ifcdaqdrv_status status;

	// Get the timestamp
	struct timespec now = {0,0};
	clock_gettime(CLOCK_REALTIME, &now);

	//ndsDebugStream(m_node) << "[FIMIOC] Entering readFPGAinfo()"  << std::endl;

	// NSamples (nframes)
	m_nSamplesPV.setValue(m_nFrames);
	m_nSamplesPV.push(now, static_cast<int32_t>(m_nFrames));

	// FSM frequency
	uint32_t u32_reg_val;
	status = ifcfastint_get_fsm_frequency(&m_deviceUser, &u32_reg_val);
	if (status) {
		ndsErrorStream(m_node) << "[FIMIOC] ifcfastint_get_fsm_frequency failed" << std::endl;
	}
	m_fsmFrequencyPV.setValue(u32_reg_val);
	m_fsmFrequencyPV.push(now, static_cast<int32_t>(u32_reg_val));

	// History Buffer mode
	ifcfastint_histmode hist_mode;
	status = ifcfastint_get_history_mode(&m_deviceUser, &hist_mode);
	if (status) {
		ndsErrorStream(m_node) << "[FIMIOC] ifcfastint_get_history_mode failed" << std::endl;
	}
	m_HistModePV.setValue(hist_mode);
	m_HistModePV.push(now, static_cast<int32_t>(hist_mode));

	// History Buffer Enabled
	ifcfastint_histcontrol hist_status;
	status = ifcfastint_get_history_status(&m_deviceUser, &hist_status);
	if (status) {
		ndsErrorStream(m_node) << "[FIMIOC] ifcfastint_get_history_status failed" << std::endl;
	}
	m_HistEnabledPV.setValue(hist_status);
	m_HistEnabledPV.push(now, static_cast<int32_t>(hist_status));

	// History Buffer Acq. Status
	ifcfastint_hist_state state;
	status = ifcfastint_get_history_acqstate(&m_deviceUser, &state);
	if (status) {
		ndsErrorStream(m_node) << "[FIMIOC] ifcfastint_get_history_acqstate failed" << std::endl;
	}
	m_HistoryStatePV.setValue(state);
	m_HistoryStatePV.push(now, static_cast<int32_t>(state));

	// Hist. Buffer Overflow
	int32_t histoverff, ringoverff;
	status = ifcfastint_get_history_flags(&m_deviceUser, &histoverff, &ringoverff);
	if (status) {
		ndsErrorStream(m_node) << "[FIMIOC] ifcfastint_get_history_flags failed" << std::endl;
	}
	m_HistOverFFPV.setValue(histoverff);
	m_HistRingOverFFPV.setValue(ringoverff);
	m_HistOverFFPV.push(now, static_cast<int32_t>(histoverff));
	m_HistRingOverFFPV.push(now, static_cast<int32_t>(ringoverff));

	// Raw Status Register
	int32_t regval;
	status = ifcfastint_get_statusreg(&m_deviceUser, &regval);
	if (status) {
		ndsErrorStream(m_node) << "[FIMIOC] ifcfastint_get_statusreg failed" << std::endl;
	}
	m_StatusRegPV.setValue(regval);
	m_StatusRegPV.push(now, regval);

	// Read digital outputs state and raw analog inputs (verification)
	uint32_t reg32 = 0;
	int16_t ai0, ai19;
	ifcfastint_get_outputs_state(&m_deviceUser, &reg32);
	ifcfastint_get_raw_ai(&m_deviceUser, &ai0, &ai19);

	m_OutsMSBPV.setValue(static_cast<int32_t>((reg32>>16) & 0x7FFF));
	m_OutsMSBPV.push(now, static_cast<int32_t>((reg32>>16) & 0x7FFF));

	m_OutsLSBPV.setValue(static_cast<int32_t>(reg32 & 0xFFFF));
	m_OutsLSBPV.push(now, static_cast<int32_t>(reg32 & 0xFFFF));

	m_RawAI0PV.setValue(static_cast<int32_t>(ai0));
	m_RawAI0PV.push(now,  static_cast<int32_t>(ai0));

	m_RawAI19PV.setValue(static_cast<int32_t>(ai19));
	m_RawAI19PV.push(now, static_cast<int32_t>(ai19));

	// Backplane trigger line
	uint32_t bkpLine;
	status = ifcfastint_get_timingmask(&m_deviceUser, &bkpLine);
	FIMIOC_STATUS_CHECK("ifcfastint_get_timingmask", status);

	m_bkpTriggerLine = static_cast<int32_t>(bkpLine);
	m_bkpTriggerLinePV.setValue(static_cast<int32_t>(bkpLine));
	m_bkpTriggerLinePV.push(now, static_cast<int32_t>(bkpLine));

	// Thread queue
	int32_t tmpvalue;
	m_QueueStatusPV.read(&now, &tmpvalue);
	m_QueueStatusPV.push(now, tmpvalue);

	// Global auto-reset
	int32_t autoreset;
	m_MainAutoResetPV.read(&now, &autoreset);
	m_MainAutoResetPV.push(now, autoreset);

	// generic purpose DO
	int32_t gpdo;
	m_gpdoPV.read(&now, &gpdo);
	m_gpdoPV.push(now, gpdo);

}

//-----------------------------------------------------------------------------
//  Acquisition Interrupt Thread
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::acqIntrThread() {
	ifcdaqdrv_status irqstatus;
	fimEventMsg msg;
	ndsDebugStream(m_node) << "[FIMIOC] Starting acquisition interrupt thread " << std::endl;

	/* Subscribe to interrupt - mandatory */
	while (m_acqIntrThreadRunning) {
		ifcdaqdrv_subs_intr(&m_deviceUser, 0x01);
		irqstatus = ifcdaqdrv_wait_intr(&m_deviceUser, 0x01);

		{ //mutex
			std::lock_guard<std::mutex> lg(updateMutex);

			// try to detect if an interlock happened
			ifcfastint_hist_state histbuf_state;
			ifcfastint_get_history_acqstate(&m_deviceUser, &histbuf_state);

			if ((histbuf_state == ifcfastint_history_ended) or (histbuf_state == ifcfastint_history_postmortem)) {
				msg.evtType = MSG_INTERLOCK;
			} else {

				// no interlock, normal acquisition
				if (irqstatus == status_success) {
					ifcfastint_read_wr_pointer(&m_deviceUser, IFCFASTINT_TRIG_WRPOINTER, &msg.readPointer);
					msg.evtType = MSG_ACQUISITION; // normal acquisition
				} else {
					ifcfastint_read_wr_pointer(&m_deviceUser, IFCFASTINT_RAND_WRPOINTER, &msg.readPointer);
					msg.evtType = MSG_TIMEOUT; // timeout
				}
			}

		}
		// Drop packet if queue is filled
		if (acqEvent.pending() <= 1) {
			acqEvent.trySend((void*) &msg, sizeof(msg));
		}
		m_QueueStatusPV.setValue(static_cast<int32_t>(acqEvent.pending()));

	}
	ndsDebugStream(m_node) << "[FIMIOC] Exiting acquisition interrupt thread " << std::endl;
}

//-----------------------------------------------------------------------------
//  Interlock Interrupt Thread
//  NOTE: detect interlock, read FPGA buffer, reset state machine
//		  and send data to message queue takes ~ 4600 microseconds
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::ilckIntrThread() {
	ndsDebugStream(m_node) << "[FIMIOC] Starting INTERLOCK ISR thread " << std::endl;

	ifcdaqdrv_status irqstatus;
	int8_t *pdata = (int8_t *)calloc((HISTORY_FRAME_SIZE * m_nFrames)+1,sizeof(int8_t));

	// thread loop
	while (m_ilckIntrThreadRunning) {
		ifcdaqdrv_subs_intr(&m_deviceUser, 0x02);
		irqstatus = ifcdaqdrv_wait_intr(&m_deviceUser, 0x02);

		if (irqstatus == status_success) {
			int32_t rdpointer;
			{ // mutex area
				std::lock_guard<std::mutex> lg(updateMutex);
				ifcfastint_wait_abort_done(&m_deviceUser); // Just to enforce that history buffer is off
				ifcfastint_read_wr_pointer(&m_deviceUser, IFCFASTINT_RAND_WRPOINTER, &rdpointer);

				// Read history and save it in message buffer (first byte is for message type)
				size_t nelm;
				ifcfastint_read_history2(&m_deviceUser, m_nFrames, (void*)&pdata[1], &nelm, rdpointer);
				ifcfastint_history_reset(&m_deviceUser);
			} //mutex area

			// send message with fixed length
			pdata[0] = MSG_INTERLOCK;
			ilckEvent.trySend((void*) pdata, (m_nFrames * HISTORY_FRAME_SIZE)+1);
		}
	}

	// Thread exit, send message to unblock interlock routine thread
	pdata[0] = MSG_TERMINATE;
	ilckEvent.trySend((void*) pdata, (m_nFrames * HISTORY_FRAME_SIZE)+1);
	free(pdata);
	ndsDebugStream(m_node) << "[FIMIOC] Exiting INTERLOCK ISR thread " << std::endl;
}


//-----------------------------------------------------------------------------
//  Interlock data treatment
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::interlockLoop() {
	ndsDebugStream(m_node) << "[FIMIOC] Starting INTERLOCK thread " << std::endl;

	bool autoResetDetected = false;
	int32_t autoResetChannelNum = 0;
	fim_inputtype_t autoResetIlckType = fim_invalid;

	struct timespec tsIntrStart = {0,0};
	struct timespec tsIntrEnd = {0,0};

	struct timespec tsDataColStart = {0,0};
	struct timespec tsDataColEnd = {0,0};

	struct timespec tsDetectStart = {0,0};
	struct timespec tsDetectEnd = {0,0};


	int8_t rawMeasData[1024];
	memset(rawMeasData, 0, 1024);
	int8_t *pdatamsg = (int8_t *)calloc((HISTORY_FRAME_SIZE * m_nFrames)+1,sizeof(int8_t));

	int interlock_count = 0;
	std::vector<int32_t> fsmTrend;

	ifcdaqdrv_status status;

	// Create object to parse history buffer frames
	HistBufferParser frameParser(nullptr);

	while(m_interlockThreadRunning) {
		// Wait for event from the message queue
		ilckEvent.receive((void*)pdatamsg, (m_nFrames * HISTORY_FRAME_SIZE)+1);
		int8_t *pdata = &pdatamsg[1];

		if (pdatamsg[0] == MSG_TERMINATE) break;

		{ // MUTEX area
			std::lock_guard<std::mutex> lg(updateMutex);

			++interlock_count;
			autoResetDetected = false;

			// Flag that causes normal acq. loop to skip one message
			m_skipNormalRead = true;

			//ndsInfoStream(m_node) << "[FIMIOC] Interlock detected! (interlock count = " << interlock_count << ")" << std::endl;
			clock_gettime(CLOCK_REALTIME, &tsIntrStart);
			clock_gettime(CLOCK_REALTIME, &tsDataColStart);

			/* Read state of the FSM (ABO - IDLE - PRE - RUN) */
			ifcfastint_fsm_state state;
			status = ifcfastint_get_fsm_state(&m_deviceUser, &state);
			if (status) {
				ndsErrorStream(m_node) << "Failed to read state" << std::endl;
			}
			m_intFSMPV.push(tsIntrStart, static_cast<int32_t>(state));

			/* Get pointer to the individual channel objects and allocate memory for history
			* channel->reserve(N) will resize std::vector<double> m_history to N;
			* */
			for (auto channel : m_AIGroup[0]->getChannels()) {
				channel->reserve(m_nFrames);
			}
			for (auto channel : m_AIGroup[1]->getChannels()) {
				channel->reserve(m_nFrames);
			}
			for (auto channel : m_DIGroup->getChannels()) {
				channel->reserve(m_nFrames);
			}
			for (auto channel: m_PwrRefGroup->getChannels()) {
				channel->reserve(m_nFrames);
			}
			for (auto channel: m_CavDecGroup->getChannels()) {
				channel->reserve(m_nFrames);
			}

			status = ifcfastint_read_measurements(&m_deviceUser, (void*) rawMeasData);
			if (status) {
				ndsErrorStream(m_node) << "Failed to read real-time measurements!" << std::endl;
			}

			/*  ITERATE OVER EACH HISTORY FRAME (64 bytes) */
			for (int8_t *current_frame(pdata); current_frame < pdata + (m_nFrames * HISTORY_FRAME_SIZE); current_frame += HISTORY_FRAME_SIZE)	{

				frameParser.updateFrame(current_frame);
				fsmTrend.push_back(frameParser.getCurrentState());

				// Parse information from analog channels (0 to 19) and digital channels (0 to 9)
				for (size_t ch_iter = 0; ch_iter < (MAX_AI_CHANNELS/2); ch_iter++)
				{
					m_AIGroup[0]->m_AIChannels[ch_iter]->historyAddPPQValue(frameParser.getAnalogInputQOUT(ch_iter));
					m_AIGroup[0]->m_AIChannels[ch_iter]->historyAddADCValue(
						frameParser.getAnalogInput(ch_iter),
						frameParser.getAnalogInput(ch_iter));

					m_AIGroup[1]->m_AIChannels[ch_iter]->historyAddPPQValue(frameParser.getAnalogInputQOUT(ch_iter+10));
					m_AIGroup[1]->m_AIChannels[ch_iter]->historyAddADCValue(
						frameParser.getAnalogInput(ch_iter+10),
						frameParser.getAnalogInput(ch_iter+10));

					m_DIGroup->m_DIChannels[ch_iter]->historyAddPPQValue(frameParser.getDigitalInputQOUT(ch_iter));
					m_DIGroup->m_DIChannels[ch_iter]->historyAddValue(frameParser.getDigitalInput(ch_iter));
				}

				// Parse information from digital channels 10 to 20
				for (size_t ch_iter = (MAX_AI_CHANNELS/2); ch_iter < MAX_DI_CHANNELS; ch_iter++)
				{
					m_DIGroup->m_DIChannels[ch_iter]->historyAddPPQValue(frameParser.getDigitalInputQOUT(ch_iter));
					m_DIGroup->m_DIChannels[ch_iter]->historyAddValue(frameParser.getDigitalInput(ch_iter));
				}

				// Parse information from REFLECTED POWER blocks
				for (size_t ch_iter = 0; ch_iter < 2; ch_iter++) {
					uint8_t refpwrdiag[4];
					refpwrdiag[0] = frameParser.getRefPwrOK(ch_iter);
					refpwrdiag[1] = frameParser.getRefPwrWinMask(ch_iter);
					refpwrdiag[2] = frameParser.getRefPwrWinMeas(ch_iter);
					refpwrdiag[3] = frameParser.getRefPwrTrig(ch_iter);

					m_PwrRefGroup->m_RefPwrChannels[ch_iter]->historyAddValues(refpwrdiag[0], refpwrdiag[1], refpwrdiag[2], refpwrdiag[3], 0);
				}

				// Parse information from CAVITY DECAY blocks
				for (size_t ch_iter = 0; ch_iter < 2; ch_iter++) {
					uint8_t cavdecdiag[5];
					cavdecdiag[0] = frameParser.getCavDecOK(ch_iter);
					cavdecdiag[1] = frameParser.getCavDecWinMask(ch_iter);
					cavdecdiag[2] = frameParser.getCavDecWinMeas(ch_iter);
					cavdecdiag[3] = frameParser.getCavDecStartTrig(ch_iter);
					cavdecdiag[4] = frameParser.getCavDecEndTrig(ch_iter);

					m_CavDecGroup->m_RefPwrChannels[ch_iter]->historyAddValues(cavdecdiag[0], cavdecdiag[1], cavdecdiag[2], cavdecdiag[3], cavdecdiag[4]);
				}

			} // end of foor loop that iterates over each frame


			/* Collect real-time (READ_OUT) parameters for each analog input block */
			for (auto channel : m_AIGroup[0]->getChannels()) {
				/* Iterate over each block to update parameters */
				for (auto ppblock : channel->getPPBlocks()) {
					ppblock->refreshRTStatus((uint8_t *) rawMeasData); // decode real-time measurements
					ppblock->updateProcOut();
					ppblock->updateTrigVal();
					if (ppblock->m_ppType == ifcfastint_analog_pp_devmon) {
						ppblock->updateDevMonMax();
						ppblock->updateDevMonMin();
					} else {
						ppblock->updatePresVal();
					}
				}
			}
			for (auto channel : m_AIGroup[1]->getChannels()) {
				/* Iterate over each block to update parameters */
				for (auto ppblock : channel->getPPBlocks()) {
					ppblock->refreshRTStatus((uint8_t *) rawMeasData); // decode real-time measurements
					ppblock->updateProcOut();
					ppblock->updateTrigVal();
					if (ppblock->m_ppType == ifcfastint_analog_pp_devmon) {
						ppblock->updateDevMonMax();
						ppblock->updateDevMonMin();
					} else {
						ppblock->updatePresVal();
					}
				}
			}

			m_fsmHistoryBufferPV.push(tsIntrStart, fsmTrend);
			fsmTrend.clear();

			clock_gettime(CLOCK_REALTIME, &tsDataColEnd);

			// Container for async tasks
			std::vector<std::future<bool> > controlSystemDetectIlckChannel;
			std::vector<std::future<bool> > controlSystemUpdatePostMortem;

			clock_gettime(CLOCK_REALTIME, &tsDetectStart);

			// Detect interlock tasks
			controlSystemDetectIlckChannel.push_back(std::async(std::launch::async, [this, state, &autoResetDetected, &autoResetChannelNum, &autoResetIlckType]() {
				for (auto channel : this->m_AIGroup[0]->getChannels()) {
					int32_t qout = (int32_t) channel->detectInterlock(state);
					channel->setPPOutput(qout);

					// DETECT IF AUTORESET SHOULD BE APPLIED
					if ((qout == 1) and (channel->m_InterlockInfo->isAutoReset())) {
						autoResetDetected = true;
						autoResetChannelNum = channel->m_channelNum;
						autoResetIlckType = fim_analog_in;
						ndsDebugStream(m_node) << "[AUTORESET] Interlock on channel " << channel->m_channelNum << std::endl;
					}
				}
				return true;
			}));
			controlSystemDetectIlckChannel.push_back(std::async(std::launch::async, [this, state, &autoResetDetected, &autoResetChannelNum, &autoResetIlckType]() {
				for (auto channel : this->m_AIGroup[1]->getChannels()) {
					int32_t qout = (int32_t) channel->detectInterlock(state);
					channel->setPPOutput(qout);

					// DETECT IF AUTORESET SHOULD BE APPLIED
					if ((qout == 1) and (channel->m_InterlockInfo->isAutoReset())) {
						autoResetDetected = true;
						autoResetChannelNum = channel->m_channelNum;
						autoResetIlckType = fim_analog_in;
						ndsDebugStream(m_node) << "[AUTORESET] Interlock on channel " << channel->m_channelNum << std::endl;
					}

				}
				return true;
			}));
			controlSystemDetectIlckChannel.push_back(std::async(std::launch::async, [this, &autoResetDetected, &autoResetChannelNum, &autoResetIlckType]() {
				for (auto channel : this->m_DIGroup->getChannels()) {
					int32_t qout = (int32_t) channel->detectInterlock();
					channel->setPPOutput(qout);

					// DETECT IF AUTORESET SHOULD BE APPLIED
					if ((qout == 1) and (channel->m_InterlockInfo->isAutoReset())) {
						autoResetDetected = true;
						autoResetChannelNum = channel->m_channelNum;
						autoResetIlckType = fim_digital_in;
						ndsDebugStream(m_node) << "[AUTORESET] Interlock on channel " << channel->m_channelNum << std::endl;
					}
				}
				return true;
			}));

			controlSystemDetectIlckChannel.push_back(std::async(std::launch::async, [this, state, &autoResetDetected, &autoResetChannelNum, &autoResetIlckType]() {
				for (auto channel : this->m_PwrRefGroup->getChannels()) {
					int32_t qout = (int32_t) channel->detectInterlock(state);
					channel->setFirstIlck(qout);

					// DETECT IF AUTORESET SHOULD BE APPLIED
					if ((qout == 1) and (channel->m_InterlockInfo->isAutoReset())) {
						autoResetDetected = true;
						autoResetChannelNum = channel->m_block;
						autoResetIlckType = fim_special_rp_in;
						ndsDebugStream(m_node) << "[AUTORESET] Interlock on channel " << channel->m_block << std::endl;
					}
				}
				return true;
			}));
			controlSystemDetectIlckChannel.push_back(std::async(std::launch::async, [this, state, &autoResetDetected, &autoResetChannelNum, &autoResetIlckType]() {
				for (auto channel : this->m_CavDecGroup->getChannels()) {
					int32_t qout = (int32_t) channel->detectInterlock(state);
					channel->setFirstIlck(qout);

					// DETECT IF AUTORESET SHOULD BE APPLIED
					if ((qout == 1) and (channel->m_InterlockInfo->isAutoReset())) {
						autoResetDetected = true;
						autoResetChannelNum = channel->m_block;
						autoResetIlckType = fim_special_cd_in;
						ndsDebugStream(m_node) << "[AUTORESET] Interlock on channel " << channel->m_block << std::endl;
					}
				}
				return true;
			}));

			// Wait for async tasks completion
			for (auto& task : controlSystemDetectIlckChannel) {
				task.get();
			}

			// check if HW timestamp is valid, if not, fallback to current time
			if  (m_HwTimestampOK.getValue() == 0) {
				clock_gettime(CLOCK_REALTIME, &m_HwTimestamp);
			}

			// Update waveforms for post mortem
			controlSystemUpdatePostMortem.push_back(std::async(std::launch::async, [this]() {
				for (auto channel : this->m_AIGroup[0]->getChannels()) {
					channel->historyReset(true, MSG_INTERLOCK, m_HwTimestamp);
				}
				return true;
			}));

			controlSystemUpdatePostMortem.push_back(std::async(std::launch::async, [this]() {
				for (auto channel : this->m_AIGroup[1]->getChannels()) {
					channel->historyReset(true, MSG_INTERLOCK, m_HwTimestamp);
				}
				return true;
			}));

			controlSystemUpdatePostMortem.push_back(std::async(std::launch::async, [this]() {
				for (auto channel : this->m_DIGroup->getChannels()) {
					channel->historyReset(true);
				}
				return true;
			}));

			controlSystemUpdatePostMortem.push_back(std::async(std::launch::async, [this]() {
				for (auto channel : this->m_PwrRefGroup->getChannels()) {
					channel->historyReset(true);
				}
				for (auto channel : this->m_CavDecGroup->getChannels()) {
					channel->historyReset(true);
				}
				return true;
			}));

			// Wait for async tasks completion
			for (auto& task : controlSystemUpdatePostMortem) {
				task.get();
			}

			clock_gettime(CLOCK_REALTIME, &tsDetectEnd);

			ifcfastint_fsm_state fsmstate_test;
			ifcfastint_get_fsm_state(&m_deviceUser, &fsmstate_test);

			// Fast recovery (interlock reset) routine
			if (autoResetDetected and (fsmstate_test == ifcfastint_fsm_state_hvon)) {

				if (m_CurrentInterlockCount == 0) {
					// reset the "quantum" counter
					m_AutoResetPeriodQuantum = m_AutoResetPeriod / FASTRESET_GRANULARITY;
					// Restart the timer
					interlockResetTimer.restart();
				}

				// increase special interlock counter
				++m_CurrentInterlockCount;
				m_AutoResetIlckCountPV.setValue(m_CurrentInterlockCount);


				// try to move state machine back to RFON!
				if (m_CurrentInterlockCount < m_AutoResetMaxIlckPV.getValue()) {

					// If the interlock is from ANALOG CHANNEL, try to manually reset
					if (autoResetIlckType == fim_analog_in) {
						//for (auto i = 0; i < 5; i++) {
							ifcfastint_reset_analog_ilck(&m_deviceUser, autoResetChannelNum);

							// std::this_thread::sleep_for(std::chrono::microseconds(500));
							ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_rfon);
							ifcfastint_get_fsm_state(&m_deviceUser, &fsmstate_test);

							if (fsmstate_test != ifcfastint_fsm_state_rfon) {
								ndsErrorStream(m_node) << "[AUTORESET] Failed to move FSM to RFON! - (ANALOG CHANNEL) " << std::endl;
							} //else {
								//break;
							//}
						//}

					} else {
						// If the interlock is from DIGITAL or SPECIAL CHANNEL, the QOUT signal is non-latched,
						// so it should be already OK
						ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_rfon);
						ifcfastint_get_fsm_state(&m_deviceUser, &fsmstate_test);

						if (fsmstate_test != ifcfastint_fsm_state_rfon) {

							//Try once to reset the state machine manually
							ifcfastint_reset_special_ilck(&m_deviceUser, IFCFASTINT_RESET_FAST);

							ifcfastint_set_fsm_state(&m_deviceUser, ifcfastint_fsm_state_rfon);
							ifcfastint_get_fsm_state(&m_deviceUser, &fsmstate_test);

							if (fsmstate_test != ifcfastint_fsm_state_rfon) {
								ndsErrorStream(m_node) << "[AUTORESET] Failed to move FSM to RFON! (not special ilck)" << std::endl;
							}
						}
					}

				} else {
					ndsErrorStream(m_node) << "[AUTORESET] Reached max. allowed interlocks. Going to HVON" << std::endl;
				}

				// compute elapsed time
				clock_gettime(CLOCK_REALTIME, &tsIntrEnd);


				m_AutoResetIlckCountPV.push(tsIntrStart, m_CurrentInterlockCount);

				// fimStateMachineTimer.wakeUp();
				m_intFSMPV.push(tsIntrStart, static_cast<int32_t>(fsmstate_test));

				double recovertime = elapsed_time(tsIntrStart, tsIntrEnd) / 1000;
				m_AutoResetDurationPV.setValue(recovertime);
				m_AutoResetDurationPV.push(tsIntrEnd, recovertime);
#if 0
				double dctime = elapsed_time(tsDataColStart, tsDataColEnd) / 1000;
				double dcdetc = elapsed_time(tsDetectStart, tsDetectEnd) / 1000;
				double dctest = elapsed_time(tsTestStart, tsTestEnd) / 1000;
#endif
			}

			/* Update precondition PVs */
			bool HVONprecond = true;
			bool RFONprecond = true;
			for (auto channel : this->m_AIGroup[0]->getChannels()) {
				HVONprecond &= channel->getHVONprecond();
				RFONprecond &= channel->getRFONprecond();
			}
			for (auto channel : this->m_AIGroup[1]->getChannels()) {
				HVONprecond &= channel->getHVONprecond();
				RFONprecond &= channel->getRFONprecond();
			}
			for (auto channel : this->m_DIGroup->getChannels()) {
				HVONprecond &= channel->getHVONprecond();
				RFONprecond &= channel->getRFONprecond();
			}
			for (auto channel : this->m_PwrRefGroup->getChannels()) {
				HVONprecond &= channel->getHVONprecond();
				RFONprecond &= channel->getRFONprecond();
			}
			for (auto channel : this->m_CavDecGroup->getChannels()) {
				HVONprecond &= channel->getHVONprecond();
				RFONprecond &= channel->getRFONprecond();
			}
			RFONprecond &= HVONprecond;

			m_HVONPreCondPV.push(tsIntrStart, static_cast<int32_t>(HVONprecond));
			m_RFONPreCondPV.push(tsIntrStart, static_cast<int32_t>(RFONprecond));

		} //end of mutex area

	}

	free(pdatamsg);
	ndsDebugStream(m_node) << "[FIMIOC] Exiting INTERLOCK thread " << std::endl;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//  WORKER THREAD
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void IFCFastIntInterlockFSM::waveAcquisitionLoop(int32_t nframes)
{
	ndsDebugStream(m_node) << "Worker thread started" << std::endl;

	/* General purpose status variables */
	ifcdaqdrv_status status;
	ifcfastint_fsm_state state;
	int timeout_count;

  	/* Process measurement variable */
	struct timespec tsLoopStart = {0,0};
	struct timespec tsLoopEnd = {0,0};
	struct timespec tsIntrStart = {0,0};
	struct timespec tsIntrEnd = {0,0};

	/* Reserve all memory accordingly to the number of frames  */
	m_rawData = (int8_t *)realloc(m_rawData, HISTORY_FRAME_SIZE * nframes);

	/* Read state of the FSM (ABO - IDLE - PRE - RUN) */
	status = ifcfastint_get_fsm_state(&m_deviceUser, &state);
	if (status) {
		ndsErrorStream(m_node) << "Failed to read state" << std::endl;
	}

	// Create object to parse history buffer frames
	HistBufferParser frameParser(nullptr);

	clock_gettime(CLOCK_REALTIME, &tsIntrStart);
	timeout_count = 0;

	/* Loop until the user stops, or state is ABORT */
	while (m_waveAcquisitionThreadRunning)
	{
		size_t nFrames;

		// Wait for event from the message queue
		fimEventMsg msg;
		acqEvent.receive((void*)&msg, sizeof(msg));

		// if acquisition driven by interrupt, run normally
		if (msg.evtType == MSG_ACQUISITION) {
			timeout_count = 0;
		} else if (msg.evtType == MSG_TIMEOUT) {
			// if acquisition driven by timeout, start counting and skip 3 times
			// (this allows 1 Hz acquisitions)
			//++timeout_count;
			if (++timeout_count > 3) {
				timeout_count = 0;
			}
		} else {
			// Drop message when not valid (interlock)
			// (this is useful for the fast recovery function)
			timeout_count = 0;
			continue;
		}

		if (m_skipNormalRead) {
			m_skipNormalRead = false;
			continue;
		}

		// MUTEX AREA
		{
			std::lock_guard<std::mutex> lg(updateMutex);

			clock_gettime(CLOCK_REALTIME, &tsIntrEnd);
			m_IntrFreq = static_cast<double>(elapsed_time(tsIntrStart, tsIntrEnd)) / 1000.0;
			clock_gettime(CLOCK_REALTIME, &tsIntrStart);
			clock_gettime(CLOCK_REALTIME, &tsLoopStart);

			/* Update FIM state machine status */
			int32_t rb_value;
			m_intFSMPV.read(&tsLoopStart, &rb_value);
			m_intFSMPV.push(tsLoopStart, rb_value);

			/* Get pointer to the individual channel objects and allocate memory for history
			* channel->reserve(N) will resize std::vector<double> m_history to N;
			* */
			for (auto channel : m_AIGroup[0]->getChannels()) {
				channel->reserve(nframes);
			}
			for (auto channel : m_AIGroup[1]->getChannels()) {
				channel->reserve(nframes);
			}
			for (auto channel : m_DIGroup->getChannels()) {
				channel->reserve(nframes);
			}
			for (auto channel: m_PwrRefGroup->getChannels()) {
				channel->reserve(nframes);
			}
			for (auto channel: m_CavDecGroup->getChannels()) {
				channel->reserve(nframes);
			}

			/* TODO: prepare PWRAVG modules */
			status = ifcfastint_read_history2(&m_deviceUser, nframes, m_rawData, &nFrames, msg.readPointer);
			if (status) {
				ndsErrorStream(m_node) << "Failed to read history" << std::endl;
				break;
			}

			// Read register area with real-time measurements of the analog input blocks
			status = ifcfastint_read_measurements(&m_deviceUser, m_rawMeasData);
			if (status) {
				ndsErrorStream(m_node) << "Failed to read history after 5 tries!" << std::endl;
				//break;
			}

			// Force nFrames to be equal to nframes
			nFrames = nframes;

			/*  ITERATE OVER EACH HISTORY FRAME (64 bytes) */
			for (int8_t *current_frame(m_rawData); current_frame < m_rawData + (nFrames * HISTORY_FRAME_SIZE); current_frame += HISTORY_FRAME_SIZE)	{

				frameParser.updateFrame(current_frame);

				// Parse information from analog channels (0 to 19) and digital channels (0 to 9)
				for (size_t ch_iter = 0; ch_iter < (MAX_AI_CHANNELS/2); ch_iter++)
				{
					m_AIGroup[0]->m_AIChannels[ch_iter]->historyAddPPQValue(frameParser.getAnalogInputQOUT(ch_iter));
					m_AIGroup[0]->m_AIChannels[ch_iter]->historyAddADCValue(
						frameParser.getAnalogInput(ch_iter),
						frameParser.getAnalogInput(ch_iter));

					m_AIGroup[1]->m_AIChannels[ch_iter]->historyAddPPQValue(frameParser.getAnalogInputQOUT(ch_iter+10));
					m_AIGroup[1]->m_AIChannels[ch_iter]->historyAddADCValue(
						frameParser.getAnalogInput(ch_iter+10),
						frameParser.getAnalogInput(ch_iter+10));

					m_DIGroup->m_DIChannels[ch_iter]->historyAddPPQValue(frameParser.getDigitalInputQOUT(ch_iter));
					m_DIGroup->m_DIChannels[ch_iter]->historyAddValue(frameParser.getDigitalInput(ch_iter));
				}

				// Parse information from digital channels 10 to 20
				for (size_t ch_iter = (MAX_AI_CHANNELS/2); ch_iter < MAX_DI_CHANNELS; ch_iter++)
				{
					m_DIGroup->m_DIChannels[ch_iter]->historyAddPPQValue(frameParser.getDigitalInputQOUT(ch_iter));
					m_DIGroup->m_DIChannels[ch_iter]->historyAddValue(frameParser.getDigitalInput(ch_iter));
				}

				// Parse information from REFLECTED POWER blocks
				for (size_t ch_iter = 0; ch_iter < 2; ch_iter++) {
					uint8_t refpwrdiag[4];
					refpwrdiag[0] = frameParser.getRefPwrOK(ch_iter);
					refpwrdiag[1] = frameParser.getRefPwrWinMask(ch_iter);
					refpwrdiag[2] = frameParser.getRefPwrWinMeas(ch_iter);
					refpwrdiag[3] = frameParser.getRefPwrTrig(ch_iter);

					m_PwrRefGroup->m_RefPwrChannels[ch_iter]->historyAddValues(refpwrdiag[0], refpwrdiag[1], refpwrdiag[2], refpwrdiag[3], 0);
				}

				// Parse information from CAVITY DECAY blocks
				for (size_t ch_iter = 0; ch_iter < 2; ch_iter++) {
					uint8_t cavdecdiag[5];
					cavdecdiag[0] = frameParser.getCavDecOK(ch_iter);
					cavdecdiag[1] = frameParser.getCavDecWinMask(ch_iter);
					cavdecdiag[2] = frameParser.getCavDecWinMeas(ch_iter);
					cavdecdiag[3] = frameParser.getCavDecStartTrig(ch_iter);
					cavdecdiag[4] = frameParser.getCavDecEndTrig(ch_iter);

					m_CavDecGroup->m_RefPwrChannels[ch_iter]->historyAddValues(cavdecdiag[0], cavdecdiag[1], cavdecdiag[2], cavdecdiag[3], cavdecdiag[4]);
				}

			} // end of foor loop that iterates over each frame

			/* Collect real-time (READ_OUT) parameters for each analog input block */
			for (auto channel : m_AIGroup[0]->getChannels()) {
				/* Iterate over each block to update parameters */
				for (auto ppblock : channel->getPPBlocks()) {
					ppblock->refreshRTStatus((uint8_t *) m_rawMeasData); // reads new register space
					ppblock->updateProcOut();
					ppblock->updateTrigVal();
					if (ppblock->m_ppType == ifcfastint_analog_pp_devmon) {
						ppblock->updateDevMonMax();
						ppblock->updateDevMonMin();
					} else {
						ppblock->updatePresVal();
					}
				}
			}
			for (auto channel : m_AIGroup[1]->getChannels()) {
				/* Iterate over each block to update parameters */
				for (auto ppblock : channel->getPPBlocks()) {
					ppblock->refreshRTStatus((uint8_t *) m_rawMeasData); // reads new register space
					ppblock->updateProcOut();
					ppblock->updateTrigVal();
					if (ppblock->m_ppType == ifcfastint_analog_pp_devmon) {
						ppblock->updateDevMonMax();
						ppblock->updateDevMonMin();
					} else {
						ppblock->updatePresVal();
					}
				}
			}

			// skip waveform update when the interruption was cause by a timeout
			// this allows data acquisition at 1 Hz (without reading just noise)
			if (!timeout_count ) {
				// Launch async task to push waveforms to EPICS
				std::vector<std::future<bool> > controlSystemUpdate;

				// check if HW timestamp is valid, if not, fallback to current time
				if  (m_HwTimestampOK.getValue() == 0) {
					clock_gettime(CLOCK_REALTIME, &m_HwTimestamp);
				}

				controlSystemUpdate.push_back(std::async(std::launch::async, [this, msg]() {
					for (auto channel : this->m_AIGroup[0]->getChannels()) {
						channel->historyReset(false, msg.evtType, m_HwTimestamp);
					}
					return true;
				}));

				controlSystemUpdate.push_back(std::async(std::launch::async, [this, msg]() {
					for (auto channel : this->m_AIGroup[1]->getChannels()) {
						channel->historyReset(false, msg.evtType, m_HwTimestamp);
					}
					return true;
				}));

				controlSystemUpdate.push_back(std::async(std::launch::async, [this]() {
					for (auto channel : this->m_DIGroup->getChannels()) {
						channel->historyReset(false);
					}
					return true;
				}));

				controlSystemUpdate.push_back(std::async(std::launch::async, [this]() {
					for (auto channel : this->m_PwrRefGroup->getChannels()) {
						channel->historyReset(false);
					}
					for (auto channel : this->m_CavDecGroup->getChannels()) {
						channel->historyReset(false);
					}
					return true;
				}));

				// Wait for completion
				for (auto& task : controlSystemUpdate) {
					task.get();
				}

				/* Update precondition PVs */
				bool HVONprecond = true;
				bool RFONprecond = true;
				for (auto channel : this->m_AIGroup[0]->getChannels()) {
					HVONprecond &= channel->getHVONprecond();
					RFONprecond &= channel->getRFONprecond();
				}
				for (auto channel : this->m_AIGroup[1]->getChannels()) {
					HVONprecond &= channel->getHVONprecond();
					RFONprecond &= channel->getRFONprecond();
				}
				for (auto channel : this->m_DIGroup->getChannels()) {
					HVONprecond &= channel->getHVONprecond();
					RFONprecond &= channel->getRFONprecond();
				}
				for (auto channel : this->m_PwrRefGroup->getChannels()) {
					HVONprecond &= channel->getHVONprecond();
					RFONprecond &= channel->getRFONprecond();
				}
				for (auto channel : this->m_CavDecGroup->getChannels()) {
					HVONprecond &= channel->getHVONprecond();
					RFONprecond &= channel->getRFONprecond();
				}
				RFONprecond &= HVONprecond;

				m_HVONPreCondPV.push(tsLoopStart, static_cast<int32_t>(HVONprecond));
				m_RFONPreCondPV.push(tsLoopStart, static_cast<int32_t>(RFONprecond));

			}

		} // END OF MUTEX AREA


		// register monitoring
		int32_t rawreg;
		status = ifcfastint_get_register(&m_deviceUser, m_SelectRegisterPV.getValue(), &rawreg);
		if (status == status_success) {
			m_RawRegisterPV.setValue(rawreg);
			m_RawRegisterPV.push(tsLoopStart, rawreg);
		}

		//TMEM monitoring
		int32_t rawreg_h;
		int32_t rawreg_l;
		status = ifcfastint_get_tmemregister(&m_deviceUser, m_SelectTMEMRegisterPV.getValue(), &rawreg_h, &rawreg_l);
		if (status == status_success) {
			m_RawTMEMRegisterHPV.setValue(rawreg_h);
			m_RawTMEMRegisterHPV.push(tsLoopStart, rawreg_h);
			m_RawTMEMRegisterLPV.setValue(rawreg_l);
			m_RawTMEMRegisterLPV.push(tsLoopStart, rawreg_l);
		}

		clock_gettime(CLOCK_REALTIME, &tsLoopEnd);
		m_ProcPeriod = elapsed_time(tsLoopStart, tsLoopEnd) / 1000;
		m_ProcPeriodPV.setValue(m_ProcPeriod);
		m_ProcPeriodPV.push(tsLoopEnd, m_ProcPeriod);

		m_IntrFreqPV.setValue((1000000.0 / m_IntrFreq));
		m_IntrFreqPV.push(tsLoopEnd, (1000000.0 / m_IntrFreq));
	}  // WHILE LOOP END
}

// placeholder
void IFCFastIntInterlockFSM::getFSMTrend(timespec *timespec, std::vector<std::int32_t> *value) {}
