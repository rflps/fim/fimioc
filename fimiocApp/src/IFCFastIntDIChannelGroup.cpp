#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <climits>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>

#include "device.h"
#include "IFCFastIntDIChannelGroup.h"
#include "IFCFastIntDIChannel.h"

IFCFastIntDIChannelGroup::IFCFastIntDIChannelGroup(const std::string &name,
                                                   nds::Node &parentNode,
                                                   ifcdaqdrv_usr &deviceUser)
    : m_node(nds::Port(name, nds::nodeType_t::generic)),
      m_deviceUser(deviceUser),
      m_HwTimestamp({0,0}) {
  parentNode.addChild(m_node);

  m_numChannels = MAX_DI_CHANNELS;

  for (size_t numChannel(0); numChannel != m_numChannels; ++numChannel) {
    std::ostringstream channelName;
    channelName << "CH" << numChannel;
    m_DIChannels.push_back(std::make_shared<IFCFastIntDIChannel>(
        channelName.str(), m_node, numChannel, m_deviceUser));
  }
}

std::vector<std::shared_ptr<IFCFastIntDIChannel>>
IFCFastIntDIChannelGroup::getChannels() {
  return m_DIChannels;
}

void IFCFastIntDIChannelGroup::setState(nds::state_t newState) {
  for (auto channel : m_DIChannels) {
    channel->setState(newState);
  }
}

// Placeholder function - NOT IN USE FOR NOW
// receive external hardware timestamp and keep it as class param
void IFCFastIntDIChannelGroup::setHwTimestamp(const timespec& ts) {
  m_HwTimestamp.tv_sec = ts.tv_sec;
  m_HwTimestamp.tv_nsec = ts.tv_nsec;
  for (auto channel : m_DIChannels) {
    channel->setHwTimestamp(ts);
  }
}
