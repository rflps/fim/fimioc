#include <ctime>
#include "common.h"

//-----------------------------------------------------------------------------
//  Checks if host is little or big endian
//-----------------------------------------------------------------------------
int CheckEndianess(void){
	unsigned int i = 1;
	char *c = (char*)&i;
	if (*c){
	// Little endian x86
		return 0;
	}
	else{
	// Big endian ppc
		return 1;
	}
}

//-----------------------------------------------------------------------------
//  Measures the elapsed time between two timespecs in nanoseconds
//-----------------------------------------------------------------------------
long elapsed_time(timespec start, timespec end)
{
	timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp.tv_nsec;
}
