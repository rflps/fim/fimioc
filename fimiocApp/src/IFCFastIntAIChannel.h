#ifndef IFCFastIntAICHANNEL_H
#define IFCFastIntAICHANNEL_H

//#include <map>
#include <nds3/nds.h>
#include <ifcfastintdrv.h>

#include "IFCFastIntAnalogPP.h"
#include "interlock_info.h"

typedef enum ifcfastint_scaling_mode_t
{
  scaling_raw,
  scaling_volts,
  scaling_from_file
} ifcfastint_scaling_mode_t;

class IFCFastIntAIChannel {
public:
  IFCFastIntAIChannel(const std::string &name, nds::Node &parentNode,
                      int32_t channelNum, struct ifcdaqdrv_usr &deviceUser,
                      double linConvFactor, std::string &calibFolder,
                      const fim_dataformat_t &dataformat);

  nds::Port m_node;
  nds::StateMachine m_stateMachine;
  struct ifcdaqdrv_usr &m_deviceUser;
  std::string &m_calibFolder;

  std::shared_ptr<FIM_InterlockInfo> m_InterlockInfo;
  std::int32_t m_channelNum;
  int32_t m_firstIlck;

  /* PP Blocks */
  std::vector<std::shared_ptr<IFCFastIntAnalogPP>> m_PPBlocks;
  std::vector<std::shared_ptr<IFCFastIntAnalogPP>> getPPBlocks();

  std::vector<double> m_scalingLut; //TODO: set this as private
  std::map<int, double> m_LUTMap; //TODO: set this as private

  void addValue(int16_t i16data, uint16_t ui16data);
  void getData(timespec *timespec, double *value);
  void getHistory(timespec *timespec, std::vector<double> *value);
  void getHistoryRaw(timespec *timespec, std::vector<std::int32_t> *value);
  void getPPQHistory(timespec *timespec, std::vector<std::uint8_t> *value);
  void getPPQPmortem(timespec *timespec, std::vector<std::uint8_t> *value);

  void historyAddADCValue(int16_t i16data, uint16_t ui16data);
  void historyAddPPQValue(uint8_t data);
  void historyReset(const bool& pmortem, const int& acq_type, const timespec& hwts);
  void reserve(size_t nframes);

  void getHistPMortem(timespec *timespec, std::vector<double> *value);

  void setState(nds::state_t newState);

  void setPPOutput(int32_t qout);
  void getPPQOut(timespec *timespec, int32_t *value);

  void setChEnable(const timespec &timespec, const int32_t &value);
  void getChEnable(timespec *timespec, int32_t *value);
  void setCalibMode(const timespec &timespec, const int32_t &value);
  void getCalibMode(timespec *timespec, int32_t *value);

  void resetInterlock(const timespec &timespec, const int32_t &value);
  void getresetInterlock(timespec *timespec, int32_t *value);

  void setInterlockStateTransitionIdlePre(const timespec &timespec, const int32_t &value);
  void getInterlockStateTransitionIdlePre(timespec *timespec, int32_t *value);

  void setInterlockStateTransitionPreRun(const timespec &timespec, const int32_t &value);
  void getInterlockStateTransitionPreRun(timespec *timespec, int32_t *value);

  void commitParameters(bool calledFromAcquisitionThread = false);
  uint8_t detectInterlock(ifcfastint_fsm_state state);

  void getCalibLut(timespec *timespec, std::vector<double> *value);
  void getRawCalibPoints(timespec *timespec, std::vector<double> *value);
  void getEguCalibPoints(timespec *timespec, std::vector<double> *value);

  std::vector<double> copyCalibLut();

  bool getHVONprecond() {
    return m_HVONprecond;
  };
  bool getRFONprecond() {
    return m_RFONprecond;
  };
  void setHwTimestamp(const timespec& ts);

private:
  const fim_dataformat_t m_DataFormat;
  double m_data;
  std::vector<double> m_history;
  std::vector<double>::iterator m_history_ptr;
  std::vector<std::int32_t> m_historyRaw;
  std::vector<std::int32_t>::iterator m_historyRaw_ptr;

  std::vector<uint8_t> m_ppqhistory;
  std::vector<uint8_t>::iterator m_ppqhistory_ptr;

  /* scaling/calibration configuration and data */
  ifcfastint_scaling_mode_t m_scalingMode;

  std::vector<double> m_rawCalibPoints;
  std::vector<double> m_eguCalibPoints;

  bool m_firstReadout; /* Flag to indicate that this is the first time data is
                          read out. */
  void switchOn();
  void switchOff();
  void start();
  void stop();
  void recover();
  bool allowChange(const nds::state_t, const nds::state_t, const nds::state_t);

  /* Variables for parameters */
  bool m_ch_enable;
  bool m_calibmode;

  /* Variables for transition conditions */
  bool m_HVONenabled;
  bool m_RFONenabled;

  /* Variable to hold current interlock status */
  bool m_ilckStatus;
  bool m_HVONprecond;
  bool m_RFONprecond;

  /* Variables to manage external hardware timestamp */
  timespec m_HwTimestamp; // not in use at the moment
  timespec m_LastHwTimestamp;
  int32_t m_DupHwTimestamp;
  bool ValidHwTimestamp(const timespec& ts) {
    return !((ts.tv_sec == 0) or (ts.tv_nsec == 0));
  }

  /* PV for channel data */
  nds::PVDelegateIn<double> m_dataPV;
  nds::PVDelegateIn<std::vector<double>>  m_historyPV;
  nds::PVDelegateIn<std::vector<std::int32_t>>  m_historyRawPV;
  nds::PVDelegateIn<std::vector<std::uint8_t>> m_ppqhistoryPV;

  nds::PVDelegateIn<std::vector<double>>  m_pmortemPV;
  nds::PVDelegateIn<std::vector<std::uint8_t>> m_ppq_pmortemPV;

  /* PVs for readback values */
  nds::PVDelegateIn<std::int32_t> m_firstIlckPV;
  nds::PVDelegateIn<std::int32_t> m_ch_enablePV;
  nds::PVDelegateIn<std::int32_t> m_ch_calibmodePV;

  /* Preprocssing sub-block */
  // nds::PVDelegateIn<std::int32_t> m_pp_activePV;
  nds::PVDelegateIn<std::int32_t> m_fsmTransIdlePrePV;
  nds::PVDelegateIn<std::int32_t> m_fsmTransPreRunPV;

  /* PVs to publish scaling/calibration data to EPICS */
  nds::PVDelegateIn<std::vector<double>> m_rawCalibPointsPV;
  nds::PVDelegateIn<std::vector<double>> m_eguCalibPointsPV;
  nds::PVDelegateIn<std::vector<double>> m_scalingLutPV;

  // PV to enable/disable pushing data to EPICS when timeout occurs
  nds::PVVariableOut<std::int32_t> m_enableIdleReadingsPV;

  // Precondition PVs
  nds::PVVariableIn<std::int32_t> m_ilckStatusPV;
  nds::PVVariableIn<std::int32_t> m_HVONPreCondPV;
  nds::PVVariableIn<std::int32_t> m_RFONPreCondPV;

  // Use hardware timestamp (enable/disable)
  nds::PVVariableOut<std::int32_t> m_UseHwTimestampPV;

  // Diagnostics: duplicated hw timestamp counter
  nds::PVVariableIn<std::int32_t> m_DupHwTimestampPV;
};

#endif /* IFCFastIntAICHANNEL_H */
