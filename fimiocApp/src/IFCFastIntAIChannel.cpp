
#include <cstdlib>
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>
#include <climits>
#include <algorithm>
#include <map>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>
#include <ifcfastintdrv.h>

#include "device.h"
#include "IFCFastIntAIChannel.h"
#include "IFCFastIntAnalogPP.h"

#include "lutTools.h"


inline void u16manualswap(uint16_t *buffer, int nsamples)
{
		uint16_t aux;
		int i;

		for (i = 0; i < nsamples; i++)
		{
				aux = (buffer[i] & 0xff00) >> 8;
				buffer[i] = ((buffer[i] & 0x00ff) << 8) | aux;

		}
}


IFCFastIntAIChannel::IFCFastIntAIChannel(const std::string &name,
										 nds::Node &parentNode,
										 int32_t channelNum,
										 ifcdaqdrv_usr &deviceUser,
										 double linConvFactor,
										 std::string &calibFolder,
										 const fim_dataformat_t &dataformat)
		: m_node(nds::Port(name, nds::nodeType_t::generic)),
			m_deviceUser(deviceUser), m_calibFolder(calibFolder), m_channelNum(channelNum),
			m_firstIlck(IFCFASTINT_CH_OK),
			m_DataFormat(dataformat),
			m_ch_enable(true), m_calibmode(false),
			m_ilckStatus(false), m_HVONprecond(false), m_RFONprecond(false),
			m_HwTimestamp({0,0}),m_LastHwTimestamp({0,0}),m_DupHwTimestamp(0),
			m_dataPV(nds::PVDelegateIn<double>(
					"Data", std::bind(&IFCFastIntAIChannel::getData, this,
														std::placeholders::_1, std::placeholders::_2))),
			m_historyPV(nds::PVDelegateIn<std::vector<double>>(
					"History", std::bind(&IFCFastIntAIChannel::getHistory, this,
															 std::placeholders::_1, std::placeholders::_2))),
			m_historyRawPV(nds::PVDelegateIn<std::vector<std::int32_t>>(
					"RawData", std::bind(&IFCFastIntAIChannel::getHistoryRaw, this,
															 std::placeholders::_1, std::placeholders::_2))),
			m_ppqhistoryPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
					"PPQHistory", std::bind(&IFCFastIntAIChannel::getPPQHistory, this,
															 std::placeholders::_1, std::placeholders::_2))),
			m_pmortemPV(nds::PVDelegateIn<std::vector<double>>(
					"PmortemHistory", std::bind(&IFCFastIntAIChannel::getHistPMortem, this,
															 std::placeholders::_1, std::placeholders::_2))),
			m_ppq_pmortemPV(nds::PVDelegateIn<std::vector<std::uint8_t>>(
					"PPQPmortem", std::bind(&IFCFastIntAIChannel::getPPQPmortem, this,
															 std::placeholders::_1, std::placeholders::_2))),
			m_firstIlckPV(nds::PVDelegateIn<std::int32_t>(
					"PPQOUT", std::bind(&IFCFastIntAIChannel::getPPQOut, this,
															std::placeholders::_1, std::placeholders::_2))),
			m_ch_enablePV(nds::PVDelegateIn<std::int32_t>(
					"ChEnable-RB", std::bind(&IFCFastIntAIChannel::getChEnable, this,
															std::placeholders::_1, std::placeholders::_2))),
			m_ch_calibmodePV(nds::PVDelegateIn<std::int32_t>(
					"CalibMode-RB", std::bind(&IFCFastIntAIChannel::getCalibMode, this,
															std::placeholders::_1, std::placeholders::_2))),
			m_fsmTransIdlePrePV(nds::PVDelegateIn<std::int32_t>(
					"TRANS-IDLE2PRE-RB",
					std::bind(&IFCFastIntAIChannel::getInterlockStateTransitionIdlePre,
										this, std::placeholders::_1, std::placeholders::_2))),
			m_fsmTransPreRunPV(nds::PVDelegateIn<std::int32_t>(
					"TRANS-PRE2RUN-RB",
					std::bind(&IFCFastIntAIChannel::getInterlockStateTransitionPreRun,
										this, std::placeholders::_1, std::placeholders::_2))),

			m_rawCalibPointsPV(nds::PVDelegateIn<std::vector<double>>(
					"SCA-RAW", std::bind(&IFCFastIntAIChannel::getRawCalibPoints, this,
														std::placeholders::_1, std::placeholders::_2))),
			m_eguCalibPointsPV(nds::PVDelegateIn<std::vector<double>>(
					"SCA-EGU", std::bind(&IFCFastIntAIChannel::getEguCalibPoints, this,
														std::placeholders::_1, std::placeholders::_2))),
			m_scalingLutPV(nds::PVDelegateIn<std::vector<double>>(
					"SCA-DATA", std::bind(&IFCFastIntAIChannel::getCalibLut, this,
														std::placeholders::_1, std::placeholders::_2))),
			m_enableIdleReadingsPV(nds::PVVariableOut<std::int32_t>("EnableIdleReads")),
			m_ilckStatusPV(nds::PVVariableIn<std::int32_t>("ilckStatus")),
			m_HVONPreCondPV(nds::PVVariableIn<std::int32_t>("HVONPreCond")),
			m_RFONPreCondPV(nds::PVVariableIn<std::int32_t>("RFONPreCond")),
			m_UseHwTimestampPV(nds::PVVariableOut<std::int32_t>("UseHwTimestamp")),
			m_DupHwTimestampPV(nds::PVVariableIn<std::int32_t>("DupHwTimestamp"))
{
	/* m_node now is Port node */
	parentNode.addChild(m_node);

	m_stateMachine = nds::StateMachine(
			true, std::bind(&IFCFastIntAIChannel::switchOn, this),
			std::bind(&IFCFastIntAIChannel::switchOff, this),
			std::bind(&IFCFastIntAIChannel::start, this),
			std::bind(&IFCFastIntAIChannel::stop, this),
			std::bind(&IFCFastIntAIChannel::recover, this),
			std::bind(&IFCFastIntAIChannel::allowChange, this, std::placeholders::_1,
								std::placeholders::_2, std::placeholders::_3));

	m_node.addChild(m_stateMachine);
	m_stateMachine.setLogLevel(nds::logLevel_t::none);

	// m_dataPV.setMaxElements(CHANNEL_SAMPLES_MAX);
	m_dataPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_dataPV);

	m_historyPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_historyPV);

	m_historyRawPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_historyRawPV);

	m_ppqhistoryPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_ppqhistoryPV);

	m_pmortemPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_pmortemPV);

	m_ppq_pmortemPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_ppq_pmortemPV);

	/* Add QOUT PV */
	m_firstIlckPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_firstIlckPV);

	/* Add Channel Enable PV */
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
			"ChEnable",
			std::bind(&IFCFastIntAIChannel::setChEnable, this,
													 std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAIChannel::getChEnable, this,
														std::placeholders::_1, std::placeholders::_2)));
	m_ch_enablePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_ch_enablePV);

	/* Add CalibMode Enable PV */
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
			"CalibMode",
			std::bind(&IFCFastIntAIChannel::setCalibMode, this,
													 std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAIChannel::getCalibMode, this,
														std::placeholders::_1, std::placeholders::_2)));
	m_ch_calibmodePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_ch_calibmodePV);

	/* Add PV for Interlock Transisition qualifier*/
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
			"TRANS-IDLE2PRE",
			std::bind(&IFCFastIntAIChannel::setInterlockStateTransitionIdlePre, this,
								std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAIChannel::getInterlockStateTransitionIdlePre, this,
								std::placeholders::_1, std::placeholders::_2)));
	/* Readback PV */
	m_fsmTransIdlePrePV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_fsmTransIdlePrePV);

	/* Add PV for Interlock Transisition qualifier*/
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
			"TRANS-PRE2RUN",
			std::bind(&IFCFastIntAIChannel::setInterlockStateTransitionPreRun, this,
								std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAIChannel::getInterlockStateTransitionPreRun, this,
								std::placeholders::_1, std::placeholders::_2)));
	/* Readback PV */
	m_fsmTransPreRunPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_fsmTransPreRunPV);

	/* Add PV for RESET interlock command */
	m_node.addChild(nds::PVDelegateOut<std::int32_t>(
			"RESET-ILCK",
			std::bind(&IFCFastIntAIChannel::resetInterlock, this,
								std::placeholders::_1, std::placeholders::_2),
			std::bind(&IFCFastIntAIChannel::getresetInterlock, this,
								std::placeholders::_1, std::placeholders::_2)));

	m_rawCalibPointsPV.setScanType(nds::scanType_t::passive);
	m_node.addChild(m_rawCalibPointsPV);

	m_eguCalibPointsPV.setScanType(nds::scanType_t::passive);
	m_node.addChild(m_eguCalibPointsPV);

	m_scalingLutPV.setScanType(nds::scanType_t::passive);
	m_node.addChild(m_scalingLutPV);

	m_node.addChild(m_enableIdleReadingsPV);

	m_node.addChild(m_ilckStatusPV);
	m_node.addChild(m_HVONPreCondPV);
	m_node.addChild(m_RFONPreCondPV);

	m_node.addChild(m_UseHwTimestampPV);
	m_DupHwTimestampPV.setScanType(nds::scanType_t::interrupt);
	m_node.addChild(m_DupHwTimestampPV);

	// Calibration Loading
	std::string myCalibFile = m_calibFolder;
	myCalibFile.append("/calib_data_ch");
	myCalibFile.append(std::to_string(m_channelNum));
	myCalibFile.append(".csv");
	lutTools lutCreator(myCalibFile, 65536, (lutTools_types_t)m_DataFormat, lutTools_voltage_table);
	m_scalingLut = lutCreator.copyLUT();
	m_rawCalibPoints = lutCreator.copyRawPoints();
	m_eguCalibPoints = lutCreator.copyEguPoints();

	/* Set some default values from hardware */
	uint32_t resolution;
	double vref_max;
	ifcdaqdrv_get_resolution(&m_deviceUser, &resolution);
	ifcdaqdrv_get_vref_max(&m_deviceUser, &vref_max);

	/* Create the four pre-processing blocks the belongs to each analog channel */
	m_PPBlocks.push_back(std::make_shared<IFCFastIntAnalogPP>(
			"LVLMON", m_node, m_channelNum, m_deviceUser,
			ifcfastint_analog_pp_lvlmon, &m_scalingLut, m_DataFormat));

	m_PPBlocks.push_back(std::make_shared<IFCFastIntAnalogPP>(
			"PULSHP", m_node, m_channelNum, m_deviceUser,
			ifcfastint_analog_pp_pulshp, &m_scalingLut, m_DataFormat));

	m_PPBlocks.push_back(std::make_shared<IFCFastIntAnalogPP>(
			"PULRAT", m_node, m_channelNum, m_deviceUser,
			ifcfastint_analog_pp_pulrate, &m_scalingLut, m_DataFormat));

	m_PPBlocks.push_back(std::make_shared<IFCFastIntAnalogPP>(
			"DEVMON", m_node, m_channelNum, m_deviceUser,
			ifcfastint_analog_pp_devmon, &m_scalingLut, m_DataFormat));

	// Interlock information
	m_InterlockInfo = std::make_shared<FIM_InterlockInfo>("ILCK", m_node, m_channelNum, fim_analog_in);
}

void IFCFastIntAIChannel::setInterlockStateTransitionIdlePre(const timespec &timespec, const int32_t &value)
{
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	option.idle2pre = value & 1;
	ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_IDLE2PRE_W, ifcfastint_analog_pp_channel, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_fsmTransIdlePrePV.read(&now, &rb_val);
	m_fsmTransIdlePrePV.push(now, rb_val);
}

void IFCFastIntAIChannel::getInterlockStateTransitionIdlePre(timespec *timespec, int32_t *value)
{
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, ifcfastint_analog_pp_channel, &option);
	*value = option.idle2pre;
	clock_gettime(CLOCK_REALTIME, timespec);

	/* Update current status of the condition */
	if (*value) {
		m_HVONenabled = true;
	} else {
		m_HVONenabled = false;
	}
}

void IFCFastIntAIChannel::setInterlockStateTransitionPreRun(
		const timespec &timespec, const int32_t &value) {
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	option.pre2run = value;
	ifcfastint_set_conf_analog_pp(&m_deviceUser, m_channelNum, IFCFASTINT_ANALOG_PRE2RUN_W, ifcfastint_analog_pp_channel, &option);

	// Trigger an update of the readback value from hardware
	int32_t rb_val;
	struct timespec now = {0, 0};
	m_fsmTransPreRunPV.read(&now, &rb_val);
	m_fsmTransPreRunPV.push(now, rb_val);
}

void IFCFastIntAIChannel::getInterlockStateTransitionPreRun(timespec *timespec,	int32_t *value)
{
	struct ifcfastint_analog_option option;
	memset(&option, 0, sizeof(struct ifcfastint_analog_option));
	ifcfastint_get_conf_analog_pp(&m_deviceUser, m_channelNum, ifcfastint_analog_pp_channel, &option);
	*value = option.pre2run;
	clock_gettime(CLOCK_REALTIME, timespec);

	/* Update current status of the condition */
	if (*value) {
		m_RFONenabled = true;
	} else {
		m_RFONenabled = false;
	}
}

void IFCFastIntAIChannel::setChEnable(const timespec &timespec, const int32_t &value) {
	struct timespec tmp;
	int32_t rb_value;

	m_ch_enable = (bool) value;

	// Push a readback value update
	m_ch_enablePV.read(&tmp, &rb_value);
	m_ch_enablePV.push(tmp, rb_value);
}

void IFCFastIntAIChannel::getChEnable(timespec *timespec, int32_t *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = (int32_t) m_ch_enable;
}

void IFCFastIntAIChannel::setCalibMode(const timespec &timespec, const int32_t &value) {
	struct timespec tmp;
	int32_t rb_value;

	m_calibmode = (bool) value;

	// Push a readback value update
	m_ch_calibmodePV.read(&tmp, &rb_value);
	m_ch_calibmodePV.push(tmp, rb_value);
}

void IFCFastIntAIChannel::getCalibMode(timespec *timespec, int32_t *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = (int32_t) m_calibmode;
}

void IFCFastIntAIChannel::commitParameters(bool calledFromAcquisitionThread) {}

void IFCFastIntAIChannel::switchOn() {}
void IFCFastIntAIChannel::switchOff() {}
void IFCFastIntAIChannel::start() {
	m_firstReadout = true;
}
void IFCFastIntAIChannel::stop() {}

/** @brief Don't support recover */
void IFCFastIntAIChannel::recover() {
	throw nds::StateMachineRollBack("Cannot recover");
}

/** @brief Allow all transitions */
bool IFCFastIntAIChannel::allowChange(const nds::state_t, const nds::state_t, const nds::state_t) {
	return true;
}

void IFCFastIntAIChannel::reserve(size_t nframes) {
	m_history.resize(nframes);
	m_history_ptr = m_history.begin();

	m_historyRaw.resize(nframes);
	m_historyRaw_ptr = m_historyRaw.begin();

	m_ppqhistory.resize(nframes);
	m_ppqhistory_ptr = m_ppqhistory.begin();
}


/* Only push data to PV if this channel is enabled! */
void IFCFastIntAIChannel::historyReset(const bool& pmortem, const int& acq_type, const timespec& hwts) {
	struct timespec now;
	bool hwts_is_valid = ValidHwTimestamp(hwts);

	// Check for duplicated timestamps (for diagnostics)
	if (hwts_is_valid) {
		if (ValidHwTimestamp(m_LastHwTimestamp)) {
			if ((m_LastHwTimestamp.tv_sec == hwts.tv_sec) and (m_LastHwTimestamp.tv_nsec == hwts.tv_nsec)) {
				m_DupHwTimestamp += 1;
				m_DupHwTimestampPV.setValue(m_DupHwTimestamp);
			}
		}
		m_LastHwTimestamp.tv_sec = hwts.tv_sec;
		m_LastHwTimestamp.tv_nsec = hwts.tv_nsec;
	}

	// Check if hardware timestamp is enabled
	if (m_UseHwTimestampPV.getValue() and hwts_is_valid) {
		now.tv_sec = hwts.tv_sec;
		now.tv_nsec = hwts.tv_nsec;
	} else {
		clock_gettime(CLOCK_REALTIME, &now);
	}

	/* If this is a post-mortem event, push data to EPICS */
	if (pmortem == true){
		m_pmortemPV.push(now, m_history);
		m_ppq_pmortemPV.push(now, m_ppqhistory);
	}

	if (m_ch_enable == true)
	{
		// Ignore reading IF it's from timeout acquistion AND
		// channel is configured to ignore IDLE readings
		if (((acq_type == MSG_ACQUISITION) || (acq_type == MSG_INTERLOCK)) || (m_enableIdleReadingsPV.getValue() > 0)) {
			m_historyPV.push(now, m_history);
		}
		m_ppqhistoryPV.push(now, m_ppqhistory);
	}

	if (m_calibmode == true) {
		m_historyRawPV.push(now, m_historyRaw);
	}

	m_historyRaw.clear();
	m_history.clear();

	/* Before reset PPQ (interlock status) vector, generate slow update status */
	if (std::find(m_ppqhistory.begin(), m_ppqhistory.end(), 0) != m_ppqhistory.end()) {
		m_ilckStatus = false;
	} else {
		m_ilckStatus = true;
	}
	m_ilckStatusPV.push(now, static_cast<int32_t>(m_ilckStatus));

	// Update the precond PVs
	m_HVONprecond = (m_HVONenabled == true ? m_ilckStatus : true);
	m_HVONPreCondPV.push(now, static_cast<int32_t>(m_HVONprecond));
	m_RFONprecond = (m_RFONenabled == true ? m_ilckStatus : true);
	m_RFONPreCondPV.push(now, static_cast<int32_t>(m_RFONprecond));

	m_ppqhistory.clear();
}

void IFCFastIntAIChannel::historyAddADCValue(int16_t i16data, uint16_t ui16data)
{
	// if data is unsigned we need to convert
	int index;
	int sign_conversion;
	if (m_DataFormat == fim_dataformat_unsigned) {
		index = ui16data;
		sign_conversion = 0;
	} else {
		index = i16data;
		sign_conversion = 32768;
	}

	try {
		*m_history_ptr = m_scalingLut[index+sign_conversion];
		m_history_ptr++;
	} catch (std::exception& e) {
		std::cout << " Exception caught at historyAddADCValue: " << e.what() << std::endl;
		*m_history_ptr = 0;
		m_history_ptr++;
	}

	//*m_historyRaw_ptr = (std::int32_t)data;
	*m_historyRaw_ptr = static_cast<double>(index);
	m_historyRaw_ptr++;
}

void IFCFastIntAIChannel::historyAddPPQValue(uint8_t data)
{
	*m_ppqhistory_ptr = data;
	m_ppqhistory_ptr++;
}

void IFCFastIntAIChannel::addValue(int16_t i16data, uint16_t ui16data)
{
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	int index;
	int sign_conversion;
	if (m_DataFormat == fim_dataformat_unsigned) {
		index = ui16data;
		sign_conversion = 0;
	} else {
		index = i16data;
		sign_conversion = 32768;
	}

	/* USE THIS VALUES TO INDICATE VOLTAGE ON THE ADC */
	double tmp;
	try {
		tmp = m_scalingLut[index+sign_conversion];
	} catch (std::exception& e) {
			std::cout << " Exception caught at addValue: " << e.what() << std::endl;
			tmp = 0;
	}

	if (m_ch_enable == true)
	{
		m_dataPV.push(now, tmp);
	}
}

void IFCFastIntAIChannel::setPPOutput(int32_t ilck) {
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	if ((!m_HVONenabled) && (!m_RFONenabled))
	{
		m_firstIlck = IFCFASTINT_CH_OK;
	} else {
		m_firstIlck = ilck;
	}

	m_firstIlckPV.push(now, m_firstIlck);
}

void IFCFastIntAIChannel::resetInterlock(const timespec &timespec, const int32_t &value)
{
	struct timespec now;
	clock_gettime(CLOCK_REALTIME, &now);

	// Test if channel is being monitored or not
	m_firstIlck = IFCFASTINT_CH_OK;
	m_firstIlckPV.push(now, m_firstIlck);

	// Reset all the pre-processing blocks when called by the individual channel
	if (value == 1) {
		for (auto ppblock : m_PPBlocks) {
			ppblock->resetPP(now, value); // reads new register space
		}
	}
}

void IFCFastIntAIChannel::getresetInterlock(timespec *timespec, int32_t *value)
{
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = 0;
}

void IFCFastIntAIChannel::getPPQOut(timespec *timespec, int32_t *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	*value = m_firstIlck;
}

/* The data PV should only be processed from within. Therefore this function
 * shouldn't do anything */
void IFCFastIntAIChannel::getHistory(timespec *timespec, std::vector<double> *value) {}
void IFCFastIntAIChannel::getHistoryRaw(timespec *timespec, std::vector<std::int32_t> *value) {}
void IFCFastIntAIChannel::getPPQHistory(timespec *timespec, std::vector<std::uint8_t> *value) {}
void IFCFastIntAIChannel::getPPQPmortem(timespec *timespec, std::vector<std::uint8_t> *value) {}

void IFCFastIntAIChannel::getData(timespec *timespec, double *value) {}

/* Expose to EPICS the calibration/scaling data */
void IFCFastIntAIChannel::getCalibLut(timespec *timespec, std::vector<double> *value) {
	clock_gettime(CLOCK_REALTIME, timespec);
	value->resize(m_scalingLut.size());
	*value = m_scalingLut;
}

std::vector<double> IFCFastIntAIChannel::copyCalibLut() {
	return m_scalingLut;
}


void IFCFastIntAIChannel::getRawCalibPoints(timespec *timespec, std::vector<double> *value) {
	int msize = 0;

	msize = m_rawCalibPoints.size();
	clock_gettime(CLOCK_REALTIME, timespec);

	if (msize == 0)
		return;

	value->resize(msize);
	*value = m_rawCalibPoints;
}

void IFCFastIntAIChannel::getEguCalibPoints(timespec *timespec, std::vector<double> *value) {
	int msize = 0;

	msize = m_eguCalibPoints.size();
	clock_gettime(CLOCK_REALTIME, timespec);

	if (msize == 0)
		return;

	value->resize(msize);
	*value = m_eguCalibPoints;
}

/* Post-mortem functions */

void IFCFastIntAIChannel::getHistPMortem(timespec *timespec, std::vector<double> *value) {}

void IFCFastIntAIChannel::setState(nds::state_t newState) {
	m_stateMachine.setState(newState);
}

std::vector<std::shared_ptr<IFCFastIntAnalogPP>> IFCFastIntAIChannel::getPPBlocks()
{
	return m_PPBlocks;
}



uint8_t IFCFastIntAIChannel::detectInterlock(ifcfastint_fsm_state state)
{
	std::vector<std::uint8_t>::iterator local_iter;
	uint8_t last_sample;
	uint8_t graphic_eval;

	/* Graphic evaluation: will iterate the vector searching for a HIGH to LOW transition */
	last_sample = m_ppqhistory[DETECT_START - 1];
	graphic_eval = IFCFASTINT_CH_OK;

	for (local_iter = (m_ppqhistory.begin() + DETECT_START); local_iter != (m_ppqhistory.end() - DETECT_PMORTEM_SIZE + DETECT_POST_WINDOW); local_iter++)
	{
		if ((*local_iter == 0) && (last_sample == 1)) {
			m_InterlockInfo->registerInterlock();
			graphic_eval = IFCFASTINT_CH_NOK;
			break;
		}
		last_sample = *local_iter;
	}
	return graphic_eval;

#if 0
	/* Individual block status: will check the status of each PP Block */
	int32_t blockinterlock = 1;
	uint8_t channel_interlock = 1;
	struct timespec now = {0, 0};
	printf("------------- Current state is %d \n", (int) state);
	for (auto ppblock : m_PPBlocks) {
			ppblock->getProcOut(&now, &blockinterlock);
			channel_interlock &= (uint8_t) blockinterlock;
			printf("    block %d status = %d\n", ppblock->m_ppType, blockinterlock);
	}
	channel_interlock = ~channel_interlock & 0x01; //Invert the logic

	/* If channels is not being monitored, there is no interlock */
	if ((!m_HVONenabled) && (!m_RFONenabled))
	{
		channel_interlock = IFCFASTINT_CH_OK;
	}

	printf("  Channel %d logic = %d\n", ((int) channel_interlock));
	printf("Channel %d | graph detector = %d | blocks = %d\n", m_channelNum, graphic_eval, channel_interlock);

	return (graphic_eval | channel_interlock);
#endif

}

// Placeholder function - NOT IN USE FOR NOW
// receive external hardware timestamp and keep it as class param
void IFCFastIntAIChannel::setHwTimestamp(const timespec& ts) {
	m_HwTimestamp.tv_sec = ts.tv_sec;
	m_HwTimestamp.tv_nsec = ts.tv_nsec;
}
